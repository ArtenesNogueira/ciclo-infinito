# Ciclo Infinito #

Sistema de gerência de doações.

## Status ##

#### O quanto já está feito: 100% ####

Do lado direito nesta página, existe um feed com as alterações que já foram feitas. Você pode clicar no número da alteração (ex. e1ba375) para ler mais detalhes sobre o que foi feito.

#### Estimativa de entrega ####

* 02/06 (Otimista)
* 17/06 (Média)
* 07/07 (Pior caso)

### Tarefas ###

#### Sistema único (50%)####

* Cadastro de planos (50%) [feito]
* Posicionamento em matriz (35%) [feito]
* Interface de visualização de matriz com paginação (15%) [feito]

####Sistema de pagamento (40%)####

* Gatilho para liberação de saldo ao fechar uma matriz (12,5%) [feito]
* Gerência de saldo de usuário (25%) [removido]
* Gerência de solicitações de saque (com opção para definição de dia de saque) (25%) [feito]
* Visualização de saldo por matriz do usuário com opção de saque (25%) [feito]
* Lista com solicitações de saque do usuário (12,5%) [feito]

####Sistema de indicados (10%)####

* Repassar 10% para patrocinador quando patrocinado doa (50%) [feito]
* Definir um patrocinador padrão quando o usuário se cadastrar sem patrocinador (50%) [feito]

#### Sistema único ####

Um usuário poderá estar inserido em mais de uma matriz ao mesmo tempo.

![shot01.png](https://bitbucket.org/repo/8db4x7/images/1027109674-shot01.png)
Na seção de administrador, existe uma seção onde será cadastrados os planos que serão usados no sistema.

![shot02.png](https://bitbucket.org/repo/8db4x7/images/1367044691-shot02.png)
No escritório virtual, existe uma opção do menu chamada 'planos'. Nela o usuário poderá se posicionar em uma matriz com o valor desejado, com base nos planos que estão cadastrados.

![shot03.png](https://bitbucket.org/repo/8db4x7/images/2133051569-shot03.png)
Na tela inicial do escritório virtual terá uma botão para 'próximo' e 'anterior'. Com eles o usuário pode ver as informações das matrizes nas quais ele está posicionado.

#### Sistema de pagamento ####

![shot.png](https://bitbucket.org/repo/8db4x7/images/4115113891-shot.png)
No painel administrativo existe uma seção chamada Pedidos de saque. Lá será possível gerenciar os pedidos de saque os rejeitando ou os aprovando. Também existe a opção de definir o dia de saque do dinheiro por parte dos usuários.

![shot2.png](https://bitbucket.org/repo/8db4x7/images/3350201676-shot2.png)
No escritório virtual haverá uma seção para gerenciar os saques. O usuário poderá solicitar saques e ver o status de suas solicitações. Esse saque estará disponível conforme o dia para saque que for definido.

![shot3.png](https://bitbucket.org/repo/8db4x7/images/2762696845-shot3.png)
Na página inicial do escritório virtual também será possível visualizar o saldo.

Ao termina cada ciclo o saldo do mesmo iria ficar em um lugar do painel do usuário, Fazendo assim com que ficasse acumulado lá ate na hora de sacar. por exemplo; ele terminou 4 Ciclos então estará disponível para ele sacar R$800,00 Reais.

E tivesse uma área para ele pedir o saque desse valor, com um botão que estaria liberado em dias determinados como por exemplo ( Dia 10, 20, 30 ) de cada mês.

Quando ele clicar para sacar iria a informação para o painel de administração com o valor solicitado pelo membro e assim eu faria o pagamento.

#### Sistema de indicados ####

O usuário padrão é definido diretamente no banco de dados, na tabela 'config', coluna 'id_usuario_padrao'. Lá é definido o id de um usuário padrão que irá ser usado toda vez que um novo usuário se cadastrar sem um patrocinador.

Esse é simples de explicar, para cada doação feita pelo seu indicado você recebe 10% do valor doado.

Exemplo: 
Você convidou seu irmão, e ele se cadastrou pelo seu link, então ele escolhe um valor para doa (Ex 100) assim que ele doasse o valor, você receberia 10% no caso 10 Reais.

Tem que arruma o link direitinho para que que não foi convidado por ninguém se cadastre com um link pre determinado com um usuário predeterminado, e quem for convidado por alguém segue normal...
