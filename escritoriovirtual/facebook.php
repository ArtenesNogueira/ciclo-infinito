<?php
session_start();
require 'src/facebook.php';
require_once("conectar_____.php");

if ($dados_usuario[status] != "2") {
	echo "<script>
	alert('".utf8_decode("Sua conta não está ativa!")."');
	window.location.href='./inicio';
	</script>";
	exit;
}

if ($_GET['error'] == "access_denied") {
	echo "<script>
	alert('".utf8_decode("Você precisa conceder as permissões necessárias. Refaça o login...")."');
	window.location.href='./';
	</script>";
	exit;
}

$APP_ID = $dados_config[face_id];
$SECRET = $dados_config[face_secret];
$PERMS = "publish_stream,email,read_stream,user_groups,manage_pages,user_friends,photo_upload,share_item,friends_activities";
 

$facebook = new Facebook(array(
  'appId'  => $APP_ID,
  'secret' => $SECRET,
));

$my_url = "http://" . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'];

$code = $_REQUEST['code'];
if(empty($code)) {
		$dialog_url = "https://www.facebook.com/dialog/oauth?client_id="
			   . $APP_ID . "&redirect_uri=" . urlencode($my_url)
			   . "&scope=$PERMS";

		header("Location: $dialog_url");
		exit;
}

$token_url = "https://graph.facebook.com/oauth/access_token?"
	   . "client_id=" . $APP_ID . "&redirect_uri=" . urlencode($my_url)
	   . "&client_secret=" . $SECRET . "&code=" . $code;

$response = file_get_contents($token_url);
$params = NULL;
parse_str($response, $params);

$user_url = "https://graph.facebook.com/me?access_token=$params[access_token]&id,name,username,email";
$response2 = file_get_contents($user_url);

$user = json_decode($response2);

$sql2 = mysql_query("SELECT * FROM usuarios_face WHERE id_user='".$user->{'id'}."' AND id_usuario<>'".$dados_usuario[id]."'");
if (mysql_num_rows($sql2)) {
	echo "<script>alert('".utf8_decode("Essa conta do facebook já está sendo usada por outro usuário do Ciclo Infinito!")."'); window.location.href='./facebookpost';</script>";
	break;
} else {

	$sql = mysql_query("SELECT * FROM usuarios_face WHERE id_user='".$user->{'id'}."'");
	$validade = date("Y-m-d H:i:s", time() + $params["expires"]);
	
	if (mysql_num_rows($sql)) {
		mysql_query("UPDATE usuarios_face SET nome='".$user->{'name'}."', user='".$user->{'username'}."', email='".$user->{'email'}."', access_token='".$params['access_token']."', token_validade='".$validade."' WHERE id_usuario='".$dados_usuario[id]."'");
		echo "<script>alert('Facebook Atualizado!');</script>";
	} else {
		mysql_query("INSERT INTO usuarios_face VALUES ('','".$dados_usuario[id]."','".$user->{'id'}."','".$user->{'name'}."','".$user->{'username'}."','".$user->{'email'}."','".$params['access_token']."','".$validade."')");
		echo "<script>alert('Facebook Cadastrado!');</script>";
	}
}


echo "<script>window.location.href='./facebookpost';</script>";
?>