<?php 
	
	/**
	*	Posiciona o usuário logado em um novo plano
	*/
	session_start();

	require_once(realpath(dirname(__FILE__)) . '/verifica.php');
	require_once(realpath(dirname(__FILE__)) . '/../administrador/includes/controller-escritorio-virtual.php');
	$controller = new ControllerEscritorioVirtual();
	$controller->assinarPlano(array_merge($_SESSION, $_POST));