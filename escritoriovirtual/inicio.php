<?php 
    
    /**
     * Painel inicial do escritório virtual.
     */

    require_once(realpath(dirname(__FILE__)) . '/../administrador/includes/controller-escritorio-virtual.php');
    $controller = new ControllerEscritorioVirtual();
    $controller->painel($_SESSION);