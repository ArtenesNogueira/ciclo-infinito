<?php 

    session_start();

    require_once(realpath(dirname(__FILE__)) . '/../administrador/includes/controller-pedido-saque.php');

    $controller = new ControllerPedidoSaque();

    $resposta = $controller->novoPedidoSaque($_POST);
    if (!$resposta && $controller->getMensagem()) {
        echo "

            <script>
                alert('{$controller->getMensagem()}');
            </script>

        ";
    }

    if ($resposta) {
        
        echo '<script>window.location.href="./sacar";</script>';

    }

    $controller->mostrarPainelUsuario($_SESSION['ide']);