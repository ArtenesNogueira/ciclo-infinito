<?php 
	
	$urlHome = './../';
	$urlNextcicle = './../avancarciclo';

	session_start();

	if (!isset($_SESSION['ide'])) {
		redirect($urlHome);							
	}

	if (empty($_SESSION['ide'])) {
		redirect($urlHome);
	}

	$idUsuario = $_SESSION['ide'];

	require_once realpath(dirname(__FILE__)) . '/connection.php';

	/**
	 * Obtendo matriz atual do usuário para saber se ela esta pendente de pagamento
	 */
	$sql = '
		SELECT matrizes.id, matrizes.data_cadastro, matrizes.data_liberacao 
		FROM matrizes 
		INNER JOIN usuarios ON matrizes.id_usuario = usuarios.id
		WHERE matrizes.id = usuarios.ciclo_atual AND
		usuarios.id = :idUsuario AND
		matrizes.status != 3
	';
	$statement = $connection->prepare($sql);
	$result = $statement->execute(array(
		':idUsuario' => $idUsuario,
	));

	if (!$result) {
		redirect($urlNextcicle);
	}

	$matriz = $statement->fetch(PDO::FETCH_ASSOC);

	if (!$matriz) {
		redirect($urlNextcicle);	
	}

	$idMatriz = $matriz['id'];

	/**
	 * Verifica se a matriz atual do usuário existe na tabela de pagamentos
	 */
	$sql = 'SELECT status FROM pagamentos WHERE id_matriz = :idMatriz AND id_usuario = :idUsuario';
	$statement = $connection->prepare($sql);
	$result = $statement->execute(array(
		':idMatriz' => $idMatriz,
		':idUsuario' => $idUsuario,
	));

	if (!$result) {
		redirect($urlHome);
	}

	$pagamento = $statement->fetch(PDO::FETCH_ASSOC);

	if (!$pagamento) {
		
		/**
		 * Calculo para saber se já pasosu um dia da data de entrada na matriz
		 */
		$dataLiberacao = new DateTime($matriz['data_liberacao']);
		$now = new DateTime();

		if ($now < $dataLiberacao) {

			redirect("./../index.php?pg=pagamento/pay&idmatriz=$idMatriz");

		} else {

			deactivateMatrix($idMatriz);
			redirect("./../index.php?pg=pagamento/before-position&idmatriz=$idMatriz&status=outoftime");

		}

	}

	/**
	 * Se o status for pendente ou aprovado, vamos só mostrar o status,
	 * caso seja alguma outra coisa, iremos pedir para o reposicionamento
	 */
	$status = $pagamento['status'];

	if ($status == 'pending') {

		redirect('./../index.php?pg=pagamento/status&collection_status=pending');

	} else if ($status == 'approved') {

		redirect('./../index.php?pg=pagamento/status&collection_status=approved');

	} else {

		deactivateMatrix($idMatriz);
		redirect("./../index.php?pg=pagamento/before-position&idmatriz=$idMatriz&status=notpaid");
		
	}	

	function deactivateMatrix($matrixId)
	{

		global $connection;

		$sql = '

			UPDATE matrizes
			SET status = 3
			WHERE id = :idMatriz

		';

		$statement = $connection->prepare($sql);
		$statement->execute(array(
			':idMatriz' => $matrixId,
		));
	}

	function redirect($url)
	{
		header("location:$url");
		die();		
	}