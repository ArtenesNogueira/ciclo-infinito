		<div id="da-content">
            <div class="da-container clearfix">
                <div id="da-sidebar-separator"></div>
                <div id="da-sidebar">
                    <div id="da-sidebar-toggle"></div>
                    <?php include_once("menu.php"); ?>                   
                </div>
                <div id="da-content-wrap" class="clearfix">
                	<div id="da-content-area">
                        
                        <div class="row-fluid">
                            <div class="span12">
                                <div class="da-panel">
                                	<div class="da-panel collapsible">
                                	<div class="da-panel-header">
                                    	<span class="da-panel-title">
                                            <i class="icon-roundabout"></i>
                                            Meus ciclos
                                        </span>
                                    </div
                                    <div class="da-panel-content da-table-container">
                                        <table id="da-ex-datatable-default" class="da-table">
                                            <thead>
                                                <tr>
                                                    <th>ID</th>
                                                    <th>Entrada</th>
                                                    <th>Prazo/Ativação</th>
                                                    <th>Status</th>
                                                    <th>Ações</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            	<?php
                                            	//$sql = mysql_query("SELECT * FROM matrizes WHERE id_usuario='".$_SESSION[ide]."' AND id NOT IN ('49','55','61','67','73') ORDER BY id DESC");
                                            	$sql = mysql_query("SELECT * FROM matrizes WHERE id_usuario='".$_SESSION[ide]."' ORDER BY id DESC");
												if (mysql_num_rows($sql) > 0) {
													while ($dados = mysql_fetch_array($sql)) {
                                            	?>
                                                <tr>
                                                    <td><?php echo "#".$dados[id]; ?></td>
                                                    <td><?php echo FormataDataHora($dados[data_cadastro]); ?></td>
                                                    <td><?php if ($dados[status] == "0" || $dados[status] == "3") echo "<span style='color: red'>".FormataDataHora($dados[data_liberacao])."</span>"; else echo FormataDataHora($dados[data_liberacao]); ?></td>
                                                    <td>
					                        		<?php
					                        		$total = 0;
													$sql9 = mysql_query("SELECT id FROM comprovantes WHERE id_matriz='".$dados[id]."' AND status='0'");
													$total = mysql_num_rows($sql9); 
					                        		if ($dados[status] == "0" && !$total)
					                        			echo "<span style='color: red;'>AGUARDANDO DOAÇÃO <a href='./enviarcomprovante-".$dados[id]."' type='button' class='btn btn-small btn-warning' rel='tooltip' data-placement='left' title='Enviar comprovante'><i class='icon-notes'></i> Enviar comprovante</a></span>";
													if ($dados[status] == "0" && $total)
					                        			echo "<span style='color: red;'>AGUARDANDO CONFIRMAÇÃO</span>";
					                        		else if ($dados[status] == "1")
					                        			echo "<span style='color: orange;'>MATRIZ ABERTA</span>"; else if ($dados[status] == "2") echo "FECHADO";
					                        		else if ($dados[status] == "3")
					                        			echo "CANCELADA";
					                        		?></td>
                                                    <td><div class="btn-group">
                                                    		<?php if ($dados[status] == "0" && !$total)
															echo '<a onclick=\'if (confirm("Tem certeza que deseja cancelar essa matriz?")) return true; else return false;\' type="button" class="btn btn-small btn-danger" href="./cancelarmatriz.php?token='.md5($dados[id]).'&matriz='.$dados[id].'" rel="tooltip" data-placement="left" title="CANCELAR MATRIZ"><i class="icon-remove"></i></a>';
															?>
                                                    		<a type="button" class="btn btn-small" href="./ciclo-<?php echo $dados[id]; ?>" rel='tooltip' data-placement='left' title='VISUALIZAR MATRIZ'><i class="icon-search"></i></a>
                                                		</div>
                                                	</td>
                                                </tr>
                                                <?php
													}
												}
												?>
                                            </tbody>
                                        </table>

                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>