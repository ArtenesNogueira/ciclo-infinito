<?php
session_start();
require_once("conectar_____.php");

$sql1 = mysql_query("SELECT id, nome, usuario FROM usuarios WHERE usuario='".strtolower(trim($_POST[patro]))."' AND status='2'");
$num1 = mysql_num_rows($sql1);
if ($num1 > 0) {
	$patro = AntiSQL($_POST[patro]);
}
$sql2 = mysql_query("SELECT id, nome, usuario FROM usuarios WHERE usuario='".strtolower(trim($_GET[Patrocinador]))."' AND status='2'");
$num2 = mysql_num_rows($sql2);
if ($num2 > 0) {
	$patro = AntiSQL($_GET[Patrocinador]);
}

if (!$num1 && !$num2) {
	echo "<script>
	alert('".utf8_decode("Você precisa de um patrocinador para efetuar o cadastro no sistema!\n\nProcure na fan-page do Ciclo Infinito\nhttp://facebook.com/CicloInfinito")."');
	window.location.href='http://facebook.com/CicloInfinito';
	</script>
	<center>".utf8_decode("<br><br><br><h3>Você precisa de um patrocinador para efetuar o cadastro no sistema!<br><br>Procure na fan-page do Ciclo Infinito<br><a href='http://facebook.com/CicloInfinito'>http://facebook.com/CicloInfinito</a></h3>")."</center>";
	exit;
}

?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--><html lang="pt-BR"><!--<![endif]-->

<head>
	<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-45706014-1', 'cicloinfinito.com.br');
  ga('send', 'pageview');

</script>
<meta charset="utf-8">
<link rel="shortcut icon" href="./assets/images/favicon.ico" type="image/x-icon">
<link rel="icon" href="./assets/images/favicon.ico" type="image/x-icon">

<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<script type="text/javascript" src="./plugins/validar.js"></script>
<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css" media="screen">

<link rel="stylesheet" href="assets/css/fluid.html" media="screen">

<link rel="stylesheet" href="assets/css/login.min.css" media="screen">

<link rel="stylesheet" href="plugins/zocial/zocial.css" media="screen">

<title>Fazer cadastro - <?php echo $dados_config[titulo]." - ".utf8_encode($dados_config[slogan]); ?></title>

</head>

<body>
    <div style="position: absolute; top: 10px; width: 100%;">
    	<center><h2>CADASTRO</h2></center>
    </div>
    <div id="da-home-wrap">
        <div id="da-home-wrap-inner" style="width: 600px; margin-left: calc(-50% - 50px);">
            <div id="da-home-inner">
                <div id="da-home-box">
                    <div id="da-home-box-header">
                        <span class="da-home-box-title"><center><img src="./assets/images/oi.png" width="288" height="56"></center></span>
                    </div>
                    <script type="text/javascript" src="./plugins/jquery.maskedinput.js"></script>
                    <form class="da-form da-home-form" method="post" action="cadastrar.php" name="cadastro">
                    	<input type="hidden" name="novocadastro" value="sim" />
                    	<input type="hidden" name="patro" value="<?php echo $patro; ?>" />
                    	<input type="hidden" name="sessao" value="<?php echo time(); ?>" />
                        <div class="da-form-row">
                        	<p>Você foi indicado por: <strong>
                        	<?php
                        	$sql = mysql_query("SELECT nome, usuario FROM usuarios WHERE usuario='".AntiSQL($patro)."' AND status='2'");
							$dados = mysql_fetch_array($sql);
							echo "$dados[nome] ($dados[usuario])";
                        	?>
                        	</strong></p>
                            <div class=" da-home-form-big">
                            	<br>Nome <strong>*</strong><br>
                                <input type="text" name="nome" value="<?php echo $_POST[nome]; ?>" maxlength="60" >
                            </div>
                            <div class=" da-home-form-big">
                            	<br>E-mail <strong>*</strong><br>
                                <input type="text" name="email" value="<?php echo $_POST[email]; ?>" maxlength="60" >
                                <small style="color: red">IMPORTANTE: DIGITE UM E-MAIL VÁLIDO!</small>
                            </div>
                            <div class=" da-home-form-big">
                            	<br>Repita seu e-mail <strong>*</strong><br>
                                <input type="text" name="email2" maxlength="60" >
                            </div>               
<div class=" da-home-form-big">
                            	<br>CPF <strong>*</strong><br>
                                <input type="text" name="cpf" id="cpf" placeholder="000.000.000-00" maxlength="14" required>
                            </div>							
                            <h3>Dados de acesso</h3>
                            <div class=" da-home-form-big">
                            	<br>Usuário <strong>*</strong><br>
                                <input type="text" name="usuario" maxlength="16" pattern="[a-zA-Z0-9]{3,16}" title="De 3 à 16 caracteres." required>
                                <small style="color: red">Obs.: Sem espaços e sem caracteres especiais!</small>
                            </div>
                            <div class=" da-home-form-big">
                            	<br>Senha <strong>*</strong><br>
                                <input type="password" name="senha" maxlength="16" required>
                            </div>
                            <div class=" da-home-form-big">
                            	<br>Repete a senha <strong>*</strong><br>
                                <input type="password" name="senha2" maxlength="16" required>
                            </div>
                            <br>
							(OBS: Cadastre seus dados bancários apenas no escritório virtual depois do cadastro.)
                            
							
							
                        </div>
                        <div class="da-home-form-btn-big">
                            <input type="submit" value="Concluir cadastro" id="da-login-submit" class="btn btn-success btn-block"><br>
                            <center><a class="btn" href="http://alfarenda.com/">voltar</a></center>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script src="assets/js/libs/jquery-1.8.3.min.js"></script>
    <script src="assets/js/libs/jquery.placeholder.min.js"></script>
    <script src="plugins/validate/jquery.validate.min.js"></script>

    <script src="assets/js/core/dandelion.login.js"></script>
    
</body>
</html>

