<?php
if ($_GET[get] != "") {
	$get = explode("_",$_GET[get]);
	if (($get[0] == "remover") && (is_numeric($get[1]))) {
		$sql2 = mysql_query("SELECT anexo FROM comprovantes WHERE id='".$get[1]."' AND id_doador='".$_SESSION[ide]."'");
		$dados2 = mysql_fetch_array($sql2);
		
		if ($dados2[anexo] != "")
			if (unlink("./arquivos/comprovantes/".$dados2[anexo]));
		
		$sql = mysql_query("DELETE FROM comprovantes WHERE id='".$get[1]."' AND id_doador='".$_SESSION[ide]."' AND status='0'");
		
		echo "<script>alert('Comprovante removido!'); window.location.href='./comprovantes';</script>";
		
	}
}
?>
		<div id="da-content">
            <div class="da-container clearfix">
                <div id="da-sidebar-separator"></div>
                <div id="da-sidebar">
                    <div id="da-sidebar-toggle"></div>
                    <?php include_once("menu.php"); ?>                   
                </div>
                <div id="da-content-wrap" class="clearfix">
                	<div id="da-content-area">
                        
                        <div class="row-fluid">
                            <div class="span12">
                                <div class="da-panel">
                                	<div class="da-panel collapsible">
                                	<div class="da-panel-header">
                                    	<span class="da-panel-title">
                                            <i class="icol-blog"></i>
                                            Comprovantes enviados
                                        </span>
                                    </div
                                    <div class="da-panel-content da-table-container">
                                        <table id="da-ex-datatable-default" class="da-table">
                                            <thead>
                                                <tr>
                                                    <th>Matriz</th>
                                                    <th>Donatário</th>
                                                    <th>Anexo</th>
                                                    <th>Data/Hora</th>
                                                    <th>Nº Doc</th>
                                                    <th>AG. Origem</th>
                                                    <th>Tipo</th>
                                                    <th>Valor</th>
                                                    <th>Status</th>
                                                    <th>Ações</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            	<?php
                                            	$sql = mysql_query("SELECT * FROM comprovantes WHERE id_doador='".$_SESSION[ide]."' ORDER BY data_envio DESC");
												if (mysql_num_rows($sql) > 0) {
													while ($dados = mysql_fetch_array($sql)) {
                                            	?>
                                                <tr>
                                                    <td><?php echo "<span rel='tooltip' data-placement='top' title='Enviado em ".FormataDataHora($dados[data_envio])."'>#".$dados[id_matriz]."</span>"; ?></td>
                                                    <td><?php NomeUsuario($dados[id_donatario]); ?></td>
                                                    <td><?php if ($dados[anexo]) { ?><a href="./arquivos/comprovantes/<?php echo $dados[anexo]; ?>" target="_new" class="btn btn-success"><i class="icol-attach-2"></i></a><?php } else echo "-"; ?></td>
                                                    <td><?php echo $dados[data]." - ".$dados[hora]; ?></td>
                                                    <td><?php echo $dados[numero]; ?></td>
                                                    <td><?php echo $dados[agencia_origem]; ?></td>
                                                    <td><?php if ($dados[tipo] == "1") echo "Depósito em dinheiro"; else if ($dados[tipo] == "2") echo "Transferência"; ?></td>
                                                    <td><?php echo $dados[valor]; ?></td>
                                                    <td><?php if ($dados[status] == "0") echo "<span class='label label-warning'>PENDENTE</span>"; else if ($dados[status] == "1") echo "<span class='label label-success'>LIBERADO</span>"; else if ($dados[status] == "2") echo "<span class='label label-important'>RECUSADO</span>"; ?></td>
                                                    <td><?php if ($dados[status] == "0") { ?>
							                        			<a class="btn btn-small btn-danger" href="./comprovantes-remover_<?php echo $dados[id]; ?>" rel='tooltip' data-placement='left' title='Remover comprovante'><i class="icon-remove"></i></a>
							                        			<?php } ?>
							                        </td>	
                                                </tr>
                                                <?php
													}
												}
												?>
                                            </tbody>
                                        </table>

                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>