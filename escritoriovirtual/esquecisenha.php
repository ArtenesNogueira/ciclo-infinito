<?php
session_start();
require_once("conectar_____.php");

if ($_POST && $_SESSION[form_senha] != $_POST[sessao]) {
		$_SESSION[form_senha] = AntiSQL($_POST[sessao]);
		$sql = mysql_query("SELECT nome, email, usuario FROM usuarios WHERE usuario='".AntiSQL($_POST[usuario])."' AND email='".AntiSQL($_POST[email])."' AND cpf='".AntiSQL($_POST[cpf])."'");
		if (mysql_num_rows($sql) > 0) {
			$dados = mysql_fetch_array($sql);
			$hoje = date("Y-m-d H:i:s");
			$codigo = md5(time());
			
			$assunto = "Recuperar a senha!";
			$headers = "MIME-Version: 1.0\r\n";
			$headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
			$headers .= "To: $dados[nome] <".AntiSQL($dados[email]).">\r\n";
			$headers .= "From: Ciclo Infinito <contato@cicloinfinito.com.br>\r\n";
			
			$mensagem = "
			Olá ".$dados[nome].",<br><br>
			
			Conforme solicitado, segue o link para dar continuidade a redefinição de sua senha no Ciclo Infinito.<br><br>
			
			Copie o link e cole no seu navegador<br>
			http://escritoriovirtual.cicloinfinito.com.br/redefinirsenha.php?codigo=".$codigo."<br><br>
			
			Att. Ciclo Infinito<br>
			contato@cicloinfinito.com.br
			";
	
			EnviarEmail("$dados[email]","contato@cicloinfinito.com.br",$assunto,$mensagem);
			//mail("$dados[email]",utf8_decode($assunto),utf8_decode($mensagem),$headers);
			
			mysql_query("DELETE FROM recuperar_senha WHERE usuario='".$dados[usuario]."'");
			mysql_query("INSERT INTO recuperar_senha VALUES ('','".$hoje."','".$codigo."','".$dados[usuario]."','".$dados[email]."')");
			
			echo "<script>
			alert('Verifique seu e-mail para concluir sua solicitação!');
			window.location.href='./login.php';
			</script>";
		} else {
			echo "<script>
			alert('Dados não encontrado. Verifique se digitou corretamente!');
			</script>";
		}
}
?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--><html lang="pt-BR"><!--<![endif]-->

<head>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-45706014-1', 'cicloinfinito.com.br');
  ga('send', 'pageview');

</script>
<meta charset="utf-8">
<link rel="shortcut icon" href="./assets/images/favicon.ico" type="image/x-icon">
<link rel="icon" href="./assets/images/favicon.ico" type="image/x-icon">

<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">

<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css" media="screen">

<link rel="stylesheet" href="assets/css/fluid.html" media="screen">

<link rel="stylesheet" href="assets/css/login.min.css" media="screen">

<link rel="stylesheet" href="plugins/zocial/zocial.css" media="screen">

<title>Fazer login - <?php echo $dados_config[titulo]." - ".utf8_encode($dados_config[slogan]); ?></title>

</head>

<body>
    
    <div id="da-home-wrap">
        <div id="da-home-wrap-inner">
            <div id="da-home-inner">
                <div id="da-home-box">
                    <div id="da-home-box-header">
                        <span class="da-home-box-title"><center><img src="./assets/images/logo_cicloinfinito.png" width="288" height="56"></center></span>
                    </div>
                    <form class="da-form da-home-form" method="post">
                    	<input type="hidden" name="sessao" value="<?php echo time(); ?>" />
                        <div class="da-form-row">
                        	<p>Preencha os campos abaixo para iniciar a recuperação de senha!</p>
                            <div class=" da-home-form-big">
                                <input type="text" name="usuario" placeholder="Usuário" required>
                            </div>
                            <div class=" da-home-form-big">
                                <input type="text" name="email" placeholder="E-mail" required>
                            </div>
                            <div class=" da-home-form-big">
                                <input type="text" name="cpf" placeholder="CPF (000.000.000-00)" required>
                            </div>
                        </div>
                        <div class="da-home-form-btn-big">
                            <input type="submit" value="Enviar" id="da-login-submit" class="btn btn-success btn-block"><br>
                            <center><a class="btn" href="./login.php">voltar</a></center>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script src="assets/js/libs/jquery-1.8.3.min.js"></script>
    <script src="assets/js/libs/jquery.placeholder.min.js"></script>
    <script src="plugins/validate/jquery.validate.min.js"></script>

    <script src="assets/js/core/dandelion.login.js"></script>
    
</body>
</html>

