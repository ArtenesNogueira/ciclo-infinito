<?php
session_start();
require_once("conectar_____.php");
require_once("verifica.php");
$sql_usuario = mysql_query("SELECT * FROM usuarios WHERE id='".$_SESSION[ide]."' AND usuario='".$_SESSION[usuario]."'");
$dados_usuario = mysql_fetch_array($sql_usuario);
?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--><html lang="pt-br"><!--<![endif]-->


<head>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-45706014-1', 'cicloinfinito.com.br');
  ga('send', 'pageview');

</script>
<meta charset="utf-8">
<link rel="shortcut icon" href="./assets/images/favicon.ico" type="image/x-icon">
<link rel="icon" href="./assets/images/favicon.ico" type="image/x-icon">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">

<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css" media="screen">


<link rel="stylesheet" href="assets/css/dandelion.theme.css" media="screen">

<link rel="stylesheet" href="assets/css/fonts/glyphicons/style.css" media="screen">

<link rel="stylesheet" href="assets/css/dandelion.min.css" media="screen">
<link rel="stylesheet" href="assets/css/customizer.css" media="screen">

<link rel="stylesheet" href="assets/css/demo.css" media="screen">


<link rel="stylesheet" href="assets/js/plugins/wizard/dandelion.wizard.css" media="screen">

<title>Escritório Virtual - <?php echo $dados_config[titulo]." - ".utf8_encode($dados_config[slogan]); ?></title>

</head>



<!-- BEGIN JIVOSITE CODE {literal} -->
<script type='text/javascript'>
(function(){ var widget_id = 'WWwSmnWNqR';var d=document;var w=window;function l(){
var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = '//code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);}if(d.readyState=='complete'){l();}else{if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})();</script>
<!-- {/literal} END JIVOSITE CODE -->

<body>


	<div id="da-wrapper">
    
        <?php include_once("topo.php");
		
		
        if (file_exists($_GET[pg].".php")) {
        	include($_GET[pg].".php");
        } else {
        	include("inicio.php");
        }
        
        
		include_once("rodape.php");
		
		?>
        
    </div>


    <script src="assets/js/libs/jquery-1.8.3.min.js"></script>
    <script src="assets/js/libs/jquery.placeholder.min.js"></script>
    <script src="assets/js/libs/jquery.mousewheel.min.js"></script>

    <script src="bootstrap/js/bootstrap.min.js"></script>
    

    <script src="assets/jui/js/jquery-ui-1.9.2.min.js"></script>
    <script src="assets/jui/jquery.ui.timepicker.min.js"></script>
    <script src="assets/jui/jquery.ui.touch-punch.min.js"></script>
    <script type="text/javascript" src="assets/jui/js/globalize/globalize.js"></script>
    <script type="text/javascript" src="assets/jui/js/globalize/cultures/globalize.culture.en-US.js"></script>
    <link rel="stylesheet" type="text/css" href="assets/jui/css/jquery.ui.all.css" media="screen">
	
	
    <script src="assets/js/plugins/wizard/dandelion.wizard.min.js"></script>
    <script src="assets/js/libs/jquery.form.min.js"></script>

    <script src="assets/js/core/dandelion.core.js"></script>

    <script src="assets/js/core/dandelion.customizer.js"></script>
    <script src="plugins/select2/select2.js"></script>
    <link rel="stylesheet" href="plugins/select2/select2.css" media="screen">
    <script src="assets/js/plugins/picklist/picklist.min.js"></script>
    <link rel="stylesheet" href="assets/js/plugins/picklist/picklist.css" media="screen">
    	
    <script src="assets/js/demo/demo.form.js"></script>
    
</body>

</html>
