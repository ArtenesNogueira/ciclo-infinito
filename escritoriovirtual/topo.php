<div id="da-header">
        
            <div id="da-header-top">
                
                <!-- Container -->
                <div class="da-container clearfix">
                    
                    <!-- Logo Container. All images put here will be vertically centere -->
                    <div id="da-logo-wrap">
                        <div id="da-logo">
                            <div id="da-logo-img">
                                <a href="./">
                                    <img src="assets/images/oi.png" alt="Alfa Renda">
                                </a>
                            </div>
                        </div>
                    </div>
                    
                    <!-- Header Toolbar Menu -->
                    <div id="da-header-toolbar" class="clearfix">
                        <div id="da-user-profile-wrap">
                            <div id="da-user-profile" data-toggle="dropdown" class="clearfix">
                                <div id="da-user-avatar">
                                    <img src="assets/images/pp.png" alt="<?php echo $dados_usuario[nome]; ?>">
                                </div>
                                <div id="da-user-info">
                                    <?php echo $dados_usuario[nome]; ?>
                                    <span class="da-user-title"><?php if ($dados_usuario[status] == "2") echo "http://alfarenda.com/".$dados_usuario[usuario]; else echo "@".$dados_usuario[usuario]; ?></span>
                                </div>
                            </div>
                            <ul class="dropdown-menu">
                                <li><a href="./">Painel</a></li>
                                <li class="divider"></li>
                                <li><a href="./minhaconta">Minha conta</a></li>
                                <li><a href="./alterarsenha">Alterar senha</a></li>
                                <li><a href="./logdeacessos">Histórico de acessos</a></li>
                            </ul>
                        </div>
                        <div id="da-header-button-container">
                            <ul>
                                <!--<li class="da-header-button-wrap">
                                    <div class="da-header-button" data-toggle="dropdown">
                                        <span class="btn-count">32</span>
                                        <a href="#"><i class="icon-circle-exclamation-mark"></i></a>
                                    </div>
                                    <ul class="dropdown-menu pull-right">
                                        <li>
                                            <span class="da-dropdown-sub-title">Notificações</span>
                                            <ul class="da-dropdown-sub">
                                                <li class="unread">
                                                    <a href="#">
                                                        <span class="thumbnail"><img src="assets/images/pp.jpg" alt=""></span>
                                                        <span class="info">
                                                            <span class="name">John Doe</span>
                                                            <span class="message">
                                                                Lorem ipsum dolor sit amet consectetur adipiscing elit, et al commore
                                                            </span>
                                                            <span class="time">
                                                                January 21, 2012
                                                            </span>
                                                        </span>
                                                    </a>
                                                </li>
                                                <li class="unread">
                                                    <a href="#">
                                                        <span class="thumbnail"><img src="assets/images/pp.jpg" alt=""></span>
                                                        <span class="info">
                                                            <span class="name">John Doe</span>
                                                            <span class="message">
                                                                Lorem ipsum dolor sit amet consectetur adipiscing elit, et al commore
                                                            </span>
                                                            <span class="time">
                                                                January 21, 2012
                                                            </span>
                                                        </span>
                                                    </a>
                                                </li>
                                                <li class="read">
                                                    <a href="#">
                                                        <span class="thumbnail"><img src="assets/images/pp.jpg" alt=""></span>
                                                        <span class="info">
                                                            <span class="name">John Doe</span>
                                                            <span class="message">
                                                                Lorem ipsum dolor sit amet consectetur adipiscing elit, et al commore
                                                            </span>
                                                            <span class="time">
                                                                January 21, 2012
                                                            </span>
                                                        </span>
                                                    </a>
                                                </li>
                                                <li class="read">
                                                    <a href="#">
                                                        <span class="thumbnail"><img src="assets/images/pp.jpg" alt=""></span>
                                                        <span class="info">
                                                            <span class="name">John Doe</span>
                                                            <span class="message">
                                                                Lorem ipsum dolor sit amet consectetur adipiscing elit, et al commore
                                                            </span>
                                                            <span class="time">
                                                                January 21, 2012
                                                            </span>
                                                        </span>
                                                    </a>
                                                </li>
                                            </ul>
                                            <a class="da-dropdown-sub-footer" href="./notificacoes">
                                                Ver todas notificações
                                            </a>
                                        </li>
                                    </ul>
                                </li>-->
                                <!--<li class="da-header-button-wrap">
                                    <div class="da-header-button" data-toggle="dropdown">
                                        <span class="btn-count" style='background: green; border-color: #390; left: 30px;'>ON</span>
                                        <a href="#"><i class="icon-chat"></i></a>
                                    </div>
                                    
                                </li>-->
                                <li class="da-header-button-wrap">
                                    <div class="da-header-button">
                                        <a href="sair.php"><i class="icon-power"></i></a>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                                    
                </div>
            </div>
            
            <div id="da-header-bottom">
                <!-- Container -->
                <div class="da-container clearfix">
                
              
                	
                    <!-- Breadcrumbs -->
                    <div id="da-breadcrumb">
                        <ul>
                            <li class="active"><a href="./"><i class="icon-home"></i> Painel</a></li>
                        </ul>
                    </div>
                    
                </div>
            </div>
        </div>