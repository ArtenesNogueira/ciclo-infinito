<div id="da-content">
    <div class="da-container clearfix">
        <div id="da-sidebar-separator"></div>
        <div id="da-sidebar">
            <div id="da-sidebar-toggle"></div>
            <?php include_once("menu.php"); ?>                   
        </div>
        <div id="da-content-wrap" class="clearfix">
            <div id="da-content-area">
                <div class="row-fluid">

                    <?php 

                        $conexao = require(realpath(dirname(__FILE__)) . '/../administrador/includes/conectar.php');
                        require(realpath(dirname(__FILE__)) . '/../administrador/includes/planos.php');
                        $plano = new Plano($conexao);
                        $planos = $plano->obterTodosPlanos();

                    ?>

                    <?php if($planos): ?>

                        <?php foreach ($planos as $plano): ?>

                                <div class="da-panel">
                                    <div class="da-panel collapsible">
                                       <div class="da-panel-header">
                                            <span class="da-panel-title">
                                                <i class="icol-star-1"></i>
                                                <?php echo $plano['nome']; ?>
                                            </span>
                                        </div>
                                        <div class="da-panel-content">
                                            <blockquote>
                                                <?php echo $plano['descricao'] ? "{$plano['descricao']}" : 'Sem descrição'; ?>
                                                <br><br>
                                                <?php echo "Valor: <h3>R\${$plano['valor']}</h3>"; ?>
                                            </blockquote>
                                            <form action="./posicionarmatriz" method="POST">
                                                <center>
                                                    <input type="hidden" name="idPlano" value="<?php echo $plano['id'] ?>">
                                                    <input type="submit" value="Contratar" class="btn btn-primary">
                                                </center>
                                            </form>
                                        </div>
                                    </div>
                                </div>

                        <?php endforeach; ?>

                    <?php else: ?>
                        <div class="span4">
                            <div class="da-panel">
                                <h3>Nenhum plano encontrado</h3>
                            </div>
                        </div>                        
                    <?php endif; ?>

                </div>
            </div>
        </div>
    </div>
</div>