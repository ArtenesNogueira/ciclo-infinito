<div id="da-main-nav" class="btn-container">
                       
                        <ul>
                            <li <?php if ($_GET[pg] == "" || $_GET[pg] == "inicio") echo "class='active'"; ?>>
                            	<a href="./inicio">
                                	<span class="da-nav-icon">
                                        <i class="icon-home"></i>
                                    </span>
                                	Painel
                                </a>
                            </li>
                            <li <?php if ($_GET[pg] == "upgrades" || $_GET[pg] == "upgrade1" || $_GET[pg] == "upgrade2" || $_GET[pg] == "upgrade5") echo "class='active'"; ?>>
                                <a href="./upgrades">
                                    <span class="da-nav-icon">
                                        <i class="icon-star"></i>
                                    </span>
                                    Planos
                                </a>
                            </li>
                           
                            <li <?php if ($_GET[pg] == "usuariosdiretos") echo "class='active'"; ?>>
                            	<a href="./usuariosdiretos">
                                    <span class="da-nav-count"><?php echo TotalDireto($_SESSION[ide]); ?></span>
                                	<span class="da-nav-icon">
                                        <i class="icon-adress-book"></i>
                                    </span>
                                	Indicados
                                </a>
                            </li>
							
							  
							
                            <!--<li <?php if ($_GET[pg] == "rede") echo "class='active'"; ?>>
                            	<a href="./rede">
                                	<span class="da-nav-icon">
                                        <i class="icon-share-alt"></i>
                                    </span>
                                	Minha rede
                                </a>
                            </li>-->
                           
                        </ul>
                    </div>