<?php
session_start();
require_once("conectar_____.php");
?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--><html lang="pt-BR"><!--<![endif]-->

<head>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-45706014-1', 'cicloinfinito.com.br');
  ga('send', 'pageview');

</script>
<meta charset="utf-8">
<link rel="shortcut icon" href="./assets/images/favicon.ico" type="image/x-icon">
<link rel="icon" href="./assets/images/favicon.ico" type="image/x-icon">

<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">

<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css" media="screen">

<link rel="stylesheet" href="assets/css/fluid.html" media="screen">

<link rel="stylesheet" href="assets/css/login.min.css" media="screen">

<link rel="stylesheet" href="plugins/zocial/zocial.css" media="screen">

<title>Fazer login - <?php echo $dados_config[titulo]." - ".utf8_encode($dados_config[slogan]); ?></title>

</head>

<body>
    
    <div id="da-home-wrap">
        <div id="da-home-wrap-inner">
            <div id="da-home-inner">
                <div id="da-home-box">
                    <div id="da-home-box-header">
                        <span class="da-home-box-title"><center><img src="./assets/images/oi.png" width="288" height="56"></center></span>
                    </div>
                    <form class="da-form da-home-form" method="post" action="autenticar.php">
                    	<input type="hidden" name="url" value="<?php echo $_GET['url']; ?>">
                        <div class="da-form-row">
                            <div class=" da-home-form-big">
                                <input type="text" name="usuario" id="da-login-username" placeholder="Usuário">
                            </div>
                            <div class=" da-home-form-big">
                                <input type="password" name="senha" id="da-login-password" placeholder="Senha">
                            </div>
                        </div>
                        <div class="da-form-row">
                            <ul class="da-form-list inline">
                                <li class="pull-right"><a href="./esquecisenha.php">Esqueci a senha</a></li>
                            </ul>
                        </div>
                        <div class="da-home-form-btn-big">
                            <input type="submit" value="Entrar" id="da-login-submit" class="btn btn-success btn-block"><br>
                            <center><a href='http://www.alfarenda.com/' class="btn">ir para o site</a></center>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script src="assets/js/libs/jquery-1.8.3.min.js"></script>
    <script src="assets/js/libs/jquery.placeholder.min.js"></script>
    <script src="plugins/validate/jquery.validate.min.js"></script>

    <script src="assets/js/core/dandelion.login.js"></script>
    
</body>
</html>

