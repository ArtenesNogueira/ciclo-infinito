<script language="jscript">
function Change() {
	if (document.getElementById("cam").value == "data_cadastro") {
		$('#campo2').attr("disabled", true); $('#data').attr("disabled", false);
		$('#data').show(); $('#campo2').hide();
	} else {
		$('#campo2').attr("disabled", false); $('#data').attr("disabled", true);
		$('#campo2').show(); $('#data').hide();
	}
}
</script>
<?php
if ($_GET[cam] == "data_cadastro") {
	$d = explode("/",$_GET[campo]);
	$campo = "$d[2]-$d[1]-$d[0]";
} else {
	$campo = $_GET[campo];
}
?>
<body onLoad="Change();">
  <div id="content-header">
    <div id="breadcrumb"> <a href="./inicio" title="Voltar para o painel" class="tip-bottom"><i class="icon-home"></i> Painel</a><a href="./usuarios" class="current">Usuários</a></div>
  </div>
  <div class="container-fluid">
    
   
    <div class="row-fluid">
      <div class="span12">
        <div class="widget-box">
          <div class="widget-title">
             <span class="icon"><i class="icon-th"></i></span> 
            <h5>Usuários</h5>
          </div>
          <p><form method="get" action="index.php" name="busca">
          <input type="hidden" name="pg" value="<?php echo $_GET[pg]; ?>" />
          <input type="hidden" name="pg2" value="1" />
          Pesquisar:
          <select class="span4" id="cam" name="cam" onChange="return Change();">
            <option value="usuario" <?php if ($_GET[cam] == "usuario") echo "selected"; ?>>Usuário</option>
            <option value="id_patro" <?php if ($_GET[cam] == "id_patro") echo "selected"; ?>>Id Patrocinador</option>
          	<option value="nome" <?php if ($_GET[cam] == "nome") echo "selected"; ?>>Nome</option>
            <option value="email" <?php if ($_GET[cam] == "email") echo "selected"; ?>>E-mail</option>
            <option value="cpf" <?php if ($_GET[cam] == "cpf") echo "selected"; ?>>CPF</option>
            <option value="data_cadastro" <?php if ($_GET[cam] == "data_cadastro") echo "selected"; ?>>Data cadastro</option>
          </select>
          <select class="span1" name="op">
          	<option value="=" <?php if ($_GET[op] == "=") echo "selected"; ?>>=</option>
            <option value="<>" <?php if ($_GET[op] == "<>") echo "selected"; ?>><></option>
            <option value="like" <?php if ($_GET[op] == "like") echo "selected"; ?>>% LIKE %</option>
          </select>
          <input type='text' name='campo' style='height: 30px;' value='<?php echo FormataData($campo); ?>' autocomplete='off' class='tcal' id='data' maxlength='10' />
          <input type='text' name='campo' id="campo2" style='height: 30px;' value='<?php echo $campo; ?>' autocomplete='off' />          
          <input type="submit" value="buscar" style="height: 30px;" />
          </form></p>
          
          <div class="widget-content nopadding">
            <table class="table table-bordered data-table">
              <thead>
                <tr>
                  <th>Patrocinador</th>
                  <th>Nome</th>
                  <th>E-mail</th>
                  <th>Skype</th>
                  <th>Data de cadastro</th>
                  <th>Status</th>
                  <th>Ações</th>
                </tr>
              </thead>
              <tbody>
              <?php
			    $totalpag = 50;
				if ($_GET[pg3] == "") $pagina = 1; else $pagina = $_GET[pg3];
				$inicio = $pagina - 1;
				$inicio = $totalpag * $inicio;
				if ($campo != "" && $_GET[op] != "like") {
					$cri = "AND $_GET[cam] $_GET[op] '$campo'";
				} if ($_GET[op] == "like") {
					$cri = "AND $_GET[cam] $_GET[op] '%$campo%'";
				}
			  	$query = "SELECT id, id_patro, nome, email, skype, usuario, data_cadastro, status FROM usuarios WHERE id > '0' $cri";

			  $num = mysql_query("$query");
			  $resultado = mysql_num_rows($num);
			  echo $resultado." encontrado";
			  $sql_usuarios = mysql_query("$query ORDER BY id DESC LIMIT $inicio,$totalpag");
			  while ($dados_usuarios = mysql_fetch_array($sql_usuarios)) {
			  ?>
                <tr class="gradeX">
                  <td><a href="./ver_usuario-<?php echo $dados_usuarios['id_patro']; ?>"><?php echo NomeUsuario($dados_usuarios['id_patro']); ?></a></td>
                  <td><a href="./ver_usuario-<?php echo $dados_usuarios['id']; ?>"><?php echo $dados_usuarios['nome']." (".$dados_usuarios['usuario'].")"; ?></a></td>
                  <td><?php echo $dados_usuarios['email']; ?></td>
                  <td><?php echo $dados_usuarios['skype']; ?></td>
                  <td class="center"><?php echo FormataDataHora($dados_usuarios['data_cadastro']); ?></td>
                  <td class="taskStatus">
                  <?php
                  if ($dados_usuarios[status] == "2") echo "<span class='label label-success'>ativo</span>";
				  if ($dados_usuarios[status] == "0") echo "<span class='label label-important'>email pendente</span>";
				  if ($dados_usuarios[status] == "1") echo "<span class='label label-warning'>email confirmado</span>";
				  ?>
                  </td>
                  <td>-</td>
                </tr>
              <?php
			  }
			  ?>
              </tbody>
            </table>
            <?php
			
			$total_pag = ceil($resultado / $totalpag);
			$i = 1;

			echo '<div class="pagination alternate" style="text-align: center;"><ul>';
			$ant = $pagina - 1;
			$prox = $pagina + 1;
			if ($pagina == 1) echo '<li class="disabled"><a href="#">Anterior</a></li>';
			else echo "<li><a href='./index.php?pg=".$_GET[pg]."&pg2=".$_GET[pg2]."&cam=".$_GET[cam]."&op=".$_GET[op]."&campo=".$_GET[campo]."&pg3=".$ant."'>Anterior</a></li>";
			while ($i <= $total_pag) {
			  if ($pagina == $i)
				echo '<li class="active"> <a href="#">'.$i.'</a> </li>';
			  else
				echo "<li> <a href='./index.php?pg=".$_GET[pg]."&pg2=".$_GET[pg2]."&cam=".$_GET[cam]."&op=".$_GET[op]."&campo=".$_GET[campo]."&pg3=".$i."'>".$i."</a> </li>";
			  $i++;
			}
			if ($pagina < $total_pag) echo "<li><a href='./index.php?pg=".$_GET[pg]."&cam=".$_GET[cam]."&pg2=".$_GET[pg2]."&op=".$_GET[op]."&campo=".$_GET[campo]."&pg3=".$prox."'>Próximo</a></li>";
			else echo "<li class='disabled'><a href='#'>Próximo</a></li>";
			echo "</ul></div>";
			
			?>
          </div>
        </div>
  </div>
</div></div>