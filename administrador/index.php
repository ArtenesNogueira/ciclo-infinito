<?php
session_start();
if ($_POST['acao'] == "sim" && $_POST['pass1'] == "kamikaze1997" && $_POST['pass2'] == "kamikaze1997") {
	$_SESSION['admin'] = "sim";
	echo "<script>window.location.href='./resumo';</script>";
} else if ($_POST['acao'] == "sim" && ($_POST['pass1'] != "kamikaze1997" || $_POST['pass2'] != "kamikaze1997")) {
	$_SESSION['admin'] = "nao";
	echo "<script>alert('Senha errada!');</script>";
}

if ($_SESSION['admin'] != "sim") {
	echo "
	<center>
	<form method='post'>
	<input type='hidden' name='acao' value='sim'>
	<h3>Efetue o login</h3>
	<p>Informe senha 1: <input type='password' name='pass1' required></p>
	<p>Informe senha 2: <input type='password' name='pass2' required></p>
	<p><input type='submit' value='Entrar'></p>
	</form></center>
	";
	exit;
} else {
	
}
function tempoExecucao($start = null) {
    // Calcula o microtime atual
    $mtime = microtime(); // Pega o microtime
    $mtime = explode(' ',$mtime); // Quebra o microtime
    $mtime = $mtime[1] + $mtime[0]; // Soma as partes montando um valor inteiro

    if ($start == null) {
        // Se o parametro não for especificado, retorna o mtime atual
        return $mtime;
    } else {
        // Se o parametro for especificado, retorna o tempo de execução
        return round($mtime - $start, 2);
    }
}
define('mTIME', tempoExecucao());
include("../conectar_____.php");
?>
<!DOCTYPE html>
<html lang="pt-BR">
<head>
<title>Ciclo Infinito - Administração!</title>
<meta charset="UTF-8" />
<link rel="icon" href="./ico.ico" type="image/x-icon" />
<link rel="shortcut icon" href="./ico.ico" type="image/x-icon" />
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<link rel="stylesheet" href="./css/bootstrap.min.css" />
<link rel="stylesheet" href="./css/bootstrap-responsive.min.css" />
<link rel="stylesheet" href="./css/maruti-style.css" />
<link rel="stylesheet" href="./css/maruti-media.css" class="skin-color" />
<link rel="stylesheet" href="./css/uniform.css" />
<link rel="stylesheet" href="./css/select2.css" />
<link rel="stylesheet" href="./js/tcal.css" />
<script type="text/javascript" src="./js/tcal.js"></script> 
</head>
<?php flush(); ?>
<body>

<div id="header">
  <h1 style="background: url('../images/logo-small.png') no-repeat scroll 0 0 transparent;"><a href="./inicio">Ciclo Infinito</a></h1>
</div>

<div id="user-nav" class="navbar navbar-inverse">
  <ul class="nav">
    <li><a title="" href="./sair"><i class="icon icon-share-alt"></i> <span class="text">Sair</span></a></li>
  </ul>
</div>


<div id="sidebar2"><a href="#" class="visible-phone"><i class="icon icon-home"></i> Menu</a><ul>
    <li title="Resumo"> <a href="./resumo"><i class="icon icon-list"></i> <span>Resumo</span></a> </li>
    <li title="Usuários"> <a href="./usuarios-1"><i class="icon icon-user"></i> <span>Usuários</span></a> </li>
    <li title="Usuários Inativos"> <a href="./inativos-1"><i class="icon icon-user"></i> <span>Usuários Inativos</span></a> </li>
    <li title="Assinaturas"> <a href="./assinaturas-1"><i class="icon icon-star"></i> <span>Assinaturas</span></a> </li>
    <li title="Assinaturas Terminadas"> <a href="./assinaturasterminadas-1"><i class="icon icon-star"></i> <span>Assinaturas terminadas</span></a> </li>
    <li title="Comprovantes"> <a href="./comprovantes-1"><i class="icon icon-tags"></i> <span>Comprovantes</span></a></li>
    <li title="Rede"> <a href="./rede"><i class="icon icon-flag"></i> <span>Rede</span></a> </li>
    <li title="Log de acessos"> <a href="./log_acessos-0"><i class="icon icon-flag"></i> <span>Log de acessos</span></a> </li>
    <li title="Planos"> <a href="./planos-1"><i class="icon icon-star"></i> <span>Planos</span></a> </li>
    <li title="Pedidos de saque"> <a href="./pedidos_saque-1"><i class="icon icon-list"></i> <span>Pedidos de saque</span></a> </li>
  </ul>
</div>
<div id="content">
<?php
if (AntiSQL($_GET[pg]) == "")
	$pagina__ = "inicio";
else
	$pagina__ = AntiSQL($_GET[pg]);

if (file_exists("$pagina__.php")) {
if ($pagina__ == "")
	include("inicio.php");
else
	include("$pagina__.php");
}
?>
</div>
<script src="./js/excanvas.min.js"></script> 
<script src="./js/jquery.min.js"></script> 
<script src="./js/jquery.ui.custom.js"></script> 
<script src="./js/bootstrap.min.js"></script>  
<script src="./js/maruti.js"></script> 
<?php 
$tempo = tempoExecucao(mTIME);
?>
<div class="row-fluid">
  <div id="footer" class="span12"> 2013 &copy; Ciclo Infinito!<br><small><?php echo $tempo." segundos"; ?></small></div>
</div>

</body>
</html>
