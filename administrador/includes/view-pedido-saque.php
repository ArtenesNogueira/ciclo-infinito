<body>
  
  <div id="content-header">
    <div id="breadcrumb"> 
      <a href="./inicio" title="Voltar para o painel" class="tip-bottom"><i class="icon-home"></i>Painel</a>
      <a href="./inativos-1" class="current">Pedidos de saque</a>
    </div>
  </div>

  <div class="container-fluid">
    <div class="row-fluid">

      <div class="span12">
        <div class="widget-box">
          <div class="widget-title">
            <span class="icon"><i class="icon-th"></i></span> 
            <h5>Dia do saque</h5>
          </div>  
        </div>
        <div class="widget-content nopadding">
          <form method="POST">
            <select name="dia">
              <?php for($times = 1; $times <= 28; $times++): ?>
                <option value="<?php echo $times; ?>" <?php echo $diaSaque == $times ? 'selected' : ''; ?>><?php echo $times; ?></option>
              <?php endfor; ?>
            </select>
            <input type="submit" value="Salvar" class="btn btn-default">
          </form>
        </div>
      </div>

      <div class="span12">
        <div class="widget-box">
          <div class="widget-title">
            <span class="icon"><i class="icon-th"></i></span> 
            <h5>Pedidos de saque</h5>
          </div>
          <div class="widget-content nopadding">

            <?php if($pedidos):?>

              <table class="table table-bordered data-table">
                
                <thead>
                  <tr>
                    <th>Número</th>
                    <th>Usuário</th>
                    <th>Valor do saque</th>
                    <th>Status</th>
                    <th>Data do pedido</th>
                    <th colspan="2">Ações</th>
                  </tr>
                </thead>

                <tbody>

                  <?php foreach ($pedidos as $pedido):?>

                    <tr class="gradeX">
                      
                      <td>#<?php echo $pedido['id']; ?></td>
                      <td><?php  echo $pedido['nome'] ?></td>
                      <td>R$<?php  echo $pedido['valor'] ?></td>
                      <td><span class="label label-warning">Pendente</span></td>
                      <td class="center"><?php echo $pedido['data'] ?></td>
                      
                      <form method="POST">
                        <input type="hidden" name="idPedido" value="<?php echo $pedido['id']; ?>">
                        <input type="hidden" name="justificativa">
                        <td> 
                          <button name="status" value="2" type="submit" class="btn btn-success" onclick="this.form.statusEscolhido = this.value;">Aprovar</button>
                        </td>
                        <td> 
                          <button name="status" value="1" type="submit" class="btn btn-danger" onclick="this.form.statusEscolhido = this.value;">Rejeitar</button>
                        </td>
                      </form>    

                    </tr>

                  <?php endforeach;?>

                </tbody>
              </table>

            <?php else:?>

              <div class="alert alert-warning">Nenhum pedido de saque pendente!</div>

            <?php endif;?>

          </div>
        </div>
      </div>
    </div>
  </div>

  <script type="text/javascript" src="./js/jquery.min.js"></script>
  <script type="text/javascript" src="./js/pedido-saque.js"></script>

</body>