<?php

	function getUsuariosInativos () {

		$connection = require(realpath(dirname(__FILE__)) . '/conectar.php');

		$sql = "

			SELECT
				usuario.id_patro,
				patrocinador.nome as nome_patrocinador,
				usuario.id,
				usuario.nome as nome_usuario,
				usuario.usuario as username,
				usuario.email,
				usuario.skype,
				DATE_FORMAT(usuario.data_cadastro, '%d/%m/%Y %H:%i:%s') as data_cadastro,
				matriz.id as id_matriz
			FROM usuarios usuario
			INNER JOIN usuarios patrocinador ON patrocinador.id = usuario.id_patro
			INNER JOIN matrizes matriz ON matriz.id_usuario = usuario.id
			WHERE matriz.status = 0;

		";

		$statement = $connection->query($sql);

		if (!$statement) {
			return false;
		}

		return $statement->fetchAll(PDO::FETCH_ASSOC);

	}