<?php 
		
	class ControllerPedidoSaque {
		
		private $mensagem;
		private $pedidoSaque;
		private $usuario;
		private $connection;

		function __construct() {
			
			$this->connection = require(realpath(dirname(__FILE__)) . '/conectar.php');

			require_once(realpath(dirname(__FILE__)) . '/pedido-saque.php');
			require_once(realpath(dirname(__FILE__)) . '/usuario.php');

			
			$this->pedidoSaque = new PedidoSaque($this->connection);
			$this->usuario     = new Usuario($this->connection);

		}

		public function atualizarDiaSaque($dados) {
			
			if (!isset($dados['dia'])) {
				return false;
			}

			$diaSaque = $dados['dia'];

			if (!$diaSaque) {
				return false;
			}

			if ($this->pedidoSaque->alterarDiaSaque($diaSaque)) {
				return true;
			} else {
				$this->mensagem = 'Erro ao atualizar o dia de saque';
				return false;
			}

		}

		public function atualizarStatusPedido($dados) {
		
			if (!isset($dados['idPedido']) || !isset($dados['status'])) {
				return false;
			}

			$idPedido = $dados['idPedido'];
			$status   = $dados['status'];

			if (!$idPedido || !$status) {
				return false;
			}

			$pedido = $this->pedidoSaque->obterPedido($idPedido);
			$saldo  = $this->usuario->saldo($pedido['id_usuario']);

			if ($pedido['valor'] > $saldo) {
				if ($this->pedidoSaque->rejeitarPedido($idPedido, 'Saldo insuficiente')) {
					return true;
				} else {
					$this->mensagem = 'Erro ao tentar rejeitar pedido de saque';
					return false;
				}
			}

			if ($status == 2) {
				
				if ($this->pedidoSaque->aprovarPedido($idPedido)) {

					if ($this->usuario->sacar($pedido['id_usuario'], $pedido['valor'])) {
						return true;
					} else {
						$this->mensagem = 'Erro ao realizar a baixa no saldo do usuário.';
						return false;
					}

				} else {
					$this->mensagem = 'Erro ao tentar aprovar o pedido de saque';
					return false;
				}

			} else {

				if (!isset($dados['justificativa'])) {
					$this->mensagem = 'Informe uma justificativa para a rejeição.';
					return false;	
				}

				$justificativa = $dados['justificativa'];

				if (!$justificativa) {
					$this->mensagem = 'Informe uma justificativa para a rejeição.';
					return false;		
				}

				if ($this->pedidoSaque->rejeitarPedido($idPedido, $justificativa)) {
					return true;
				} else {
					$this->mensagem = 'Erro ao tentar rejeitar pedido de saque';
					return false;
				}

			}

		}	

		public function mostrarPedidosPendentes() {
							
			global $pedidos;
			global $diaSaque;

			$pedidos  = $this->pedidoSaque->obterPedidosPendentes();
			$diaSaque = $this->pedidoSaque->obterDiaSaque();

			require_once(realpath(dirname(__FILE__)) . '/view-pedido-saque.php');

		}

		public function novoPedidoSaque($dados) {
			
			if (!isset($dados['idUsuario'])) {
				return false;
			}

			if (!isset($dados['valor'])) {
				return false;
			}

			$idUsuario = $dados['idUsuario'];
			$valor     = $dados['valor'];

			if (!$idUsuario || !$valor) {
				return false;
			}

			$saldoAtual = $this->usuario->saldo($idUsuario);

			if ($valor > $saldoAtual) {
				$this->mensagem = 'Saldo insuficiente. Realize o saque de um valor menor';
				return false;
			}

			if ($this->pedidoSaque->novo($idUsuario, $valor)) {
				return true;
			} else {
				$this->mensagem = 'Erro ao registrar o pedido de saque';
				return false;
			}

		}

		public function mostrarPainelUsuario($idUsuario) {
			
			global $saldo;
			global $pedidos;
			global $podeSacar;
			global $diaSaque;

			$saldo    = $this->usuario->saldo($idUsuario);
			$pedidos  = $this->pedidoSaque->obterPedidosPendentesUsuario($idUsuario);

			$diaSaque  = $this->pedidoSaque->obterDiaSaque();
			$diaAtual  = date('d');
			$podeSacar = $diaSaque == $diaAtual;

			require_once(realpath(dirname(__FILE__)) . '/view-pedido-saque-usuario.php');

		}

		public function getMensagem() {
			return $this->mensagem;
		}

	}