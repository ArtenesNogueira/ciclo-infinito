<?php

	function renderButton ($id, $status) {

		if (empty($id) or empty($status)) {
			return '-';
		}

		if ($status === '1') {

			return '

				<form method="POST" action="inativos-1">

                     <button type="submit" class="btn btn-small btn-success" name="idMatriz" value="'.$id.'">Ativar</button>   

                </form>

			';

		} else {

			return '

				<button type="button" class="btn btn-small btn-success" disabled>Ativar</button>

			';

		}

	}