<?php 
	
	/**
	 * Controla as requisições do módulo do administrador.
	 */
	class ControllerAdministrador  {
		
		private $connection;

		function __construct() {
			$this->connection = require(realpath(dirname(__FILE__)) . '/conectar.php');
		}

		/**
		 * Mostra tela de assinaturas ativas.
		 */
		public function assinaturas() {
			
			require_once(realpath(dirname(__FILE__)) . '/matrizes.php');
			$matrizes = new Matrizes($this->connection);

			$assinaturas = $matrizes->assinaturas(true);

			require_once(realpath(dirname(__FILE__)) . '/view-assinaturas.php');

		}

		/**
		 * Mostra tela de assinaturas terminadas.
		 */
		public function assinaturasTerminadas() {
			
			require_once(realpath(dirname(__FILE__)) . '/matrizes.php');
			$matrizes = new Matrizes($this->connection);

			$assinaturas = $matrizes->assinaturas(false);

			require_once(realpath(dirname(__FILE__)) . '/view-assinaturas-terminadas.php');

		}

		/**
		 * Realiz a movimentação de uma assinatura.
		 * @param $dados dados para serem processados.
		 * @return true caso sucesso, false caso contrário.
		 */
		public function movimentar($dados) {
			
			require_once(realpath(dirname(__FILE__)) . '/matrizes.php');
			$matrizes = new Matrizes($this->connection);

			if (isset($dados['massa'])) {
				
				if (!isset($dados['valor']))
					return false;				

				$assinaturas = $matrizes->assinaturas(true);
				foreach ($assinaturas as $assinatura) {
					$matrizes->movimentar($assinatura['id_usuario'], $assinatura['id'], $dados['valor']);
					$matrizes->cancelarAssinatura($assinatura['id_usuario'], $assinatura['id']);
				}	

			} else {

				if (!isset($dados['idusuario']) || !isset($dados['idmatriz']) || !isset($dados['valor']))
					return false;

				$matrizes->movimentar($dados['idusuario'], $dados['idmatriz'], $dados['valor']);
				$matrizes->cancelarAssinatura($dados['idusuario'], $dados['idmatriz']);

			}

		}
	}