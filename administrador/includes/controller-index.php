<?php 

	class ControllerIndex {
		
		private $connection;

		function __construct () {
			$this->connection = require(realpath(dirname(__FILE__)) . '/conectar.php');
		}

		public function mostrarIndex($dados) {
			
			require(realpath(dirname(__FILE__)) . '/usuario.php');
			
			$usuarios = new Usuario($this->connection);
			$usuario = null;
			
			if (isset($dados['i']) && (!empty($dados['i']))) {

				$usuario = $usuarios->obterPorUsuario(strtolower(trim($dados['i'])));

			}

			if (!$usuario) {
				
				require_once(realpath(dirname(__FILE__)) . '/configuracao.php');
				$configuracoes = new configuracao($this->connection);
				$usuario = $configuracoes->obterUsuarioPadrao();

			}

			global $nomeusuario;
			$nomeusuario = $usuario['usuario'];
			
			require_once(realpath(dirname(__FILE__)) . '/view-index.php');

		}
		
	}