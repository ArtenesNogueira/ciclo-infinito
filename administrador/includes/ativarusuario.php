<?php
	
	/**
	* Classe usada para ativar um usuário.
	* @deprecated
	*/	
	class AtivarUsuario {

		private $connection;
		private $idMatriz;
		private $hoje;

		public function __construct ($idMatriz) {

			$this->connection = require(realpath(dirname(__FILE__)) . '/conectar.php');
			$this->idMatriz = $idMatriz;
			$this->hoje = date('Y-m-d H:i:s');

		}

		public function ativar () {

			$this->connection->beginTransaction();

			if (empty($this->idMatriz)) {
				$this->connection->rollBack();
				return 'Id da matriz não foi informada';
			}
			
			$matriz = $this->getMatriz();
			if (!$matriz) {
				$this->connection->rollBack();
				return 'Matriz não encontrada';
			}

			$temErro = $this->ativarUsuario($matriz);
			if ($temErro) {
				$this->connection->rollBack();
				return $temErro;
			}

			return false;

		}

		private function getMatriz () {

			$sqlMatriz = '

				SELECT 
					mat.id,
					mat.id_usuario
				FROM matrizes mat
				WHERE mat.status = 0 AND mat.id = :id

			';

			$statement = $this->connection->prepare($sqlMatriz);
			$statement->bindValue(':id', $this->idMatriz);
			$statement->execute();

			if ($statement->rowCount() === 1) {

				return $statement->fetch(PDO::FETCH_ASSOC);

			} 
			
			return false;

		}

		private function ativarUsuario ($matriz) {

			$sqlUpdateMatriz = "

				UPDATE matrizes 

				SET 
				data_liberacao = '{$this->hoje}',
				status = 1 

				WHERE id = {$matriz['id']} AND id_usuario = {$matriz['id_usuario']}

			";

			$sqlUpdateUsuario = "

				UPDATE usuarios 

				SET 
				data_liberacao = '{$this->hoje}',
				status = 2 

				WHERE id = {$matriz['id_usuario']}

			";

			if ($this->connection->exec($sqlUpdateMatriz) == false) {
				return 'Não foi possível atualizar a matriz atual do usuário';
			}

			if ($this->connection->exec($sqlUpdateUsuario) == false) {
				return 'Não foi possível ativar o usuário';
			}

			return false;

		}

		private function enviarEmail ($matriz) {

			$sqlUsuario = "

				SELECT *
				FROM usuarios
				WHERE id = :id

			";
			$statement = $this->connection->prepare($sqlUsuario);
			$statement->bindValue(':id', $matriz['id_usuario']);
			if ($statement->execute() === false) {
				return 'Não foi possível obter os dados do usuário para enviar o email';
			}
			$usuario = $statement->fetch(PDO::FETCH_ASSOC);

			$nome = $usuario['nome'];
			$email = $usuario['email'];
			$idMatriz = $matriz['id'];
			$nomeUsuario = $usuario['usuario'];
			$nomeCiclo = $this->getNomeCiclo($idMatriz);

			$assunto = "DOAÇÃO CONFIRMADA!";
			$headers = "MIME-Version: 1.0\r\n";
			$headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
			$headers .= "To: $nome <$email>\r\n";
			$headers .= "From: Ciclo Infinito <contato@cicloinfinito.com.br>\r\n";
			
			$mensagem = "
			<body>
			<style>
			body {
				margin: 0px;
				background: #EEE;
				font-family: Tahoma, Geneva, sans-serif;
				font-size: 16px;
				color: #999;
			}
			.conteudo {
				width: 620px;
				overflow: auto;
				margin: 20px;
			}
			.topo {
				text-align: center;
				padding: 20px;
				border-bottom: 1px solid #EEE;
				border: 10px solid #EAEAEA;
				background: #FFF;
			}
			.info {
				padding: 20px;
				border: 10px solid #EAEAEA;
				background: #FFF;
			}
				
			</style>
				<div class='conteudo'>
					<div class='topo'>
						<a href='http://cicloinfinito.com.br/'><img src='http://cicloinfinito.com.br/images/logo.png' width='288' height='56' border='0'></a>
					</div>
					<div class='info'>
						<center><p style='font-size: 115%;'>Olá $nome </p></center>
						<p>Sua doação para a matriz # $idMatriz ($nomeCiclo) foi confirmada.</p>
						<p>Link para indicação: <strong>http://www.cicloinfinito.com.br/$nomeUsuario</strong></p>
					  
					  <br><br>
					  <p><strong>Dados de acesso</strong><br>
						Usuário: $nomeUsuario<br>
						Senha: a mesma que você definiu no cadastro.<br>
						<br>
					  Tem dúvidas sobre como funciona o Ciclo Infinito? Entre em contato com o nosso suporte pelo o e-mail <strong>duvidas@cicloinfinito.com.br</strong></p>
					  <p>Não deixa essa oportunidade passar, seja você o diferencial!</p>
					  <p>Skype: <strong>cicloinfinito</strong></p>
					</div>
					<center><p style='font-size: 80%;'>Mensagem automática, não responda!</strong></p></center>
				</div>
			</body>
			";

			//@editado: removido por indisponibilidade do serviço
			//$this->send($email,'contato@cicloinfinito.com.br', $assunto, $mensagem);

		}

		private function getNomeCiclo ($id) {

			$sqlCiclo = "

				SELECT ciclo 
				FROM matrizes 
				WHERE id = $id 
				ORDER BY id ASC

			";

			$statement = $this->connection->query($sqlCiclo);

			if ($statement->rowCount() > 0) {

				$ciclo = $statement->fetch(PDO::FETCH_ASSOC);
				$ciclo = intval($ciclo['ciclo']);

				if ($ciclo <= 4) 
					return "Basic $ciclo";
				else if ($ciclo === 5)
					return "Advanced 1";
				else if ($ciclo === 6)
					return "Advanced 2";
				else if ($ciclo === 7)
					return "Plus 1";
				else if ($ciclo === 8)
					return "Plus 2";

			} 

			return "NENHUM";

		}

		private function send($para, $de, $assunto, $mensagem) {
			require_once(realpath(dirname(__FILE__)) . '/../../turbosmtp/lib/TurboApiClient.php');
			$email = new Email();
			$email->setFrom($de);
			$email->setToList($para);
			$email->setSubject($assunto);
			$email->setHtmlContent("$mensagem");
			$turboApiClient = new TurboApiClient("contato@worldfinances.com.br", "pr260891");
			$turboApiClient->sendEmail($email);
		}

		private function liberaParaPatrocinador ($idUsuario) {

			
			
		}

	}