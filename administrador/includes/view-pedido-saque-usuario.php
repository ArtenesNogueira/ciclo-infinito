<div id="da-content">
    <div class="da-container clearfix">
        <div id="da-sidebar-separator"></div>
        <div id="da-sidebar">
            <div id="da-sidebar-toggle"></div>
            <?php include_once("menu.php"); ?>                   
        </div>
		
		
		  
 <center> <table style="text-align: left;"
 border="1" cellpadding="2" cellspacing="2">
  <tbody>
    <tr style="font-weight: bold;" align="center">
      <td><span
 style="color: rgb(0, 0, 0); font-family: Calibri,sans-serif; font-size: 16px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: normal; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 1; word-spacing: 0px; display: inline ! important; float: none;"><span
 style="font-weight: bold;">OBSERVA&Ccedil;&Otilde;ES</span></span></td>
    </tr>
    <tr>
      <td><span
 style="color: rgb(0, 0, 0); font-family: Calibri,sans-serif; font-size: 16px; font-style: normal; font-variant: normal; letter-spacing: normal; line-height: normal; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 1; word-spacing: 0px; display: inline ! important; float: none; font-weight: bold;">-
Saques apenas dia 5.</span><br
 style="text-indent: 0px; color: rgb(0, 0, 0); font-family: Calibri,sans-serif; font-size: 16px; font-style: normal; font-variant: normal; letter-spacing: normal; line-height: normal; text-align: start; text-transform: none; white-space: normal; widows: 1; word-spacing: 0px; font-weight: bold;">
      <span
 style="color: rgb(0, 0, 0); font-family: Calibri,sans-serif; font-size: 16px; font-style: normal; font-variant: normal; letter-spacing: normal; line-height: normal; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 1; word-spacing: 0px; display: inline ! important; float: none; font-weight: bold;">-
O Valor estar&aacute; em sua conta bancaria cadastrada em ate 5
Dias uteis.</span>
<br><span
 style="color: rgb(0, 0, 0); font-family: Calibri,sans-serif; font-size: 16px; font-style: normal; font-variant: normal; letter-spacing: normal; line-height: normal; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 1; word-spacing: 0px; display: inline ! important; float: none; font-weight: bold;">-
Valor mínimo para saque: R$150,00.</span>
           <div
 style="text-indent: 0px; color: rgb(0, 0, 0); font-family: Calibri,sans-serif; font-size: 16px; font-style: normal; font-variant: normal; letter-spacing: normal; line-height: normal; text-align: start; text-transform: none; white-space: normal; widows: 1; word-spacing: 0px; font-weight: bold;">-
E cobrado uma taxa de 5% do valor retirado.</div>
      </td>
    </tr>
    <tr>
      <td><a href="http://alfarenda.com/escritoriovirtual/minhaconta#conta1" class="btn btn-primary btn-block">Adicionar conta Bancária</a></td>
    </tr>
  </tbody>
</table>
</center>
  
  
  <br>
		
		
        <div id="da-content-wrap" class="clearfix">
        	<div id="da-content-area"> 
                <div class="row-fluid">

                    <div class="span4">
                        <div class="da-panel">
                        	<div class="da-panel collapsible">
                        	   <div class="da-panel-header">
                                    <span class="da-panel-title">
                                        <i class="icon-youtube"></i>
                                        Saque
                                    </span>
                                </div>
                                <div class="da-panel-content with-padding">
                                    <div class="da-gallery photoSwipe">
                                      
                                        <h4>Saldo</h4>
                                        <h3>
                                            <span>R$</span>
                                            <?php echo $saldo ? $saldo : '0.00';?>
                                        </h3>

                                        <hr>

                                        <h4>Realizar saque</h4>

                                        <?php if ($podeSacar): ?>
                                            
                                            <form method="POST" id="formSaque">
                                                <input type="hidden" value="<?php echo $idUsuario; ?>" name="idUsuario">
                                                <div class="form-group">
                                                    <label for="textValor">Valor:</label>
                                                    <input type="text" id="textValor" name="valor" class="form-control">    
                                                </div>
                                                <br>
                                                <input type="submit" value="Sacar" class="btn btn-warning btn-block">

                                            </form>

                                        <?php else: ?>

                                            <span>Saque disponível somente no dia <?php echo $diaSaque; ?></span>

                                        <?php endif ?>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="span8">
                        <div class="da-panel">
                            <div class="da-panel collapsible">
                               <div class="da-panel-header">
                                    <span class="da-panel-title">
                                        <i class="icon-youtube"></i>
                                        Pedidos de saque
                                    </span>
                                </div>
                                <div class="da-panel-content with-padding">
                                    <div class="da-gallery photoSwipe">

                                        <?php if ($pedidos): ?>
                                            <table id="da-ex-datatable-default" class="da-table">
                                                <thead>
                                                    <tr>
                                                        <th>Número</th>
                                                        <th>Valor</th>
                                                        <th>Data</th>
                                                        <th>Status</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php foreach ($pedidos as $pedido): ?>
                                                        <tr>
                                                            <td>#<?php echo $pedido['id'] ?></td>
                                                            <td>R$<?php echo $pedido['valor'] ?></td>
                                                            <td><?php echo $pedido['data'] ?></td>
                                                            <td>
                                                                <?php switch ($pedido['status']) {
                                                                    case '1':
                                                                        echo '<span class="label label-danger" rel="tooltip" data-placement="right" title="'.$pedido['justificativa'].'">Rejeitado</span>';
                                                                        break;
                                                                    case '2':
                                                                        echo '<span class="label label-success">Aprovado</span>';
                                                                        break;
                                                                    default:    
                                                                        echo '<span class="label label-warning">Pendente</span>';
                                                                        break;
                                                                } ?>
                                                            </td>
                                                        </tr>
                                                    <?php endforeach ?>
                                                </tbody>
                                            </table>    
                                        <?php else: ?>
                                            <div class="alert alert-info">Nenhuma solicitação encontrada</div>
                                        <?php endif ?>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="./../administrador/js/jquery.min.js"></script>
<script type="text/javascript" src="./../administrador/js/jquery.maskMoney.js"></script>
<script type="text/javascript" src="./../administrador/js/pedido-saque.js"></script>