<?php 

	
	class Plano {
		
		private $connection;

		function __construct($connection) {
			$this->connection = $connection;
		}

		function obterTodosPlanos(){

			$sql = 'SELECT * FROM planos';

			$statement = $this->connection->query($sql);

			return $statement->fetchAll(PDO::FETCH_ASSOC);

		}

		function obterPlano ($id) {

			$sql = 'SELECT * FROM planos WHERE id = :id';

			$statement = $this->connection->prepare($sql);
			$statement->bindParam(':id', $id);
			$statement->execute();

			return $statement->fetch(PDO::FETCH_ASSOC);

		}

		function salvarPlano () {

			if (!isset($_POST['id']) || !isset($_POST['nome']) || !isset($_POST['descricao']) || !isset($_POST['valor'])) {
				echo '<script>alert("Execução inválida do script")</script>';
				return false;
			}

			if (empty($_POST['nome'])) {
				echo '<script>alert("Informe um nome para o plano")</script>';
				return false;
			}

			if (empty($_POST['valor'])) {
				echo '<script>alert("O valor do plano deve ser informado")</script>';
				return false;
			}

			if (!is_numeric($_POST['valor'])) {
				echo '<script>alert("Valor informado não é um número")</script>';
				return false;
			}

			$connection = require(realpath(dirname(__FILE__)) . '/conectar.php');

			$id        = $_POST['id'];
			$nome      = $_POST['nome'];
			$descricao = $_POST['descricao'];
			$valor     = $_POST['valor'];

			if (intval($id) != 0) {
				
				$sql = 'UPDATE planos SET nome = :nome, descricao = :descricao, valor = :valor WHERE id = :id;';

				$statement = $connection->prepare($sql);
				$statement->bindParam(':id'       , $id);
				$statement->bindParam(':nome'     , $nome);
				$statement->bindParam(':descricao', $descricao);
				$statement->bindParam(':valor'    , $valor);

				if (!$statement->execute()) {

					echo '<script>alert("Erro ao atualizar dados do plano")</script>';
					return false;

				}

			} else {

				$sql = 'INSERT INTO planos(nome, descricao, valor) VALUES (:nome, :descricao, :valor);';

				$statement = $connection->prepare($sql);
				$statement->bindParam(':nome'     , $nome);
				$statement->bindParam(':descricao', $descricao);
				$statement->bindParam(':valor'    , $valor);

				if (!$statement->execute()) {

					echo '<script>alert("Erro ao cadastrar o plano")</script>';
					return false;

				}

				$id = $connection->lastInsertId();

			}

			return $id;
		}

		function excluirPlano () {

			if (!isset($_POST['id']) || empty($_POST['id'])) {
				echo '<script>alert("Execução inválida do script")</script>';
				return false;
			}

			$id = $_POST['id'];

			$sql = 'DELETE FROM planos WHERE id = :id;';

			$statement = $this->connection->prepare($sql);
			$statement->bindParam(':id', $id);

			if (!$statement->execute()) {

				echo '<script>alert("Erro ao excluir o plano")</script>';

			}

			return true;

		}

	}