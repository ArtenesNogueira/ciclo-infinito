<?php 

	class Usuario {
		
		private $connection;

		function __construct($connection) {

			$this->connection = $connection;

		}

		public function obter($id) {
			
			$sql = 'SELECT * FROM usuarios WHERE id = :id';
			$statement = $this->connection->prepare($sql);
			$statement->bindValue(':id', $id);
			$statement->execute();

			return $statement->fetch(PDO::FETCH_ASSOC);

		}

		public function obterTodosUsuarios () {
			
			$sql = 'SELECT * FROM usuarios';
			$statement = $this->connection->query($sql);
			return $statement->fetchAll(PDO::FETCH_ASSOC);

		}

		public function obterPorUsuario($usuario) {
			
			$sql = 'SELECT * FROM usuarios WHERE usuario = :usuario AND status = 2 LIMIT 1';
			$statement = $this->connection->prepare($sql);
			$statement->bindValue(':usuario', $usuario);
			$statement->execute();

			return $statement->fetch(PDO::FETCH_ASSOC);

		}

		public function saldo($id) {
			
			$sql = '

				SELECT SUM(valor) as saldo
				FROM movimentacoes
				WHERE id_usuario = :id;

			';
			$statement = $this->connection->prepare($sql);
			$statement->bindValue(':id', $id);
			$statement->execute();

			return $statement->fetch(PDO::FETCH_ASSOC)['saldo'];

		}

		public function sacar($id, $valor) {
			
			$valor = $valor * (-1);

			$sql = '

				INSERT INTO movimentacoes (
					id_usuario,
					valor,
					data
				) VALUES (
					:id,
					:valor,
					NOW()
				);

			';
			$statement = $this->connection->prepare($sql);
			$statement->bindValue(':id', $id);
			$statement->bindValue(':valor', $valor);

			return $statement->execute();

		}

		public function doar($idMatriz) {
			
			$sql = '

				SELECT
				m.id, 
				p.valor_plano,
				mavo.id_usuario as id_avo,
				mpai.id_usuario as id_pai
				FROM planos_em_uso p
				INNER JOIN matrizes m    ON m.id    = p.id_matriz
				INNER JOIN matrizes mavo ON mavo.id = m.id_avo
				INNER JOIN matrizes mpai ON mpai.id = m.id_pai
				WHERE p.id_matriz = :idMatriz
				AND m.status = 1;

			';
			$statement = $this->connection->prepare($sql);
			$statement->bindValue(':idMatriz', $idMatriz);
			$statement->execute();
			
			$matriz = $statement->fetch(PDO::FETCH_ASSOC);			
			$valor  = floatval($matriz['valor_plano']);

			$valorProAvo = $valor * 0.90;
			$valorProPai = $valor * 0.10;

			$resposta = true;

			$sql = '

				INSERT INTO movimentacoes (
					id_usuario,
					valor,
					id_matriz_origem,
					data
				) VALUES (
					:idUsuario,
					:valor,
					:idMatrizOrigem,
					NOW()
				);

			';

			$statement = $this->connection->prepare($sql);
			$statement->bindValue(':idUsuario'      , $matriz['id_avo']);
			$statement->bindValue(':valor'          , $valorProAvo);
			$statement->bindValue(':idMatrizOrigem' , $matriz['id']);

			$resposta = $statement->execute() ? $resposta : false;

			$statement->bindValue(':idUsuario'      , $matriz['id_pai']);
			$statement->bindValue(':valor'          , $valorProPai);
			$statement->bindValue(':idMatrizOrigem' , $matriz['id']);			
			
			$resposta = $statement->execute() ? $resposta : false;

			return $resposta;

		}

	}