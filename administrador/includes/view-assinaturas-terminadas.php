
<?php 

  /**
   * View da tela de assinaturas terminadas.
   */

 ?>

<body>
  <div id="content-header">
    <div id="breadcrumb"> <a href="./inicio" title="Voltar para o painel" class="tip-bottom"><i class="icon-home"></i> Painel</a><span class="current">Assinaturas terminadas</span></div>
  </div>
  <div class="container-fluid">
    <div class="row-fluid">
      <div class="span12">
        <div class="widget-box">
          <div class="widget-title">
             <span class="icon"><i class="icon-th"></i></span> 
              <h5>Assinaturas Terminadas</h5>
          </div>
          <?php if ($assinaturas): ?>

            <div class="widget-content nopadding">
              <table class="table table-bordered data-table">
                <thead>
                  <tr>
                    <th>Id da assinatura</th>
                    <th>Usuário</th>
                    <th>Data da assinatura</th>
                    <th>Data da término</th>
                    <th>Plano</th>
                    <th>Valor</th>
                    <th>Saldo</th>
                    <th>Franquia consumida</th>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach ($assinaturas as $assinatura): ?>
                    <tr>
                      <td><?php echo $assinatura['id']; ?></td>
                      <td><?php echo $assinatura['nome']; ?></td>
                      <td><?php echo $assinatura['data_cadastro']; ?></td>
                      <td><?php echo $assinatura['data_liberacao']; ?></td>
                      <td><?php echo $assinatura['descricao']; ?></td>
                      <td><?php echo $assinatura['valor_plano']; ?></td>
                      <td><?php echo $assinatura['saldo']; ?></td>
                      <td><?php echo number_format($assinatura['franquia_consumida'], 2); ?>%</td>
                    </tr>
                  <?php endforeach ?>
                </tbody>
              </table>  

          <?php else: ?>

              <div class="alert alert-warning">Nenhuma assinatura encotrada!</div>

          <?php endif ?>

          </div>
        </div>
    </div>
  </div>
</div>
</body>