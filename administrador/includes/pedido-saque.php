<?php 

	class PedidoSaque {
		
		private $connection;

		function __construct($connection) {

			$this->connection = $connection;

		}

		public function obterPedido($id) {
		
			$sql = 'SELECT * FROM pedidos_saque WHERE id = :id';
			$statement = $this->connection->prepare($sql);
			$statement->bindValue(':id', $id);
			$statement->execute();

			return $statement->fetch(PDO::FETCH_ASSOC);
			
		}		

		public function obterPedidosPendentes() {
			
			$sql = '

				SELECT 
				pedidos_saque.id,
				usuarios.nome,
				pedidos_saque.valor,
				DATE_FORMAT(pedidos_saque.data, \'%d/%m/%Y %h:%i:%s\') as data
				FROM pedidos_saque
				INNER JOIN usuarios ON usuarios.id = pedidos_saque.id_usuario
				WHERE pedidos_saque.status = 0;

			';
			$statement = $this->connection->query($sql);
			return $statement->fetchAll(PDO::FETCH_ASSOC);

		}

		public function obterPedidosPendentesUsuario($idUsuario) {
			
			$sql = '

				SELECT 
				pedidos_saque.id,
				usuarios.nome,
				pedidos_saque.valor,
				DATE_FORMAT(pedidos_saque.data, \'%d/%m/%Y %h:%i:%s\') as data,
				pedidos_saque.status,
				pedidos_saque.justificativa
				FROM pedidos_saque
				INNER JOIN usuarios ON usuarios.id = pedidos_saque.id_usuario
				WHERE pedidos_saque.id_usuario = :idUsuario
				ORDER BY pedidos_saque.id DESC
				LIMIT 15;

			';
			$statement = $this->connection->prepare($sql);
			$statement->bindValue(':idUsuario', $idUsuario);
			$statement->execute();
			return $statement->fetchAll(PDO::FETCH_ASSOC);

		}

		public function aprovarPedido($id) {
			
			return $this->alterarStatus($id, 2);

		}

		public function rejeitarPedido($id, $justificativa) {
			
			return $this->alterarStatus($id, 1, $justificativa);

		}

		private function alterarStatus($id, $status, $justificativa = null) {
			
			$sql = '

				UPDATE pedidos_saque
				SET
				status = :status,
				justificativa = :justificativa,
				data_atualizacao = NOW()
				WHERE id = :id;

			';
			$statement = $this->connection->prepare($sql);
			$statement->bindValue(':status', $status);
			$statement->bindValue(':justificativa', $justificativa);
			$statement->bindValue(':id', $id);

			return $statement->execute();
		}

		public function obterDiaSaque() {
			
			$sql = 'SELECT * FROM config';
			$statement = $this->connection->query($sql);
			$config = $statement->fetch(PDO::FETCH_ASSOC);
			return $config['dia_saque'];

		}

		public function alterarDiaSaque($diaSaque) {
			
			$sql = '

				UPDATE config
				SET dia_saque = :diaSaque;

			';
			$statement = $this->connection->prepare($sql);
			$statement->bindValue(':diaSaque', $diaSaque);

			return $statement->execute();

		}

		public function novo($idUsuario, $valor) {
			
			$sql = '

				INSERT INTO pedidos_saque (
					id_usuario,
					valor,
					status,
					data,
					data_atualizacao
				) VALUES (
					:idUsuario,
					:valor,
					0,
					NOW(),
					NOW()
				);			

			';

			$statement = $this->connection->prepare($sql);
			$statement->bindValue(':idUsuario', $idUsuario);
			$statement->bindValue(':valor', $valor);
			return $statement->execute();

		}

	}