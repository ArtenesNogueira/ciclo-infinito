<?php 
	
	/**
	 * Controller para o módulo de escritório virtual
	 */
	class ControllerEscritorioVirtual {
		
		/**
		 * @var PDO
		 */
		private $connection;

		public function __construct(){
			$this->connection = require(realpath(dirname(__FILE__)) . '/conectar.php');
		}

		/**
		 * Renderiza o painel inicial do escritório virtual.
		 * @param $dados dados que serão usados para processamento. Deve conter a chave 'ide'.
		 */
		public function painel($dados) {
			
			require_once(realpath(dirname(__FILE__)) . '/matrizes.php');
			$matrizes = new Matrizes($this->connection);

			require_once(realpath(dirname(__FILE__)) . '/usuario.php'); 
			$usuario = new Usuario($this->connection);

			$planos = $matrizes->planos($dados['ide']);

			$saldo = $usuario->saldo($dados['ide']);

			require_once(realpath(dirname(__FILE__)) . '/view-escritorio-painel.php');

		}

		/**
		 * Realiza o processamento de assinar um plano para o usuário.
		 * @param $dados dados que serão usados para processamento. Deve conter as chaves 'idPlano' e 'ide'. 
		 */
		public function assinarPlano($dados) {
			
			//Validação--------------------------------------------------------------------------------------
			if (!isset($dados['ide']) || !isset($dados['idPlano'])) {
				echo '<script>window.location.href="http://alfarenda.com/escritoriovirtual/pagamento.html";</script>';
				return false;
			}

			if (empty($dados['ide'] || empty($dados['idPlano']))) {
				echo '<script>window.location.href="http://alfarenda.com/escritoriovirtual/pagamento.html";</script>';
				return false;
			}

			//Processamento---------------------------------------------------------------------------------
			require_once(realpath(dirname(__FILE__)) . '/matrizes.php');
			$matrizes = new Matrizes($this->connection);

			$idUsuario    = $dados['ide'];
			$idPlano      = $dados['idPlano'];
			$mensagem     = "";

			if ($matrizes->inserir($idUsuario, $idPlano)) {
				$mensagem = 'Você assinou um novo plano, faça seu pagamento para se ativar!';
			} else {
				$mensagem = 'Houve um erro ao assinar o plano que você escolheu. Tente novamente mais tarde.';
			}

			echo '
				<script>
					alert("'.$mensagem.'");
					window.location.href="http://alfarenda.com/escritoriovirtual/pagamento.html";
				</script>
			';
			
		}
		
	}