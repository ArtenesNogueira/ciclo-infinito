<?php 

	/**
	 * Processa a movimentação em uma assinatura.
	 */

	require_once(realpath(dirname(__FILE__)) . '/controller-administrador.php');
	$controller = new ControllerAdministrador();
	$controller->movimentar($_POST);
	header('Location: ./../assinaturas');