<?php 

	
	class Configuracao {
		
		private $connection;

		function __construct($connection) {
			$this->connection = $connection;
		}

		public function obterUsuarioPadrao() {
			
			$sql = 'SELECT id_usuario_padrao FROM config';
			$statement = $this->connection->query($sql);
			$id = $statement->fetch(PDO::FETCH_ASSOC)['id_usuario_padrao'];

			$usuario = new Usuario($this->connection);
			return $usuario->obter($id);

		}

	}