<?php 
	
	/**
	*	Classe que representa a entidade Matriz. 
	*/	
	class Matrizes {

		private $connection;
		
		function __construct ($connection) {
			$this->connection = $connection;
		}

		/**
		 * Insere uma nova matriz e um novo plano em uso.	
		 * @param $idUsuario Id do usuário associado à matriz.
		 * @param $idPlano Id do plano escolhido para a matriz.
		 * @return id da matriz criada, false caso algo de errado.
		 */
		public function inserir($idUsuario, $idPlano) {
			
			$sql = '

				INSERT INTO matrizes (
					id_usuario,
					data_cadastro,
					status
				) VALUES (
					:idUsuario,
					NOW(),
					0
				);

			';
			$statement = $this->connection->prepare($sql);

			if (!$statement)
				return false;

			$statement->bindValue(':idUsuario', $idUsuario);
			
			if (!$statement->execute())
				return false;

			$idNovaMatriz = $this->connection->lastInsertId();

			$sql = '

				INSERT INTO planos_em_uso (
					id_matriz,
					id_plano,
					valor_plano
				) VALUES (
					:idMatriz,
					:idPlano,
					(SELECT valor FROM planos WHERE id = :idPlano)
				);

			';
			$statement = $this->connection->prepare($sql);

			if (!$statement)
				return false;

			$statement->bindValue(':idMatriz', $idNovaMatriz);
			$statement->bindValue(':idPlano' , $idPlano);
			
			if (!$statement->execute())
				return false;

			return $idNovaMatriz;

		}

		/**
		 * Pega todos os planos (matrizes) de um usuário.
		 * @param $idUsuario id do usuário associado aos planos.
		 * @return array com os planos encontrados. Um array vazio também pode indicar algum erro.
		 */
		public function planos($idUsuario) {
			
			$sql = '
				SELECT 
					m.id,
					u.nome,
					DATE_FORMAT(m.data_cadastro, \'%d/%m/%Y %k:%i:%S\') as data_cadastro,
					pu.valor_plano,
					p.descricao,
					m.status,
					(SELECT IFNULL(SUM(valor),0.00) FROM movimentacoes WHERE id_usuario = u.id AND id_matriz_origem = m.id) as saldo
				FROM matrizes m
				INNER JOIN planos_em_uso pu ON pu.id_matriz = m.id
				INNER JOIN planos p ON p.id = pu.id_plano 
				INNER JOIN usuarios u ON u.id = m.id_usuario
				WHERE u.id = :idUsuario AND
				(m.status = 0 OR m.status = 1 OR m.status = 3)
				ORDER BY m.data_cadastro DESC;
			';
			$stmt = $this->connection->prepare($sql);

			if (!$stmt) 
				return array();

			$stmt->bindValue(':idUsuario', $idUsuario);

			if (!$stmt->execute())
				return array();
			
			$assinaturas = $stmt->fetchAll(PDO::FETCH_ASSOC);

			foreach ($assinaturas as &$assinatura) {
				$assinatura['franquia_consumida'] = $this->franquiaConsumida($idUsuario, $assinatura['id']);
			}

			return $assinaturas;

		}

		/**
		 * Pega todas as assinaturas ativas ou inativas do banco.
		 * @param $ativo define se será pego as assinaturas ativas ou inativas.
		 * @return array com lista de assinaturas. Uma lista vazia pode indicar um erro na execução.
		 */
		public function assinaturas($ativo) {
			
			$ativo = $ativo ? '1' : '3';

			$sql = '
				SELECT 
					m.id,
					u.id as id_usuario,
					u.nome,
					DATE_FORMAT(m.data_cadastro, \'%d/%m/%Y %k:%i:%S\') as data_cadastro,
					DATE_FORMAT(m.data_liberacao, \'%d/%m/%Y %k:%i:%S\') as data_liberacao,
					pu.valor_plano,
					p.descricao,
					m.status,
					(SELECT IFNULL(SUM(valor),0.00) FROM movimentacoes WHERE id_usuario = u.id AND id_matriz_origem = m.id) as saldo
				FROM matrizes m
				INNER JOIN planos_em_uso pu ON pu.id_matriz = m.id
				INNER JOIN planos p ON p.id = pu.id_plano 
				INNER JOIN usuarios u ON u.id = m.id_usuario
				WHERE m.status = '.$ativo.'
				ORDER BY m.data_cadastro DESC;
			';
			$stmt = $this->connection->query($sql);

			if (!$stmt) 
				return array();

			$assinaturas = $stmt->fetchAll(PDO::FETCH_ASSOC);

			foreach ($assinaturas as &$assinatura) {
				$assinatura['franquia_consumida'] = $this->franquiaConsumida($assinatura['id_usuario'], $assinatura['id']);
			}

			unset($assinatura);

			return $assinaturas;

		}

		/**
		 * Calcula o saldo e o valor de uma assinatura.
		 * @param $idUsuario id do usuário da assinatura.
		 * @param $isMatriz id da matriz (assinatura).
		 * @return array com os valores do saldo e valor da assinatura.
		 */
		public function valoresAssinatura($idUsuario, $idMatriz) {
			
			$sql = '
				SELECT
					(SELECT IFNULL(SUM(valor), 0) FROM movimentacoes WHERE id_usuario = :idUsuario AND id_matriz_origem = :idMatriz) as saldo,
					(SELECT IFNULL(valor_plano, 0) FROM planos_em_uso WHERE id_matriz = :idMatriz) as valor;
			';

			$stmt = $this->connection->prepare($sql);

			if (!$stmt) {
				return $franquiaConsumida;
			}

			$stmt->bindValue(':idUsuario', $idUsuario);
			$stmt->bindValue(':idMatriz', $idMatriz);

			if (!$stmt->execute()) {
				return $franquiaConsumida;
			}

			return $stmt->fetch(PDO::FETCH_ASSOC);

		}

		/**
		 * Calcula a porcentagem de franquia consumida de um plano.
		 * @param $idUsuario id do usuário do plano.
		 * @param $isMatriz id da matriz (plano).
		 * @return float em porcentagem de franquia consumida de um plano. Zero pode indicar um erro na execução.
		 */
		private function franquiaConsumida($idUsuario, $idMatriz) {
			
			$franquiaConsumida = 0;

			$resultado = $this->valoresAssinatura($idUsuario, $idMatriz);
			$saldo  = floatval($resultado['saldo']);
			$limite = floatval($resultado['valor']) * 2;

			if ($saldo <= 0) {
				return $franquiaConsumida;
			}

			$franquiaConsumida = ($saldo * 100) / $limite;

			return $franquiaConsumida;

		}

		/**
		 * Cria uma nova movimentação em uma conta.
		 * @param $idUsuario id do usuário para fazer a movimentação.
		 * @param $idMatriz matriz do qual a movimentação é proveniente.
		 * @param $valor valor a ser movimentado.
		 * @return true em caso de sucesso, false caso contrário.
		 */
		public function movimentar($idUsuario, $idMatriz, $valor) {
			
			$valor = floatval($valor);

			$sql = 'INSERT INTO movimentacoes VALUES (null, :idUsuario, :valor, :idMatriz, NOW());';
			$stmt = $this->connection->prepare($sql);

			if (!$stmt) {
				return false;
			}

			$stmt->bindValue(':idUsuario', $idUsuario);
			$stmt->bindValue(':valor', $valor);
			$stmt->bindValue(':idMatriz', $idMatriz);

			return $stmt->execute();

		}

		/**
		 * Cancela uma assinatura caso sua franquia já tenha sido alcançada.
		 * @param $idUsuario id do usuário dono da assinatura.
		 * @param $idMatriz id da assinatura (matriz).
		 * @return true em caso de sucesso, false caso contrário.
		 */
		public function cancelarAssinatura($idUsuario, $idMatriz) {
			
			$resultado = $this->valoresAssinatura($idUsuario, $idMatriz);
			$saldo  = floatval($resultado['saldo']);
			$limite = floatval($resultado['valor']) * 2;

			if ($saldo >= $limite) {
				
				$sql = 'UPDATE matrizes SET status = 3, data_liberacao = NOW() WHERE id = :idMatriz';
				$stmt = $this->connection->prepare($sql);
				if (!$stmt)
					return false;
				$stmt->bindValue(':idMatriz', $idMatriz);
				return $stmt->execute();

			}

			return true;

		}

		function obterMatrizes ($idUsuario) {

			$sql = '

				SELECT *
				FROM matrizes 
				WHERE id_usuario = :idUsuario AND (status = 0 OR status = 1) 
				ORDER BY id ASC

			';

			$statement = $this->connection->prepare($sql);
			$statement->bindValue(':idUsuario', $idUsuario);
			$statement->execute();

			return $statement->fetchAll(PDO::FETCH_ASSOC);

		}

		function obterPaginacaoMatriz ($idUsuario, $idMatrizAtual) {

			$dados = array(
				'matrizAtual'    => null,
				'idMatrizAnterior' => null,
				'idMatrizProxima'  => null
			);

			$matrizes = $this->obterMatrizes($idUsuario);

			for ($index = 0; $index < count($matrizes); $index++) { 
				
				if ($matrizes[$index]['id'] == $idMatrizAtual) {
					
					$dados['matrizAtual'] = $matrizes[$index];

					if (isset($matrizes[$index - 1])) {
						$dados['idMatrizAnterior'] = $matrizes[$index - 1]['id'];
					}

					if (isset($matrizes[$index + 1])) {
						$dados['idMatrizProxima'] = $matrizes[$index + 1]['id'];
					}

					break;

				}

			}

			if (!$dados['matrizAtual'] && count($matrizes) > 0) {

				$dados['matrizAtual'] = $matrizes[0];

				if (isset($matrizes[1])) {
					$dados['idMatrizProxima'] = $matrizes[1]['id'];
				}

			}

			return $dados;

		}

		public function liberarMatriz ($idMatriz) {

			if ($this->matrizFechou($idMatriz)) {
				
				if (!$this->atualizarMatrizFechada($idMatriz)) {
					return 'Não foi possível atualizar a matriz do pai/avo';
				}

			}

			return false;

		}	

		private function matrizFechou ($idMatriz) {

			$sql = '

				SELECT *
				FROM matrizes
				WHERE id = :idMatriz
				AND status = 1
				AND pos1 <> 0
				AND pos2 <> 0
				AND pos3 <> 0
				AND pos4 <> 0
				AND pos5 <> 0
				AND pos6 <> 0;

			';
			$statement = $this->connection->prepare($sql);
			$statement->bindValue(':idMatriz', $idMatriz);
			$statement->execute();

			return $statement->rowCount() > 0;

		}	

		private function atualizarMatrizFechada ($idMatriz) {

			$sql = '

				UPDATE matrizes
				SET 
				data_liberacao = NOW(),
				status = 2
				WHERE id = :idMatriz;

			';
			$statement = $this->connection->prepare($sql);
			$statement->bindValue(':idMatriz', $idMatriz);
			
			return $statement->execute();

		}



		/**
		 *
		 *	Obtem lista de matrizes por usuário e por plano. 
		 *
		 *	@param $idUsuario id do usuário para busca.
		 *	@param $idPlano id do plano para busca.
		 *	@return array com o resultado da pesquisa.
		 */
		public function obterMatrizPorUsuarioEPlano($idUsuario, $idPlano) {
			
			$sql = '

				SELECT
					m.id,
					m.id_avo,
					m.id_pai,
					m.id_usuario,
					m.data_cadastro,
					m.data_liberacao,
					m.status,
					m.ciclo,
					m.pos1,
					m.pos2,
					m.pos3,
					m.pos4,
					m.pos5,
					m.pos6,
					m.basic1,
					peu.id as id_plano_em_uso,
					peu.id_plano,
					peu.valor_plano
				FROM matrizes m
				INNER JOIN planos_em_uso peu ON peu.id_matriz = m.id
				WHERE m.id_usuario = :idUsuario
				AND peu.id_plano = :idPlano;

			';

			$statement = $this->connection->prepare($sql);
			$statement->bindValue(':idUsuario', $idUsuario);
			$statement->bindValue(':idPlano', $idPlano);

			if (!$statement->execute()) {
				return array();
			}

			$matrizes = $statement->fetchAll(PDO::FETCH_ASSOC);

			return $matrizes;

		}

		/**
		 * 
		 *	Atualiza um plano em uso de uma matriz.
		 *
		 *	@param $idPlanoEmUso id da instância do plano em uso a ser atualizada.
		 *	@param $idPlano o id do novo plano.
		 *	@param $valor o novo valor do plano.
		 *	@return true se algum registro foi salvo, false caso contrário.
		 */
		public function atualizarPlanoEmUso($idPlanoEmUso, $idPlano, $valor) {
			
			$sql = '

				UPDATE planos_em_uso
				SET 
					id_plano = :idPlano,
					valor_plano = :valorPlano
				WHERE id = :idPlanoEmUso;

			';

			$statement = $this->connection->prepare($sql);
			$statement->bindValue(':idPlano', $idPlano);
			$statement->bindValue(':valorPlano', $valor);
			$statement->bindValue(':idPlanoEmUso', $idPlanoEmUso);

			if (!$statement->execute()) {
				return false;
			}

			return $statement->rowCount() > 0;

		}

		/**
		 * 
		 *	Cria uma matriz padrão, onde o avo e pai da matriz é ela mesma.
		 *
		 *	@param $idUsuario id do usuário a ser inserido na matriz.
		 *	@param $idPlano id do plano associado à matriz.
		 *	@return true para sucesso, false caso contrário.
		 */
		public function criarMatrizPadrao($idUsuario, $idPlano) {
			
			$sql = '

				INSERT INTO matrizes VALUES (
					null,
					0,
					0,
					:idUsuario,
					NOW(),
					NOW() + INTERVAL 1 DAY,
					2,
					0,
					0,
					0,
					0,
					0,
					0,
					0,
					0
				)

			';

			$statement = $this->connection->prepare($sql);
			$statement->bindValue(':idUsuario', $idUsuario);

			if (!$statement->execute()) {
				return false;
			}

			$idMatriz = $this->connection->lastInsertId();

			$sql = '

				UPDATE matrizes
				SET
					id_avo = :idMatriz,
					id_pai = :idMatriz
				WHERE id = :idMatriz;

			';

			$statement = $this->connection->prepare($sql);
			$statement->bindValue(':idMatriz', $idMatriz);

			if (!$statement->execute()) {
				return false;
			}

			$sql = '

				INSERT INTO planos_em_uso VALUES (
					null,
					:idMatriz,
					:idPlano,
					(SELECT valor FROM planos WHERE id = :idPlano)
				);

			';

			$statement = $this->connection->prepare($sql);
			$statement->bindValue(':idMatriz', $idMatriz);
			$statement->bindValue(':idPlano', $idPlano);

			if (!$statement->execute()) {
				return false;
			}

			return true;

		}

		public function obtemMatrizPai($idUsuario, $idPlano) {
				
			$sql = '

				SELECT matrizPai.id, matrizPai.pos1, matrizPai.pos2
				FROM usuarios filho
				INNER JOIN usuarios pai ON pai.id = filho.id_patro
				INNER JOIN matrizes matrizPai ON matrizPai.id_usuario = pai.id
				INNER JOIN planos_em_uso planosPai ON planosPai.id_matriz = matrizPai.id
				INNER JOIN planos planoFilho ON planoFilho.id = planosPai.id_plano
				WHERE 
				filho.id = :idUsuario AND 
				planoFilho.id = :idPlano AND 
				planoFilho.valor = planosPai.valor_plano AND 
				(matrizPai.pos1 = 0 OR matrizPai.pos2 = 0) AND
				matrizPai.status = 1
				ORDER BY matrizPai.id DESC LIMIT 1

			';
			$statement = $this->connection->prepare($sql);
			$statement->bindValue(':idUsuario', $idUsuario);
			$statement->bindValue(':idPlano'  , $idPlano);
			$statement->execute();
			return $statement->fetchAll(PDO::FETCH_ASSOC);

		}

		public function pesquisaMatrizPai($idPlano) {
			
			$sql = '

				SELECT * FROM 
				matrizes m
				INNER JOIN planos_em_uso peu ON peu.id_matriz = m.id
				WHERE
				(m.pos1 = 0 OR m.pos2 = 0) AND
				peu.id_plano = :idPlano
				LIMIT 1;

			';
			$statement = $this->connection->prepare($sql);
			$statement->bindValue(':idPlano'  , $idPlano);
			$statement->execute();
			return $statement->fetch(PDO::FETCH_ASSOC);

		}

		public function obtemMatrizAvo ($idUsuario, $idPlano) {
			
			$sql = '

				SELECT matrizAvo.id, matrizAvo.pos3, matrizAvo.pos4, matrizAvo.pos5, matrizAvo.pos6
				FROM usuarios neto
				INNER JOIN usuarios pai ON pai.id = neto.id_patro
				INNER JOIN usuarios avo ON avo.id = pai.id_patro
				INNER JOIN matrizes matrizAvo ON matrizAvo.id_usuario = avo.id
				INNER JOIN planos_em_uso planosAvo ON planosAvo.id_matriz = matrizAvo.id
				INNER JOIN planos planoNeto ON planoNeto.id = planosAvo.id_plano
				WHERE 
				neto.id = :idUsuario AND 
				planoNeto.id = :idPlano AND 
				planoNeto.valor = planosAvo.valor_plano AND 
				(matrizAvo.pos3 = 0 OR matrizAvo.pos4 = 0 OR matrizAvo.pos5 = 0 OR matrizAvo.pos6 = 0) AND
				matrizAvo.status = 1
				ORDER BY matrizAvo.id DESC LIMIT 1;

			';
			$statement = $this->connection->prepare($sql);
			$statement->bindValue(':idUsuario', $idUsuario);
			$statement->bindValue(':idPlano'  , $idPlano);
			$statement->execute();
			return $statement->fetchAll(PDO::FETCH_ASSOC);

		}

		public function pesquisaMatrizAvo($idPlano, $idMatriz) {
			
			$sql = '

				SELECT * FROM 
				matrizes m
				INNER JOIN planos_em_uso peu ON peu.id_matriz = m.id
				WHERE
				(m.pos3 = 0 OR m.pos4 = 0 OR m.pos5 = 0 OR m.pos6 = 0) AND
				peu.id_plano = :idPlano AND
				m.id = :idMatriz
				LIMIT 1;

			';
			$statement = $this->connection->prepare($sql);
			$statement->bindValue(':idPlano'  , $idPlano);
			$statement->bindValue(':idMatriz'  , $idMatriz);
			$statement->execute();
			return $statement->fetch(PDO::FETCH_ASSOC);

		}

		/**
		 * Ativar uma matriz, mudando seu status para 1.
		 * @param $id id da matriz a ser ativada.
		 * @return true se foi bem sucedido, false caso contrário.
		 */
		public function ativar($id) {
			
			$sql = 'UPDATE matrizes SET data_liberacao = NOW(), status = 1 WHERE id = :id';
			$stmt = $this->connection->prepare($sql);
			if (!$stmt) 
				return false;

			$stmt->bindValue(':id', $id);
			
			return $stmt->execute();

		}

	}