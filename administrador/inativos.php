<?php
  
  /********************************
  *
  * @editado: adicionado seção
  * para exibir usuário inativos em algum ciclo
  *
  *********************************/

  if (isset($_POST['idMatriz'])) {

    $idMatriz = htmlspecialchars($_POST['idMatriz']);

    if (!empty($idMatriz)) {

      require_once(realpath(dirname(__FILE__)) . '/includes/matrizes.php');
      $connection = require(realpath(dirname(__FILE__)) . '/includes/conectar.php');
      $matrizes = new Matrizes($connection);
      
      if (!$matrizes->ativar($idMatriz)) {
        
        echo "
          <script>
            alert('Erro na ativação do usuário.');
          </script>
        ";        

      }

      echo '<script>window.location.href="./inativos";</script>';

    }

  }

?>

<body>
  
  <div id="content-header">
    <div id="breadcrumb"> 
      <a href="./inicio" title="Voltar para o painel" class="tip-bottom"><i class="icon-home"></i>Painel</a>
      <a href="./inativos-1" class="current">Usuários Inativos</a>
    </div>
  </div>

  <div class="container-fluid">
    <div class="row-fluid">
      <div class="span12">
        <div class="widget-box">
          <div class="widget-title">
            <span class="icon"><i class="icon-th"></i></span> 
            <h5>Usuários Inativos</h5>
          </div>
          <div class="widget-content nopadding">

            <?php
             
              require_once(realpath(dirname(__FILE__)) . '/includes/get-usuarios-inativos.php');
              $usuarios = getUsuariosInativos();

            ?>

            <?php if($usuarios):?>

              <table class="table table-bordered data-table">
                
                <thead>
                  <tr>
                    <th>Patrocinador</th>
                    <th>Nome</th>
                    <th>E-mail</th>
                    <th>Skype</th>
                    <th>Data de cadastro</th>
                    <th>Ações</th>
                  </tr>
                </thead>

                <tbody>

                  <?php foreach ($usuarios as $usuario):?>

                    <tr class="gradeX">
                      
                      <td><u><a href="./ver_usuario-<?= $usuario['id_patro'] ?>"><?= $usuario['nome_patrocinador'] ?></a></u></td>
                      <td><u><a href="./ver_usuario-<?= $usuario['id'] ?>"><?= "{$usuario['nome_usuario']} ({$usuario['username']})" ?></a></u></td>
                      <td><?= $usuario['email'] ?></td>
                      <td><?= $usuario['skype'] ?></td>
                      <td class="center"><?= $usuario['data_cadastro'] ?></td>
                      <td> 

                        <?php
                          require_once(realpath(dirname(__FILE__)) . '/includes/botaoativarusuario.php');
                          echo renderButton($usuario['id_matriz'], '1');
                        ?>
                            
                      </td>
                    </tr>

                  <?php endforeach;?>

                </tbody>
              </table>

            <?php else:?>

              <div class="alert alert-warning">Nenhum usuário inativo encontrado!</div>

            <?php endif;?>

          </div>
        </div>
      </div>
    </div>
  </div>
</body>