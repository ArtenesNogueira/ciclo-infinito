$("tr form[method='POST']").submit(function(){

      var status = this.statusEscolhido;
      var mensagem = status == 2 ? 'Tem certeza que deseja aprovar esse pedido?' : 'Tem certeza que deseja rejeitar esse pedido?';
      var temCerteza = confirm(mensagem);

      if (!temCerteza) {
        return false;  
      }

      if (status == 2) {
        return true;
      } else {

        var justificativa = prompt('Qual a justificativa da rejeição?', 'Sem motivos');

        if (!justificativa) {
          alert('Informe uma justificativa para a rejeição');
          return false;
        }

        this.justificativa.value = justificativa;
        return true;
      }

    });

$("#formSaque").submit(function(){

  if (this.valor.value <= 0) {
    alert('Informe um valor para o saque.');
    return false;
  }

  return confirm('Deseja sacar R$' + this.valor.value + '?');

});

$('#textValor').maskMoney({thousands: '', decimal: '.'});