/**
 * JS utilizado na tela de assinaturas ativas.
 **/

window.onload = inicializar();

function inicializar () {
	
	jQuery.getScript('./js/jquery.maskMoney.js', function () {
	    jQuery('input[name=valor]').maskMoney({thousands: '', decimal: '.', allowNegative: true});
	});

	jQuery('#form-acao-massa').submit(function(){

		if (!this.valor.value) {

			alert("Informe um valor para movimentar!");
			return false;

		}

		return confirm("Deseja movimentar o valor de " + this.valor.value + " em todas as assinaturas ativas?");

	});

	jQuery('form.form-acao-individual').submit(function(){

		if (!this.valor.value) {

			alert("Informe um valor para movimentar!");
			return false;

		}

		return confirm("Deseja movimentar o valor de " + this.valor.value + " no plano número " + this.idmatriz.value + "?");

	});

}