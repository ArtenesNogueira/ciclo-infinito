
$(document).ready(function(){

	var login = $('#loginform');
	var recover = $('#recoverform');
	var pagar = $('#pagar');
	var confirmar = $('#confirmar');
	var speed = 400;

	$('#to-recover').click(function(){
		$("#loginform").slideUp();
		$("#pagar").slideUp();
		$("#confirmar").slideUp();
		$("#recoverform").fadeIn();
	});
	
	$('#to-pagar').click(function(){
		$("#loginform").slideUp();
		$("#pagar").fadeIn();
		$("#confirmar").slideUp();
		$("#recoverform").slideUp();
	});
	
	$('.to-confirmar').click(function(){
		$("#loginform").slideUp();
		$("#confirmar").fadeIn();
		$("#pagar").slideUp();
		$("#recoverform").slideUp();
	});
	
	$('.to-login').click(function(){
		$("#recoverform").hide();
		$("#pagar").hide();
		$("#loginform").fadeIn();
	});
	
	
	$('#to-login').click(function(){
	
	});
    
    if($.browser.msie == true && $.browser.version.slice(0,3) < 10) {
        $('input[placeholder]').each(function(){ 
       
        var input = $(this);       
       
        $(input).val(input.attr('placeholder'));
               
        $(input).focus(function(){
             if (input.val() == input.attr('placeholder')) {
                 input.val('');
             }
        });
       
        $(input).blur(function(){
            if (input.val() == '' || input.val() == input.attr('placeholder')) {
                input.val(input.attr('placeholder'));
            }
        });
    });

        
        
    }
});