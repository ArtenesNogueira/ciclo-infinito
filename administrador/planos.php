<?php
    
    require(realpath(dirname(__FILE__)) . '/includes/planos.php');
    require(realpath(dirname(__FILE__)) . '/includes/matrizes.php');
    require(realpath(dirname(__FILE__)) . '/includes/configuracao.php');
    require(realpath(dirname(__FILE__)) . '/includes/usuario.php');
    $connection = require(realpath(dirname(__FILE__)) . '/includes/conectar.php'); 
    $plano = new Plano($connection);
    $matrizes = new Matrizes($connection);
    $configuracoes = new Configuracao($connection);

    if (isset($_POST)) {
        
        if (isset($_POST['salvar'])) {
            
            $idPlano = $plano->salvarPlano();

            if ($idPlano) {

                $usuarioPadrao = $configuracoes->obterUsuarioPadrao();
                $listaMatrizes = $matrizes->obterMatrizPorUsuarioEPlano($usuarioPadrao['id'], $_POST['id']);
                
                if (count($listaMatrizes) > 0) {
                    
                    foreach ($listaMatrizes as $matriz) {
                        
                        $matrizes->atualizarPlanoEmUso($matriz['id_plano_em_uso'], $idPlano, $_POST['valor']);

                    }

                } else {

                    $matrizes->criarMatrizPadrao($usuarioPadrao['id'], $idPlano);

                }

                echo '<script>window.location.href="./planos";</script>';

            }

        }

        if (isset($_POST['excluir'])) {
            
            if ($plano->excluirPlano()) {

                echo '<script>window.location.href="./planos";</script>';
                
            }

        }

    }

?>


<script src="./js/jquery.min.js"></script>

<div id="content-header">
    <div id="breadcrumb"> <a href="./inicio" title="Voltar para o painel" class="tip-bottom"><i class="icon-home"></i> Painel</a></div>
</div> 


<div class="row-fluid"> 
    <div class="span6">
      <div class="widget-box">
        <div class="widget-title"><span class="icon"><i class="icon-tasks"></i></span>
            <h5>Dados do plano</h5>
        </div>
        <div class="widget-content nopadding">
          <table class="table table-bordered" style="font-size: 12px;">
            <tbody>

                <?php

                    $id = 0;
                    $nome = '';
                    $descricao = '';
                    $valor = '';
                    
                    if (isset($_POST['editar'])) {
                        $planoEncontrado = $plano->obterPlano($_POST['id']);
                        $id        = $planoEncontrado['id'];
                        $nome      = $planoEncontrado['nome'];
                        $descricao = $planoEncontrado['descricao'];
                        $valor     = $planoEncontrado['valor'];
                    }

                ?>

                <form action="./planos" method="POST">
                
                    <input type="hidden" id="id" value="<?php echo $id ?>" name="id">

                    <tr>
                        <th>Nome</th>
                        <td><input id="nome" maxlength="60" name="nome" value="<?php echo $nome ?>"></td>
                    </tr>
                    <tr>
                        <th>Descrição</th>
                        <td><textarea id="descricao" rows="4" cols="50" maxlength="1000" name="descricao"><?php echo $descricao ?></textarea></td>
                    </tr>
                    <tr>
                        <th>Valor</th>
                        <td><input id="valor" name="valor" value="<?php echo $valor ?>"></td>
                    </tr>
                    <tr>
                        <th colspan="2">
                            <input type="submit" value="Salvar" name="salvar">
                        </th>
                    </tr>

                </form>
            </tbody>
        </table>
    </div>
</div>
</div>

<div class="span6"> 
    <div class="row-fluid">
      <div class="widget-box">
        <div class="widget-title"><span class="icon"><i class="icon-tasks"></i></span>
          <h5>Planos cadastrados</h5>
      </div>
      <div class="widget-content nopadding" id="planos-cadastrados">

        <?php 

        $planosEncontrados = $plano->obterTodosPlanos($connection);

        ?>

        <?php if($planosEncontrados): ?>

        <table class="table table-bordered" style="font-size: 12px;">
            <thead>
                <tr>
                    <th>Nome</th>
                    <th>Valor</th>
                    <th colspan="2">Opções</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($planosEncontrados as $plano): ?>

                <tr>
                    <td><?php echo $plano['nome'] ?></td>
                    <td><?php echo $plano['valor'] ?></td>
                    <td>
                        <form action="./planos" method="POST">
                            <input type="hidden" value="<?php echo $plano['id'] ?>" name="id">
                            <input type="submit" name="editar" value="Editar">
                        </form>
                    </td>
                    <td>
                        <form action="./planos" method="POST" id="formExcluir">
                            <input type="hidden" value="<?php echo $plano['id'] ?>" name="id">
                            <input type="submit" name="excluir" value="Excluir">
                        </form>
                    </td>
                </tr>   

            <?php endforeach; ?>
        </tbody>    
    </table>

<?php else:?>

    <p>Nenhum plano encontrado.</p>

<?php endif;?>

</div>
</div>
</div>
</div>

</div>

<script type="text/javascript" src="./js/planos.js"></script>