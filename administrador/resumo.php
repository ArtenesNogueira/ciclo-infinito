<?php
function TotalUsuario($status) {
	if ($status != "-") {
		$cri = "AND status='$status'";
	}
	$sql = mysql_query("SELECT id FROM usuarios WHERE id > '0' $cri");
	return mysql_num_rows($sql);
}
function TotalUsuariosSemMatriz($tipo) {
	$sql = mysql_query("SELECT id FROM usuarios ORDER BY id DESC");
	$total = 0;
	while ($dados = mysql_fetch_array($sql)) {
		if ($tipo == "nao") $cri = "AND (status<>'3' OR status<>'2')"; else $cri = "";
		$sql2 = mysql_query("SELECT id FROM matrizes WHERE id_usuario='".$dados[id]."' $cri");
		if (!mysql_num_rows($sql2)) $total++;
	}
	return $total;
}
function TotalMatrizes($status,$ciclo) {
	if ($status != "-") {
		$cri2 = "AND status='$status'";
	}
	if ($ciclo != "-") {
		$cri1 = "AND ciclo='$ciclo'";
	}
	$sql = mysql_query("SELECT id FROM matrizes WHERE id > '0' $cri1 $cri2");
	return mysql_num_rows($sql);
}
function TotalComp($status) {
	if ($status != "-") {
		$cri = "AND status='$status'";
	}
	$sql = mysql_query("SELECT id FROM comprovantes WHERE id > '0' $cri");
	return mysql_num_rows($sql);
}
?>
<script src="./js/jquery.min.js"></script>
<script language="javascript">
$(document).ready(function() {
	$("#online").load("online.php");
	var reload = function(){
		$("#online").fadeOut().load("online.php").fadeIn();
	};
	var auto_refresh = setInterval(reload, 10000);
});
</script>
  <div id="content-header">
    <div id="breadcrumb"> <a href="./inicio" title="Voltar para o painel" class="tip-bottom"><i class="icon-home"></i> Painel</a></div>
  </div> 
  
  
  <div class="span6"> 
    <div class="row-fluid">
      <div class="widget-box">
        <div class="widget-title"><span class="icon"><i class="icon-tasks"></i></span>
          <h5>Usuários</h5>
        </div>
        <div class="widget-content nopadding">
          <table class="table table-bordered" style="font-size: 12px;">
            <tbody>
                <tr>
                    <th>Total</th>
                    <td><?php echo TotalUsuario("-"); ?> usuários</td>
                </tr>
                <tr>
                    <th>E-mail pendente</th>
                    <td><?php echo TotalUsuario("0"); ?> usuários</td>
                </tr>
                <tr>
                    <th>E-mail confirmado</th>
                    <td><?php echo TotalUsuario("1"); ?> usuários</td>
                </tr>
                <tr>
                    <th>Usuários não posicionados</th>
                    <td><?php echo TotalUsuariosSemMatriz("nao"); ?> usuários</td>
                </tr>
                
                <tr>
                    <th>Usuários sem matriz</th>
                    <td><?php echo TotalUsuariosSemMatriz("sem"); ?> usuários</td>
                </tr>
                <tr>
                    <th>ATIVO</th>
                    <td><?php echo TotalUsuario("2"); ?> usuários</td>
                </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
    
    
    <div class="row-fluid">
      <div class="widget-box">
        <div class="widget-title"><span class="icon"><i class="icon-tasks"></i></span>
          <h5>Matrizes</h5>
        </div>
        <div class="widget-content nopadding">
          <table class="table table-bordered" style="font-size: 12px;">
            <tbody>
                <tr>
                    <th>-</th>
                    <th>#1</th>
                    <th>#2</th>
                    <th>#3</th>
                    <th>#4</th>
                    <th>#5</th>
                    <th>#6</th>
                    <th>#7</th>
                    <th>#8</th>
                </tr>
                <tr>
                    <th>Total</th>
                    <td><?php echo TotalMatrizes("-","1"); ?></td>
                    <td><?php echo TotalMatrizes("-","2"); ?></td>
                    <td><?php echo TotalMatrizes("-","3"); ?></td>
                    <td><?php echo TotalMatrizes("-","4"); ?></td>
                    <td><?php echo TotalMatrizes("-","5"); ?></td>
                    <td><?php echo TotalMatrizes("-","6"); ?></td>
                    <td><?php echo TotalMatrizes("-","7"); ?></td>
                    <td><?php echo TotalMatrizes("-","8"); ?></td>
                </tr>
                <tr style="color: blue">
                    <th>Abertas</th>
                    <td><?php echo TotalMatrizes("1","1"); ?></td>
                    <td><?php echo TotalMatrizes("1","2"); ?></td>
                    <td><?php echo TotalMatrizes("1","3"); ?></td>
                    <td><?php echo TotalMatrizes("1","4"); ?></td>
                    <td><?php echo TotalMatrizes("1","5"); ?></td>
                    <td><?php echo TotalMatrizes("1","6"); ?></td>
                    <td><?php echo TotalMatrizes("1","7"); ?></td>
                    <td><?php echo TotalMatrizes("1","8"); ?></td>
                </tr>
                <tr style="color: red">
                    <th>Pendentes</th>
                    <td><?php echo TotalMatrizes("0","1"); ?></td>
                    <td><?php echo TotalMatrizes("0","2"); ?></td>
                    <td><?php echo TotalMatrizes("0","3"); ?></td>
                    <td><?php echo TotalMatrizes("0","4"); ?></td>
                    <td><?php echo TotalMatrizes("0","5"); ?></td>
                    <td><?php echo TotalMatrizes("0","6"); ?></td>
                    <td><?php echo TotalMatrizes("0","7"); ?></td>
                    <td><?php echo TotalMatrizes("0","8"); ?></td>
                </tr>
                <tr style="color: green">
                    <th>Fechadas</th>
                    <td><?php echo TotalMatrizes("2","1"); ?></td>
                    <td><?php echo TotalMatrizes("2","2"); ?></td>
                    <td><?php echo TotalMatrizes("2","3"); ?></td>
                    <td><?php echo TotalMatrizes("2","4"); ?></td>
                    <td><?php echo TotalMatrizes("2","5"); ?></td>
                    <td><?php echo TotalMatrizes("2","6"); ?></td>
                    <td><?php echo TotalMatrizes("2","7"); ?></td>
                    <td><?php echo TotalMatrizes("2","8"); ?></td>
                </tr>
                <tr style="color: orange">
                    <th>Perdidas</th>
                    <td><?php echo TotalMatrizes("3","1"); ?></td>
                    <td><?php echo TotalMatrizes("3","2"); ?></td>
                    <td><?php echo TotalMatrizes("3","3"); ?></td>
                    <td><?php echo TotalMatrizes("3","4"); ?></td>
                    <td><?php echo TotalMatrizes("3","5"); ?></td>
                    <td><?php echo TotalMatrizes("3","6"); ?></td>
                    <td><?php echo TotalMatrizes("3","7"); ?></td>
                    <td><?php echo TotalMatrizes("3","8"); ?></td>
                </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <div class="row-fluid">
      <div class="widget-box">
        <div class="widget-title"><span class="icon"><i class="icon-tasks"></i></span>
          <h5>Comprovantes</h5>
        </div>
        <div class="widget-content nopadding">
          <table class="table table-bordered" style="font-size: 12px;">
            <tbody>
                <tr>
                    <th>Total</th>
                    <td><?php echo TotalComp("-"); ?></td>
                </tr>
                <tr>
                    <th>Pendentes</th>
                    <td><?php echo TotalComp("0"); ?></td>
                </tr>
                <tr>
                    <th>Recusados</th>
                    <td><?php echo TotalComp("2"); ?></td>
                </tr>
                <tr>
                    <th>Liberados</th>
                    <td><?php echo TotalComp("1"); ?></td>
                </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
    
    
    
    
    
  </div>
  
  
  
  
  <div class="span6"> 
    <div class="row-fluid">
      <div class="widget-box">
        <div class="widget-title"><span class="icon"><i class="icon-tasks"></i></span>
          <h5>Usuários online</h5>
        </div>
        <div class="widget-content nopadding" id="online">
          
        </div>
      </div>
    </div>
  </div>
    
</div>