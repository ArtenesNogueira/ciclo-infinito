<?php
session_start();
require_once("../conectar_____.php");

$sql = mysql_query("SELECT id, nome, email FROM usuarios WHERE status='1' ORDER BY id ASC");
$i = 1;
while ($dados = mysql_fetch_array($sql)) {
	$n = explode(" ",$dados[nome]);
	if (strlen($n[1]) <= 3)
		$nome = "$n[0] $n[1] $n[2]";
	else
		$nome = "$n[0] $n[1]";
	
	echo "$i - $nome<br>";
	$i++;
	
	$mensagem = '<!DOCTYPE HTML>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;" />

    <title>Ciclo Infinito</title>
	
	<link href="http://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700" rel="stylesheet" type="text/css">
	<link href="http://fonts.googleapis.com/css?family=Roboto:400,300,700" rel="stylesheet" type="text/css">
	
    <style type="text/css">
    
    	
	    body{
            width: 100%; 
            background-color: #ecf0f1; 
            margin:0; 
            padding:0; 
            -webkit-font-smoothing: antialiased;
        }
        
        p,h1,h2,h3,h4{
	        margin-top:0;
			margin-bottom:0;
			padding-top:0;
			padding-bottom:0;
        }
        
        span.preheader{display: none; font-size: 1px;}
        
        html{
            width: 100%; 
        }
        
        table{
            font-size: 14px;
            border: 0;
            border-collapse:collapse; 
            mso-table-lspace:0pt; 
            mso-table-rspace:0pt;
        }
        
        @media only screen and (max-width: 640px){
			.rounded-edg-bg{width: 440px !important; height: 10px !important;}
            .main-header{line-height: 28px !important; font-size: 17px !important;}
            .subheader{width: 390px !important;}
            .main-subheader{line-height: 28px !important;}
            
			.main-image img{width: 420px !important; height: auto !important;}
			
			.container580{width: 440px !important;}
			.container560{width: 420px !important;}
			.main-content{width: 438px !important;}
			.divider img{width: 438px !important; height: 10px !important;}
			
			.section-item{width: 420px !important;}
			.table-inside{width: 418px !important;}
            .section-img img{width: 420px !important; height: auto !important;}
            .table-rounded-edg-bg{width: 420px !important; height: 5px !important;}
			           
			
		}
		
		@media only screen and (max-width: 479px){
			
			.rounded-edg-bg{width: 280px !important; height: 10px !important;}
            .main-header{line-height: 28px !important; font-size: 17px !important;}
            .subheader{width: 260px !important;}
            .main-subheader{line-height: 28px !important;}
            
			.main-image img{width: 260px !important; height: auto !important;}
			
			.container580{width: 280px !important;}
			.container560{width: 260px !important;}
			.main-content{width: 278px !important;}
			.divider img{width: 278px !important; height: 10px !important;}
			
			.section-item{width: 260px !important;}
			.table-inside{width: 258px !important;}
            .section-img img{width: 260px !important; height: auto !important;}
			.table-rounded-edg-bg{width: 260px !important; height: 5px !important;}
			
			.icon img{width: 20px !important; height: 20px !important;}
			.cta-text{text-align: center !important; line-height: 24px !important;}
           
			
			
		}
		
	</style>
</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
	
	<table border="0" width="100%" cellpadding="0" cellspacing="0" bgcolor="ecf0f1">
		
		<tr bgcolor="2c3e50"><td height="6"></td></tr>
		<tr bgcolor="2c3e50">
			<td align="center" mc:edit="view-online" style="color: #ffffff; font-size: 12px; font-weight: 300; font-family: \'Roboto\', Arial, sans-serif;">
				<multiline>
					<a href="http://www.cicloinfinito.com.br/email.html" style="text-decoration: none; color: #f1c40f">Clique aqui</a> para abrir em outra página.
				</multiline>
			</td>	
		</tr>
		<tr bgcolor="2c3e50"><td height="6"></td></tr>
		
		<tr>
			<td align="center"  bgcolor="ffffff">
				<table width="580" cellpadding="0" cellspacing="0" border="0" class="container580">
					<tr><td height="20"></td></tr>
					<tr>
						<td>
							<table border="0" align="left" cellpadding="0" cellspacing="0" class="logo">
	                			<tr>
	                				<td align="center">
	                					<a href="#" style="display: block; border-style: none !important; border: 0 !important;"><img editable="true" mc:edit="logo" width="128" height="30" border="0" style="display: block; width: 128px; height: 30px" src="http://www.cicloinfinito.com.br/img/email/logo.png" alt="logo" /></a>
	                				</td>
	                			</tr>
	                		</table>		
	                		<table border="0" align="left" cellpadding="0" cellspacing="0" class="separator">
	                			<tr>
	                				<td height="20" width="20"></td>
	                			</tr>
	                		</table>
	                		<table border="0" align="right" cellpadding="0" cellspacing="0" class="socials">
	                			
	                			<tr>
	                				<td align="center" style="font-size: 13px; font-family: Helvetica, Arial, sans-serif;">
	                					<table border="0" align="center" cellpadding="0" cellspacing="0">
	                						<tr>
	                							<td>
	                								<a style="display: block; width: 24px; border-style: none !important; border: 0 !important;" href="https://www.facebook.com/CicloInfinito" target="_blank"><img editable="true" mc:edit="facebook" width="24" height="24" border="0" style="display: block;" src="http://www.cicloinfinito.com.br/img/email/facebook.png" alt="Facebook do Ciclo Infinito" /></a>		
	                							</td>
	                						</tr>
	                					</table>
	                				</td>
	                			</tr>
	                		</table>
						</td>
					</tr>
					<tr><td height="20"></td></tr>
					
				</table>
			</td>
		</tr>
		<tr bgcolor="e6e9ea"><td height="2"></td></tr>
		
		<tr><td height="40"><center style="font-family:\'Roboto\', Arial, sans-serif; line-height: 40px; font-size: 16px;">Olá '.$nome.'!</center></td></tr>
		
		<tr>
	        <td align="center">
	        	<img editable="true" src="http://www.cicloinfinito.com.br/img/email/top-rounded-bg.png" style="display: block; width: 580px; height: 10px; line-height: 1px;" width="580" height="10" border="0" class="rounded-edg-bg" />
	        </td>
        </tr>
        
		<tr mc:repeatable>
			<td align="center">
				<table border="0" align="center" width="580" cellpadding="0" cellspacing="0" bgcolor="e6e9ea" class="container580">
								
					<tr>
						<td align="center">
							<table border="0" align="center" width="578" cellpadding="0" cellspacing="0" bgcolor="ffffff" class="main-content">
								
								<tr>
									
									<td align="center">
										<table width="560" cellpadding="0" align="center" cellspacing="0" border="0" class="container560">
											<tr>
												<td align="center" class="main-image">
										        	<img editable="true" mc:edit="main-image" src="http://www.cicloinfinito.com.br/img/email/main-image.png" style="display: block; width: 560px; height: 300px; line-height: 1px;" width="560" height="300" border="0" alt="main image" />
										        </td>	
											</tr>
										</table>
									</td>
									
								</tr>
								
								
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		
		<tr mc:repeatable>
			<td>
				<table width="580" cellpadding="0" align="center" cellspacing="0" bgcolor="e6e9ea" border="0" class="container580">
					<tr>
						<td>
							<table border="0" align="center" width="578" cellpadding="0" cellspacing="0" bgcolor="ffffff" class="main-content">			
								<tr><td bgcolor="ffffff" height="35"></td></tr>	
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		
		<tr mc:repeatable>
			<td align="center">
				<table border="0" align="center" width="580" cellpadding="0" cellspacing="0" bgcolor="e6e9ea" class="container580">
					<tr>
						<td>
							<table border="0" align="center" width="578" cellpadding="0" cellspacing="0" bgcolor="ffffff" class="main-content">			
								
								<tr mc:repeatable >
			        				<td align="center">
			        					<table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container560">
			        						<tr>
			        							<td align="center" mc:edit="main-header" style="color: #424345; font-size: 22px; font-weight: bold; font-family: \'Roboto Slab\', Arial, sans-serif;" class="main-header">
						        					<multiline>
						        						Estamos decolando
						        					</multiline>
						        				</td>	
			        						</tr>
			        						<tr><td height="20"></td></tr>
			        						<tr>
			        							<td align="center" mc:edit="main-subtitle" style="color: #95a5a6; font-size: 14px; font-family: \'Roboto\', Arial, sans-serif;" class="main-header">
						        					<multiline>
						        						O MELHOR SISTEMA DE DOAÇÕES EM CICLOS DO BRASIL
						        					</multiline>
						        				</td>	
			        						</tr>
			        						<tr><td height="15"></td></tr>
			        						<tr>
			        							<td align="center">
			        								<table border="0" width="400" align="center" cellpadding="0" cellspacing="0" class="subheader">
			        									<tr>
			        										<td align="center" mc:edit="main-subheader" style="color: #bdc3c7; font-size: 14px; font-weight: 300; font-family: \'Roboto\', Arial, sans-serif; line-height: 28px;" class="main-subheader">
						        								<multiline>
						        									Esse projeto é baseado no Cód. Civil,  Art. 541 - DOAÇÕES. 
						        								</multiline>
						        							</td>			
			        									</tr>
			        								</table>
			        							</td>
			        						</tr>
			        						
			        						<tr><td height="25"></td></tr>
			        						
			        						<tr>
			        							<td align="center">
			        								<table border="0" width="230" align="center" cellpadding="0" cellspacing="0">
			        									<tr>
			        										<td align="left">
			        											<a href="http://www.cicloinfinito.com.br/CicloInfinito.ppsx" target="_blank" style="display: block; width: 120px; border-style: none !important; border: 0 !important;"><img editable="true" mc:edit="apre1" width="120" height="32" border="0" style="display: block; width: 120px; height: 32px;" src="http://www.cicloinfinito.com.br/img/email/apre1.png" alt="Apresentação em Power Point" /></a>
			        										</td>
                                                            <td align="center">
			        											 <a href="http://www.youtube.com/watch?v=P8OlgAVXzMA" target="_blank" style="display: block; width: 120px; border-style: none !important; border: 0 !important;"><img editable="true" mc:edit="video" width="90" height="32" border="0" style="display: block; width: 90px; height: 32px;" src="http://www.cicloinfinito.com.br/img/email/video.png" alt="Vídeo no YouTUBE" /></a>
			        										</td>
			        										<td align="right">
			        											<a href="http://www.cicloinfinito.com.br/?email" target="_blank" style="display: block; width: 90px; border-style: none !important; border: 0 !important;"><img editable="true" mc:edit="cadastro" width="90" height="32" border="0" style="display: block; width: 90px; height: 32px;" src="http://www.cicloinfinito.com.br/img/email/cadastro.png" alt="Cadastro" /></a>
			        										</td>
			        									</tr>
			        								</table>
			        							</td>
			        						</tr>
			        						
			        					</table>			
			        				</td>
			        			</tr>
								
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		
		<tr mc:repeatable>
			<td>
				<table width="580" cellpadding="0" align="center" cellspacing="0" bgcolor="e6e9ea" border="0" class="container580">
					<tr>
						<td>
							<table border="0" align="center" width="578" cellpadding="0" cellspacing="0" bgcolor="ffffff" class="main-content">			
								<tr><td bgcolor="ffffff" height="35"></td></tr>	
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		
		<tr mc:repeatable>
			<td>
				<table width="578" cellpadding="0" align="center" cellspacing="0" border="0" bgcolor="ffffff" class="main-content">
					<tr>
						<td class="divider">
							<img editable="false" mc:edit="divider" src="http://www.cicloinfinito.com.br/img/email/divider.png" style="display: block; width: 578px; height: 10px;" width="578" height="10" border="0" alt="divider" />
						</td>
					</tr>
				</table>
			</td>
		</tr>
		
		<tr mc:repeatable>
			<td>
				<table width="580" cellpadding="0" align="center" cellspacing="0" bgcolor="e6e9ea" border="0" class="container580">
					<tr>
						<td>
							<table border="0" align="center" width="578" cellpadding="0" cellspacing="0" bgcolor="ffffff" class="main-content">			
								<tr><td bgcolor="ffffff" height="35"></td></tr>	
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		
		<tr mc:repeatable>
			<td align="center">
				<table border="0" align="center" width="580" cellpadding="0" cellspacing="0" bgcolor="e6e9ea" class="container580">
					<tr>
						<td>
							<table border="0" align="center" width="578" cellpadding="0" cellspacing="0" bgcolor="ffffff" class="main-content">			
								
								<tr>
									<td align="center">
										<table width="560" align="center" cellpadding="0" cellspacing="0" border="0" class="container560">
											<tr>
			        							<td align="center" mc:edit="feature-header" style="color: #424345; font-size: 18px; font-weight: bold; font-family: \'Roboto Slab\', Arial, sans-serif;">
						        					<multiline>
						        						Ciclo Infinito
						        					</multiline>
						        				</td>	
			        						</tr>
			        						<tr><td height="25"></td></tr>
			        						<tr>
			        							<td align="center" mc:edit="feature-subheader" style="color: #95a0a6; font-size: 12px; font-weight: 300; font-family: \'Roboto\', Arial, sans-serif;">
						        					<multiline>
						        						www.cicloinfinito.com.br
						        					</multiline>
						        				</td>	
			        						</tr>
			        						<tr><td height="45"></td></tr>
											<tr>
												
												<td>
        					
						        					<table border="0" width="170" align="left" cellpadding="0" cellspacing="0" class="section-item">
														<tr>
															<td align="center" class="feature-img">
																<a href="#" style=" border-style: none !important; width: 180px; display: block; border: 0 !important;"><img editable="true" mc:edit="feature-image1" src="http://www.cicloinfinito.com.br/img/email/feature-img1.png" style="display: block; width: 60px; height: 60px;" width="60" height="60" border="0" alt="feature image1" /></a>
															</td>
														</tr>
														
														<tr><td height="25"></td></tr>
														
														<tr>
						        							<td align="center" mc:edit="feature-title1" style="color: #424345; font-size: 15px; font-weight: bold; font-family: \'Roboto Slab\', Arial, sans-serif;">
									        					<multiline>
									        						Fácil
									        					</multiline>
									        				</td>	
						        						</tr>
						        						
						        						<tr><td height="10"></td></tr>
						        						
						        						<tr>
						        							<td align="center" mc:edit="feature-subtitle1" style="color: #bdc3c7; font-size: 13px; font-weight: 300; font-family: \'Roboto\', Arial, sans-serif; line-height: 24px;">
						        								<multiline>
						        									Fácil de explicar, fácil para entender
						        								</multiline>
						        							</td>
						        						</tr>
														
													</table>
													
													<table border="0" width="10" align="left" cellpadding="0" cellspacing="0">
														<tr><td width="10" height="40"></td></tr>
													</table>
													
													<table border="0" width="170" align="left" cellpadding="0" cellspacing="0" class="section-item">
														<tr>
															<td align="center" class="feature-img">
																<a href="#" style=" border-style: none !important; width: 180px; display: block; border: 0 !important;"><img editable="true" mc:edit="feature-image2" src="http://www.cicloinfinito.com.br/img/email/feature-img2.png" style="display: block; width: 60px; height: 60px;" width="60" height="60" border="0" alt="feature image2"/></a>
															</td>
														</tr>
														
														<tr><td height="25"></td></tr>
														
														<tr>
						        							<td align="center" mc:edit="feature-title2" style="color: #424345; font-size: 15px; font-weight: bold; font-family: \'Roboto Slab\', Arial, sans-serif;">
									        					<multiline>
									        						Rápido
									        					</multiline>
									        				</td>	
						        						</tr>
						        						
						        						<tr><td height="10"></td></tr>
						        						
						        						<tr>
						        							<td align="center" mc:edit="feature-subtitle2" style="color: #bdc3c7; font-size: 13px; font-weight: 300; font-family: \'Roboto\', Arial, sans-serif; line-height: 24px;">
						        								<multiline>
						        									Maior agilidade sem travamentos na rede
						        								</multiline>
						        							</td>
						        						</tr>
														
													</table>
													
													<table border="0" width="10" align="left" cellpadding="0" cellspacing="0">
														<tr><td width="10" height="40"></td></tr>
													</table>
													
													<table border="0" width="170" align="left" cellpadding="0" cellspacing="0" class="section-item">
														<tr>
															<td align="center" class="feature-img">
																<a href="#" style=" border-style: none !important; width: 180px; display: block; border: 0 !important;"><img editable="true" mc:edit="feature-image3" src="http://www.cicloinfinito.com.br/img/email/feature-img3.png" style="display: block; width: 60px; height: 60px;" width="60" height="60" border="0" alt="feature image3" /></a>
															</td>
														</tr>
														
														<tr><td height="25"></td></tr>
														
														<tr>
						        							<td align="center" mc:edit="feature-title3" style="color: #424345; font-size: 15px; font-weight: bold; font-family: \'Roboto Slab\', Arial, sans-serif;">
									        					<multiline>
									        						Prático
									        					</multiline>
									        				</td>	
						        						</tr>
						        						
						        						<tr><td height="10"></td></tr>
						        						
						        						<tr>
						        							<td align="center" mc:edit="feature-subtitle3" style="color: #bdc3c7; font-size: 13px; font-weight: 300; font-family: \'Roboto\', Arial, sans-serif; line-height: 24px;">
						        								<multiline>
						        									Indique apenas 2 pessoas
						        								</multiline>
						        							</td>
						        						</tr>
														
													</table>
													
			        							</td>
													
											</tr>
										</table>
									</td>
								</tr>
								
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		
		<tr mc:repeatable>
			<td>
				<table width="580" cellpadding="0" align="center" cellspacing="0" bgcolor="e6e9ea" border="0" class="container580">
					<tr>
						<td>
							<table border="0" align="center" width="578" cellpadding="0" cellspacing="0" bgcolor="ffffff" class="main-content">			
								<tr><td bgcolor="ffffff" height="35"></td></tr>	
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		
		<tr mc:repeatable>
			<td>
				<table width="578" cellpadding="0" align="center" cellspacing="0" border="0" bgcolor="ffffff" class="main-content">
					<tr>
						<td class="divider">
							<img editable="false" mc:edit="divider" src="http://www.cicloinfinito.com.br/img/email/divider.png" style="display: block; width: 578px; height: 10px;" width="578" height="10" border="0" alt="divider" />
						</td>
					</tr>
				</table>
			</td>
		</tr>
		
		<tr mc:repeatable>
			<td>
				<table width="580" cellpadding="0" align="center" cellspacing="0" bgcolor="e6e9ea" border="0" class="container580">
					<tr>
						<td>
							<table border="0" align="center" width="578" cellpadding="0" cellspacing="0" bgcolor="ffffff" class="main-content">			
								<tr><td bgcolor="ffffff" height="35"></td></tr>	
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
				
		<tr mc:repeatable>
			<td align="center">
				<table border="0" align="center" width="580" cellpadding="0" cellspacing="0" bgcolor="e6e9ea" class="container580">
					<tr>
						<td>
							<table border="0" align="center" width="578" cellpadding="0" cellspacing="0" bgcolor="ffffff" class="main-content">			
								
								<tr mc:repeatable >
			        				<td align="center">
										<table width="560" align="center" cellpadding="0" cellspacing="0" border="0" class="container560">
											<tr mc:repeatable>
							    				<td>	
							    					
							    					<table border="0" width="260" align="left" cellpadding="0" cellspacing="0" class="section-item">
														<tr>
															<td style="height: 180px; line-height: 180px;" class="section-img">
																<a href="#" style=" border-style: none !important; border: 0 !important;"><img editable="true" mc:edit="image1" src="http://www.cicloinfinito.com.br/img/email/BASIC1.png" style="display: block; width: 260px; height: 180px; padding-right: 0 !important; margin-right: 0 !important; padding-left: 0 !important; margin-left: 0 !important;" width="260" height="180" border="0" alt="section image" /></a>
															</td>
														</tr>
													</table>
													
													<table border="0" width="40" align="left" cellpadding="0" cellspacing="0">
														<tr><td width="40" height="30"></td></tr>
													</table>
													
													<table border="0" width="260" align="right" cellpadding="0" cellspacing="0" class="section-item">
														
														<tr>
															<td align="left" mc:edit="title1" style="color: #424345; font-size: 18px; font-weight: bold; font-family: \'Roboto Slab\', Arial, sans-serif; vertical-align: top;" class="main-header">
									        					<multiline>
									        						Basic 1
									        					</multiline>
									        				</td>	
														</tr>
														
														<tr><td height="16"></td></tr>
														
														<tr>
															<td align="left" mc:edit="text1" style="color: #000; font-size: 14px; font-weight: 300; font-family: \'Roboto\', Arial, sans-serif; line-height: 24px; vertical-align: top;">
																<multiline>
																	Doe <strong>R$ 30,00</strong><br>Receba em doações <strong>R$ 120,00</strong><br>Guarde para você <strong>R$ 30,00</strong><br>Próximo ciclo: <strong>R$ 90,00</strong>. 
																</multiline>
															</td>
														</tr>	
													</table>
													
							    				</td>
							    			</tr>
										</table>
			        				</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
				    				
		
		<tr mc:repeatable>
			<td>
				<table width="580" cellpadding="0" align="center" cellspacing="0" bgcolor="e6e9ea" border="0" class="container580">
					<tr>
						<td>
							<table border="0" align="center" width="578" cellpadding="0" cellspacing="0" bgcolor="ffffff" class="main-content">			
								<tr><td bgcolor="ffffff" height="35"></td></tr>	
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		
		<tr mc:repeatable>
			<td>
				<table width="578" cellpadding="0" align="center" cellspacing="0" border="0" bgcolor="ffffff" class="main-content">
					<tr>
						<td class="divider">
							<img editable="false" mc:edit="divider" src="http://www.cicloinfinito.com.br/img/email/divider.png" style="display: block; width: 578px; height: 10px;" width="578" height="10" border="0" alt="divider" />
						</td>
					</tr>
				</table>
			</td>
		</tr>
		
		<tr mc:repeatable>
			<td>
				<table width="580" cellpadding="0" align="center" cellspacing="0" bgcolor="e6e9ea" border="0" class="container580">
					<tr>
						<td>
							<table border="0" align="center" width="578" cellpadding="0" cellspacing="0" bgcolor="ffffff" class="main-content">			
								<tr><td bgcolor="ffffff" height="35"></td></tr>	
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		
		
		<tr mc:repeatable>
			<td align="center">
				<table border="0" align="center" width="580" cellpadding="0" cellspacing="0" bgcolor="e6e9ea" class="container580">
					<tr>
						<td>
							<table border="0" align="center" width="578" cellpadding="0" cellspacing="0" bgcolor="ffffff" class="main-content">			
								
								<tr mc:repeatable >
			        				<td align="center">
										<table width="560" align="center" cellpadding="0" cellspacing="0" border="0" class="container560">
											<tr mc:repeatable>
							    				<td>	
							    					<table border="0" width="260" align="left" cellpadding="0" cellspacing="0" class="section-item">
														
														<tr>
															<td align="left" mc:edit="title2" style="color: #424345; font-size: 18px; font-weight: bold; font-family: \'Roboto Slab\', Arial, sans-serif; vertical-align: top;" class="main-header">
									        					<multiline>
									        						Basic 2
									        					</multiline>
									        				</td>	
														</tr>
														
														<tr><td height="16"></td></tr>
														
														<tr>
															<td align="left" mc:edit="text2" style="color: #000; font-size: 14px; font-weight: 300; font-family: \'Roboto\', Arial, sans-serif; line-height: 24px; vertical-align: top;">
																<multiline>
																	Doe <strong>R$ 90,00</strong><br>Receba em doações <strong>R$ 360,00</strong><br>Próximo ciclo: <strong>R$ 360,00</strong>.
																</multiline>
															</td>
														</tr>	
													</table>
													
													<table border="0" width="40" align="left" cellpadding="0" cellspacing="0">
														<tr><td width="40" height="30"></td></tr>
													</table>
													
							    					<table border="0" width="260" align="left" cellpadding="0" cellspacing="0" class="section-item">
														<tr>
															<td style="180: 260px; line-height: 180px;" class="section-img">
																<a href="#" style=" border-style: none !important; border: 0 !important;"><img editable="true" mc:edit="image2" src="http://www.cicloinfinito.com.br/img/email/BASIC2.png" style="display: block; width: 260px; height: 180px; padding-right: 0 !important; margin-right: 0 !important; padding-left: 0 !important; margin-left: 0 !important;" width="260" height="180" border="0" alt="section image" /></a>
															</td>
														</tr>
													</table>
													
							    				</td>
							    			</tr>
										</table>
			        				</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
				    				
		
		
		<tr mc:repeatable>
			<td>
				<table width="580" cellpadding="0" align="center" cellspacing="0" bgcolor="e6e9ea" border="0" class="container580">
					<tr>
						<td>
							<table border="0" align="center" width="578" cellpadding="0" cellspacing="0" bgcolor="ffffff" class="main-content">			
								<tr><td bgcolor="ffffff" height="35"></td></tr>	
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		
				
		<tr mc:repeatable>
			<td>
				<table width="578" cellpadding="0" align="center" cellspacing="0" border="0" bgcolor="ffffff" class="main-content">
					<tr>
						<td class="divider" class="divider">
							<img editable="false" mc:edit="divider" src="http://www.cicloinfinito.com.br/img/email/divider.png" style="display: block; width: 578px; height: 10px;" width="578" height="10" border="0" />
						</td>
					</tr>
				</table>
			</td>
		</tr>
		
		<tr mc:repeatable>
			<td>
				<table width="580" cellpadding="0" align="center" cellspacing="0" bgcolor="e6e9ea" border="0" class="container580">
					<tr>
						<td>
							<table border="0" align="center" width="578" cellpadding="0" cellspacing="0" bgcolor="ffffff" class="main-content">			
								<tr><td bgcolor="ffffff" height="35"></td></tr>	
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		
		<tr mc:repeatable>
			<td align="center">
				<table border="0" align="center" width="580" cellpadding="0" cellspacing="0" bgcolor="e6e9ea" class="container580">
					<tr>
						<td>
							<table border="0" align="center" width="578" cellpadding="0" cellspacing="0" bgcolor="ffffff" class="main-content">			
								
								<tr mc:repeatable >
			        				<td align="center">
										<table width="560" align="center" cellpadding="0" cellspacing="0" border="0" class="container560">
											<tr mc:repeatable>
							    				<td>	
							    					
							    					<table border="0" width="260" align="left" cellpadding="0" cellspacing="0" class="section-item">
														<tr>
															<td style="height: 180px; line-height: 180px;" class="section-img">
																<a href="#" style=" border-style: none !important; border: 0 !important;"><img editable="true" mc:edit="image3" src="http://www.cicloinfinito.com.br/img/email/BASIC3.png" style="display: block; width: 260px; height: 180px; padding-right: 0 !important; margin-right: 0 !important; padding-left: 0 !important; margin-left: 0 !important;" width="260" height="180" border="0" alt="section image" /></a>
															</td>
														</tr>
													</table>
													
													<table border="0" width="40" align="left" cellpadding="0" cellspacing="0">
														<tr><td width="40" height="30"></td></tr>
													</table>
													
													<table border="0" width="260" align="right" cellpadding="0" cellspacing="0" class="section-item">
														
														<tr>
															<td align="left" mc:edit="title3" style="color: #424345; font-size: 18px; font-weight: bold; font-family: \'Roboto Slab\', Arial, sans-serif; vertical-align: top;" class="main-header">
									        					<multiline>
									        						Basic 3
									        					</multiline>
									        				</td>	
														</tr>
														
														<tr><td height="16"></td></tr>
														
														<tr>
															<td align="left" mc:edit="tex3" style="color: #000; font-size: 14px; font-weight: 300; font-family: \'Roboto\', Arial, sans-serif; line-height: 24px; vertical-align: top;">
																<multiline>
																	Doe <strong>R$ 360,00</strong><br>Receba em doações <strong>R$ 1.440,00</strong><br>Guarde para você <strong>R$ 360,00</strong><br>Próximo ciclo: <strong>R$ 1.080,00</strong>.
																</multiline>
															</td>
														</tr>	
													</table>
													
							    				</td>
							    			</tr>
										</table>
			        				</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
				    				
		
		<tr mc:repeatable>
			<td>
				<table width="580" cellpadding="0" align="center" cellspacing="0" bgcolor="e6e9ea" border="0" class="container580">
					<tr>
						<td>
							<table border="0" align="center" width="578" cellpadding="0" cellspacing="0" bgcolor="ffffff" class="main-content">			
								<tr><td bgcolor="ffffff" height="35"></td></tr>	
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		
		<tr mc:repeatable>
			<td>
				<table width="578" cellpadding="0" align="center" cellspacing="0" border="0" bgcolor="ffffff" class="main-content">
					<tr>
						<td class="divider">
							<img editable="false" mc:edit="divider" src="http://www.cicloinfinito.com.br/img/email/divider.png" style="display: block; width: 578px; height: 10px;" width="578" height="10" border="0" alt="divider" />
						</td>
					</tr>
				</table>
			</td>
		</tr>
		
		<tr mc:repeatable>
			<td>
				<table width="580" cellpadding="0" align="center" cellspacing="0" bgcolor="e6e9ea" border="0" class="container580">
					<tr>
						<td>
							<table border="0" align="center" width="578" cellpadding="0" cellspacing="0" bgcolor="ffffff" class="main-content">			
								<tr><td bgcolor="ffffff" height="35"></td></tr>	
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		
		
		<tr mc:repeatable>
			<td align="center">
				<table border="0" align="center" width="580" cellpadding="0" cellspacing="0" bgcolor="e6e9ea" class="container580">
					<tr>
						<td>
							<table border="0" align="center" width="578" cellpadding="0" cellspacing="0" bgcolor="ffffff" class="main-content">			
								
								<tr mc:repeatable >
			        				<td align="center">
										<table width="560" align="center" cellpadding="0" cellspacing="0" border="0" class="container560">
											<tr mc:repeatable>
							    				<td>	
							    					<table border="0" width="260" align="left" cellpadding="0" cellspacing="0" class="section-item">
														
														<tr>
															<td align="left" mc:edit="title4" style="color: #424345; font-size: 18px; font-weight: bold; font-family: \'Roboto Slab\', Arial, sans-serif; vertical-align: top;" class="main-header">
									        					<multiline>
									        						Basic 4
									        					</multiline>
									        				</td>	
														</tr>
														
														<tr><td height="16"></td></tr>
														
														<tr>
															<td align="left" mc:edit="text4" style="color: #000; font-size: 14px; font-weight: 300; font-family: \'Roboto\', Arial, sans-serif; line-height: 24px; vertical-align: top;">
																<multiline>
																	Doe <strong>R$ 1.080,00</strong><br>Receba em doações <strong>R$ 4.320,00</strong><br>Guarde para você <strong>R$ 2.160,00</strong><br>Próximo ciclo: <strong>R$ 2.160,00</strong>.
																</multiline>
															</td>
														</tr>	
													</table>
													
													<table border="0" width="40" align="left" cellpadding="0" cellspacing="0">
														<tr><td width="40" height="30"></td></tr>
													</table>
													
							    					<table border="0" width="260" align="left" cellpadding="0" cellspacing="0" class="section-item">
														<tr>
															<td style="180: 260px; line-height: 180px;" class="section-img">
																<a href="#" style=" border-style: none !important; border: 0 !important;"><img editable="true" mc:edit="image4" src="http://www.cicloinfinito.com.br/img/email/BASIC4.png" style="display: block; width: 260px; height: 180px; padding-right: 0 !important; margin-right: 0 !important; padding-left: 0 !important; margin-left: 0 !important;" width="260" height="180" border="0" alt="section image" /></a>
															</td>
														</tr>
													</table>
													
							    				</td>
							    			</tr>
										</table>
			        				</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
				    				
        
        <tr mc:repeatable>
			<td>
				<table width="580" cellpadding="0" align="center" cellspacing="0" bgcolor="e6e9ea" border="0" class="container580">
					<tr>
						<td>
							<table border="0" align="center" width="578" cellpadding="0" cellspacing="0" bgcolor="ffffff" class="main-content">			
								<tr><td bgcolor="ffffff" height="35"></td></tr>	
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		
				
		<tr mc:repeatable>
			<td>
				<table width="578" cellpadding="0" align="center" cellspacing="0" border="0" bgcolor="ffffff" class="main-content">
					<tr>
						<td class="divider" class="divider">
							<img editable="false" mc:edit="divider" src="http://www.cicloinfinito.com.br/img/email/divider.png" style="display: block; width: 578px; height: 10px;" width="578" height="10" border="0" />
						</td>
					</tr>
				</table>
			</td>
		</tr>
		
		<tr mc:repeatable>
			<td>
				<table width="580" cellpadding="0" align="center" cellspacing="0" bgcolor="e6e9ea" border="0" class="container580">
					<tr>
						<td>
							<table border="0" align="center" width="578" cellpadding="0" cellspacing="0" bgcolor="ffffff" class="main-content">			
								<tr><td bgcolor="ffffff" height="35"></td></tr>	
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		
		<tr mc:repeatable>
			<td align="center">
				<table border="0" align="center" width="580" cellpadding="0" cellspacing="0" bgcolor="e6e9ea" class="container580">
					<tr>
						<td>
							<table border="0" align="center" width="578" cellpadding="0" cellspacing="0" bgcolor="ffffff" class="main-content">			
								
								<tr mc:repeatable >
			        				<td align="center">
										<table width="560" align="center" cellpadding="0" cellspacing="0" border="0" class="container560">
											<tr mc:repeatable>
							    				<td>	
							    					
							    					<table border="0" width="260" align="left" cellpadding="0" cellspacing="0" class="section-item">
														<tr>
															<td style="height: 180px; line-height: 180px;" class="section-img">
																<a href="#" style=" border-style: none !important; border: 0 !important;"><img editable="true" mc:edit="image3" src="http://www.cicloinfinito.com.br/img/email/ADVANCED1.png" style="display: block; width: 260px; height: 180px; padding-right: 0 !important; margin-right: 0 !important; padding-left: 0 !important; margin-left: 0 !important;" width="260" height="180" border="0" alt="section image" /></a>
															</td>
														</tr>
													</table>
													
													<table border="0" width="40" align="left" cellpadding="0" cellspacing="0">
														<tr><td width="40" height="30"></td></tr>
													</table>
													
													<table border="0" width="260" align="right" cellpadding="0" cellspacing="0" class="section-item">
														
														<tr>
															<td align="left" mc:edit="title3" style="color: #424345; font-size: 18px; font-weight: bold; font-family: \'Roboto Slab\', Arial, sans-serif; vertical-align: top;" class="main-header">
									        					<multiline>
									        						Advanced 1
									        					</multiline>
									        				</td>	
														</tr>
														
														<tr><td height="16"></td></tr>
														
														<tr>
															<td align="left" mc:edit="tex3" style="color: #000; font-size: 14px; font-weight: 300; font-family: \'Roboto\', Arial, sans-serif; line-height: 24px; vertical-align: top;">
																<multiline>
																	Doe <strong>R$ 2.160,00</strong><br>Receba em doações <strong>R$ 8.640,00</strong><br>Guarde para você <strong>R$ 4.320,00</strong><br>Próximo ciclo: <strong>R$ 4.320,00</strong>. 
																</multiline>
															</td>
														</tr>	
													</table>
													
							    				</td>
							    			</tr>
										</table>
			        				</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
				    				
		
		<tr mc:repeatable>
			<td>
				<table width="580" cellpadding="0" align="center" cellspacing="0" bgcolor="e6e9ea" border="0" class="container580">
					<tr>
						<td>
							<table border="0" align="center" width="578" cellpadding="0" cellspacing="0" bgcolor="ffffff" class="main-content">			
								<tr><td bgcolor="ffffff" height="35"></td></tr>	
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		
		<tr mc:repeatable>
			<td>
				<table width="578" cellpadding="0" align="center" cellspacing="0" border="0" bgcolor="ffffff" class="main-content">
					<tr>
						<td class="divider">
							<img editable="false" mc:edit="divider" src="http://www.cicloinfinito.com.br/img/email/divider.png" style="display: block; width: 578px; height: 10px;" width="578" height="10" border="0" alt="divider" />
						</td>
					</tr>
				</table>
			</td>
		</tr>
		
		<tr mc:repeatable>
			<td>
				<table width="580" cellpadding="0" align="center" cellspacing="0" bgcolor="e6e9ea" border="0" class="container580">
					<tr>
						<td>
							<table border="0" align="center" width="578" cellpadding="0" cellspacing="0" bgcolor="ffffff" class="main-content">			
								<tr><td bgcolor="ffffff" height="35"></td></tr>	
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		
		
		<tr mc:repeatable>
			<td align="center">
				<table border="0" align="center" width="580" cellpadding="0" cellspacing="0" bgcolor="e6e9ea" class="container580">
					<tr>
						<td>
							<table border="0" align="center" width="578" cellpadding="0" cellspacing="0" bgcolor="ffffff" class="main-content">			
								
								<tr mc:repeatable >
			        				<td align="center">
										<table width="560" align="center" cellpadding="0" cellspacing="0" border="0" class="container560">
											<tr mc:repeatable>
							    				<td>	
							    					<table border="0" width="260" align="left" cellpadding="0" cellspacing="0" class="section-item">
														
														<tr>
															<td align="left" mc:edit="title4" style="color: #424345; font-size: 18px; font-weight: bold; font-family: \'Roboto Slab\', Arial, sans-serif; vertical-align: top;" class="main-header">
									        					<multiline>
									        						Advanced 2
									        					</multiline>
									        				</td>	
														</tr>
														
														<tr><td height="16"></td></tr>
														
														<tr>
															<td align="left" mc:edit="text4" style="color: #000; font-size: 14px; font-weight: 300; font-family: \'Roboto\', Arial, sans-serif; line-height: 24px; vertical-align: top;">
																<multiline>
																	Doe <strong>R$ 4.320,00</strong><br>Receba em doações <strong>R$ 17.280,00</strong><br>Guarde para você <strong>R$ 8.640,00</strong><br>Próximo ciclo: <strong>R$ 8.640,00</strong>. 
																</multiline>
															</td>
														</tr>	
													</table>
													
													<table border="0" width="40" align="left" cellpadding="0" cellspacing="0">
														<tr><td width="40" height="30"></td></tr>
													</table>
													
							    					<table border="0" width="260" align="left" cellpadding="0" cellspacing="0" class="section-item">
														<tr>
															<td style="180: 260px; line-height: 180px;" class="section-img">
																<a href="#" style=" border-style: none !important; border: 0 !important;"><img editable="true" mc:edit="image4" src="http://www.cicloinfinito.com.br/img/email/ADVANCED2.png" style="display: block; width: 260px; height: 180px; padding-right: 0 !important; margin-right: 0 !important; padding-left: 0 !important; margin-left: 0 !important;" width="260" height="180" border="0" alt="section image" /></a>
															</td>
														</tr>
													</table>
													
							    				</td>
							    			</tr>
										</table>
			        				</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
				    				
        
        <tr mc:repeatable>
			<td>
				<table width="580" cellpadding="0" align="center" cellspacing="0" bgcolor="e6e9ea" border="0" class="container580">
					<tr>
						<td>
							<table border="0" align="center" width="578" cellpadding="0" cellspacing="0" bgcolor="ffffff" class="main-content">			
								<tr><td bgcolor="ffffff" height="35"></td></tr>	
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		
				
		<tr mc:repeatable>
			<td>
				<table width="578" cellpadding="0" align="center" cellspacing="0" border="0" bgcolor="ffffff" class="main-content">
					<tr>
						<td class="divider" class="divider">
							<img editable="false" mc:edit="divider" src="http://www.cicloinfinito.com.br/img/email/divider.png" style="display: block; width: 578px; height: 10px;" width="578" height="10" border="0" />
						</td>
					</tr>
				</table>
			</td>
		</tr>
		
		<tr mc:repeatable>
			<td>
				<table width="580" cellpadding="0" align="center" cellspacing="0" bgcolor="e6e9ea" border="0" class="container580">
					<tr>
						<td>
							<table border="0" align="center" width="578" cellpadding="0" cellspacing="0" bgcolor="ffffff" class="main-content">			
								<tr><td bgcolor="ffffff" height="35"></td></tr>	
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		
		<tr mc:repeatable>
			<td align="center">
				<table border="0" align="center" width="580" cellpadding="0" cellspacing="0" bgcolor="e6e9ea" class="container580">
					<tr>
						<td>
							<table border="0" align="center" width="578" cellpadding="0" cellspacing="0" bgcolor="ffffff" class="main-content">			
								
								<tr mc:repeatable >
			        				<td align="center">
										<table width="560" align="center" cellpadding="0" cellspacing="0" border="0" class="container560">
											<tr mc:repeatable>
							    				<td>	
							    					
							    					<table border="0" width="260" align="left" cellpadding="0" cellspacing="0" class="section-item">
														<tr>
															<td style="height: 180px; line-height: 180px;" class="section-img">
																<a href="#" style=" border-style: none !important; border: 0 !important;"><img editable="true" mc:edit="image3" src="http://www.cicloinfinito.com.br/img/email/PLUS1.png" style="display: block; width: 260px; height: 180px; padding-right: 0 !important; margin-right: 0 !important; padding-left: 0 !important; margin-left: 0 !important;" width="260" height="180" border="0" alt="section image" /></a>
															</td>
														</tr>
													</table>
													
													<table border="0" width="40" align="left" cellpadding="0" cellspacing="0">
														<tr><td width="40" height="30"></td></tr>
													</table>
													
													<table border="0" width="260" align="right" cellpadding="0" cellspacing="0" class="section-item">
														
														<tr>
															<td align="left" mc:edit="title3" style="color: #424345; font-size: 18px; font-weight: bold; font-family: \'Roboto Slab\', Arial, sans-serif; vertical-align: top;" class="main-header">
									        					<multiline>
									        						Plus 1
									        					</multiline>
									        				</td>	
														</tr>
														
														<tr><td height="16"></td></tr>
														
														<tr>
															<td align="left" mc:edit="tex3" style="color: #000; font-size: 14px; font-weight: 300; font-family: \'Roboto\', Arial, sans-serif; line-height: 24px; vertical-align: top;">
																<multiline>
																	Doe <strong>R$ 8.640,00</strong><br>Receba em doações <strong>R$ 34.560,00</strong><br>Guarde para você <strong>R$ 17.280,00</strong><br>Próximo ciclo: <strong>R$ 17.280,00</strong>.
																</multiline>
															</td>
														</tr>	
													</table>
													
							    				</td>
							    			</tr>
										</table>
			        				</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
				    				
		
		<tr mc:repeatable>
			<td>
				<table width="580" cellpadding="0" align="center" cellspacing="0" bgcolor="e6e9ea" border="0" class="container580">
					<tr>
						<td>
							<table border="0" align="center" width="578" cellpadding="0" cellspacing="0" bgcolor="ffffff" class="main-content">			
								<tr><td bgcolor="ffffff" height="35"></td></tr>	
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		
		<tr mc:repeatable>
			<td>
				<table width="578" cellpadding="0" align="center" cellspacing="0" border="0" bgcolor="ffffff" class="main-content">
					<tr>
						<td class="divider">
							<img editable="false" mc:edit="divider" src="http://www.cicloinfinito.com.br/img/email/divider.png" style="display: block; width: 578px; height: 10px;" width="578" height="10" border="0" alt="divider" />
						</td>
					</tr>
				</table>
			</td>
		</tr>
		
		<tr mc:repeatable>
			<td>
				<table width="580" cellpadding="0" align="center" cellspacing="0" bgcolor="e6e9ea" border="0" class="container580">
					<tr>
						<td>
							<table border="0" align="center" width="578" cellpadding="0" cellspacing="0" bgcolor="ffffff" class="main-content">			
								<tr><td bgcolor="ffffff" height="35"></td></tr>	
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		
		
		<tr mc:repeatable>
			<td align="center">
				<table border="0" align="center" width="580" cellpadding="0" cellspacing="0" bgcolor="e6e9ea" class="container580">
					<tr>
						<td>
							<table border="0" align="center" width="578" cellpadding="0" cellspacing="0" bgcolor="ffffff" class="main-content">			
								
								<tr mc:repeatable >
			        				<td align="center">
										<table width="560" align="center" cellpadding="0" cellspacing="0" border="0" class="container560">
											<tr mc:repeatable>
							    				<td>	
							    					<table border="0" width="260" align="left" cellpadding="0" cellspacing="0" class="section-item">
														
														<tr>
															<td align="left" mc:edit="title4" style="color: #424345; font-size: 18px; font-weight: bold; font-family: \'Roboto Slab\', Arial, sans-serif; vertical-align: top;" class="main-header">
									        					<multiline>
									        						Plus 2
									        					</multiline>
									        				</td>	
														</tr>
														
														<tr><td height="16"></td></tr>
														
														<tr>
															<td align="left" mc:edit="text4" style="color: #000; font-size: 14px; font-weight: 300; font-family: \'Roboto\', Arial, sans-serif; line-height: 24px; vertical-align: top;">
																<multiline>
																	Doe <strong>R$ 17.280,00</strong><br>Receba em doações <strong>R$ 69.120,00</strong><br>Guarde para você <strong>R$ 69.090,00</strong><br>Próximo ciclo: <strong>R$ 30,00 (Basic 1)</strong>.
																</multiline>
															</td>
														</tr>	
													</table>
													
													<table border="0" width="40" align="left" cellpadding="0" cellspacing="0">
														<tr><td width="40" height="30"></td></tr>
													</table>
													
							    					<table border="0" width="260" align="left" cellpadding="0" cellspacing="0" class="section-item">
														<tr>
															<td style="180: 260px; line-height: 180px;" class="section-img">
																<a href="#" style=" border-style: none !important; border: 0 !important;"><img editable="true" mc:edit="image4" src="http://www.cicloinfinito.com.br/img/email/PLUS2.png" style="display: block; width: 260px; height: 180px; padding-right: 0 !important; margin-right: 0 !important; padding-left: 0 !important; margin-left: 0 !important;" width="260" height="180" border="0" alt="section image" /></a>
															</td>
														</tr>
													</table>
													
							    				</td>
							    			</tr>
										</table>
			        				</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
				    				
		
		<tr mc:repeatable>
			<td>
				<table width="580" cellpadding="0" align="center" cellspacing="0" bgcolor="e6e9ea" border="0" class="container580">
					<tr>
						<td>
							<table border="0" align="center" width="578" cellpadding="0" cellspacing="0" bgcolor="ffffff" class="main-content">			
								<tr><td bgcolor="ffffff" height="35"><h2 style="font-family:\'Roboto\', Arial, sans-serif; line-height: 40px;"><center>GANHO TOTAL: <strong style="color: #0C0">R$ 101.880,00</strong></center></h2></td></tr>	
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		
		<tr mc:repeatable>
			<td>
				<table width="578" cellpadding="0" align="center" cellspacing="0" border="0" bgcolor="ffffff" class="main-content">
					<tr>
						<td class="divider" class="divider">
							<img editable="false" mc:edit="divider" src="http://www.cicloinfinito.com.br/img/email/divider.png" style="display: block; width: 578px; height: 10px;" width="578" height="10" border="0" />
						</td>
					</tr>
				</table>
			</td>
		</tr>
		
		<tr mc:repeatable>
			<td align="center">
				<table border="0" align="center" width="580" cellpadding="0" cellspacing="0" bgcolor="1e2f3e" class="container580">
					<tr>
						<td align="center">
							<table border="0" align="center" width="540" cellpadding="0" cellspacing="0" class="container560">			
								<tr><td height="30"></td></tr>
								<tr>
									<td align="center">
										<table border="0" align="left" cellpadding="0" cellspacing="0" class="container560">			
											<tr>
												<td>
													<table border="0" align="center" cellpadding="0" cellspacing="0">
							                			<tr>
							                				<td align="left" class="icon" valign="top">
							                					<img editable="true" mc:edit="icon" width="40" height="40" border="0" style="display: block; width: 40px; height: 40px" src="http://www.cicloinfinito.com.br/img/email/icon.png" alt="icon" />
							                				</td>
							                				<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
							                				<td class="cta-text" align="left" mc:edit="cta-text" style="color: #ffffff; font-size: 14px; font-weight: 300; font-family: \'Roboto\', Arial, sans-serif; line-height: 30px; vertical-align: top;">
																<multiline>
																	 Essa é sua oportunidade,<span style="font-weight: bold; font-family: \'Roboto Slab\', Arial, sans-serif;"> entre agora !</span> 
																</multiline>
															</td>
							                			</tr>
							                		</table>
												</td>
											</tr>
										</table>
										
				                		<table border="0" align="left" cellpadding="0" cellspacing="0" class="container560">
				                			<tr>
				                				<td height="20" width="20"></td>
				                			</tr>
				                		</table>
				                		
				                		<table border="0" width="120" align="right" cellpadding="0" cellspacing="0" class="container560">
	    									<tr>
	    										<td align="center">
	    											<a href="http://cicloinfinito.com.br/CicloInfinito.ppsx" style="display: block; width: 120px; border-style: none !important; border: 0 !important;"><img editable="true" mc:edit="download" width="120" height="32" border="0" style="display: block; width: 120px; height: 32px;" src="http://www.cicloinfinito.com.br/img/email/apre2.png" alt="download" /></a>
	    										</td>
	    										
	    									</tr>
	    								</table>
									</td>
								</tr>
								<tr><td height="20"></td></tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
				
		<tr>
	        <td align="center">
	        	<img editable="true" src="http://www.cicloinfinito.com.br/img/email/bottom-rounded-bg.png" style="display: block; width: 580px; height: 10px; line-height: 1px;" width="580" height="10" border="0" class="rounded-edg-bg" />
	        </td>
        </tr>
		
		<tr><td height="40"></td></tr>
		
		<tr><td height="40"></td></tr>
		
		<tr>
			<td align="center"  bgcolor="1e2f3e">
				<table width="580" cellpadding="0" cellspacing="0" border="0" class="container580">
					<tr><td height="10"></td></tr>
					<tr>
						<td>
							<table border="0" align="left" cellpadding="0" cellspacing="0" class="copy">
	                			<tr>
	                				<td align="center" mc:edit="copy" style="color: #ffffff; font-size: 12px; font-weight: 300; font-family: \'Roboto\', Arial, sans-serif; line-height: 28px;" >
				    					<multiline>
				    						Ciclo Infinito <span style="color: #9aa3ab;">© Todos os direitos reservados - 2013</span>
				    					</multiline>
				    				</td>	
	                			</tr>
	                		</table>		
	                		<table border="0" align="left" cellpadding="0" cellspacing="0" class="separator">
	                			<tr>
	                				<td height="20" width="20"></td>
	                			</tr>
	                		</table>
	                		<table border="0" align="right" cellpadding="0" cellspacing="0" class="socials">
	                			
	                			<tr>
	                				<td align="center" style="font-size: 13px; font-family: Helvetica, Arial, sans-serif;">
	                					<table border="0" align="center" cellpadding="0" cellspacing="0">
	                						<tr>
	                							<td>
	                								<a style="display: block; width: 24px; border-style: none !important; border: 0 !important;" href="https://www.facebook.com/CicloInfinito" target="_blank"><img editable="true" mc:edit="facebook" width="24" height="24" border="0" style="display: block;" src="http://www.cicloinfinito.com.br/img/email/facebook.png" alt="facebook" /></a>		
	                							</td>
	                						</tr>
	                					</table>
	                				</td>
	                			</tr>
	                		</table>
						</td>
					</tr>
					<tr><td height="5"></td></tr>
					
				</table>
			</td>
		</tr>
		
		
	</table>

</body>
</html>		
';

	
	EnviarEmail("$dados[email]","contato@cicloinfinito.com.br","Tá com dúvidas sobre o Ciclo Infinito?",$mensagem);
	//sleep(8);
}
flush();
?>
