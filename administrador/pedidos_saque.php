<?php 

	require_once(realpath(dirname(__FILE__)) . '/includes/controller-pedido-saque.php');

	$controller = new ControllerPedidoSaque();

	$resposta = $controller->atualizarDiaSaque($_POST);

	if (!$resposta && $controller->getMensagem()) {
		echo "

			<script>
				alert('{$controller->getMensagem()}');
			</script>

		";
	}

	if ($resposta) {

		echo '<script>window.location.href="./pedidos_saque";</script>';

	}

	$resposta = $controller->atualizarStatusPedido($_POST);
	if (!$resposta && $controller->getMensagem()) {
		echo "

			<script>
				alert('{$controller->getMensagem()}');
			</script>

		";
	}

	if ($resposta) {
		
		echo '<script>window.location.href="./pedidos_saque";</script>';		

	}

	$controller->mostrarPedidosPendentes();