function Validar(){
	
	
	function validarCPF(cpf) {
	
		cpf = cpf.replace(/[^\d]+/g,'');
	
		if(cpf == '') return false;
	
		// Elimina CPFs invalidos conhecidos
		if (cpf.length != 11 || 
			cpf == "00000000000" || 
			cpf == "11111111111" || 
			cpf == "22222222222" || 
			cpf == "33333333333" || 
			cpf == "44444444444" || 
			cpf == "55555555555" || 
			cpf == "66666666666" || 
			cpf == "77777777777" || 
			cpf == "88888888888" || 
			cpf == "99999999999")
			return false;
		
		// Valida 1o digito
		add = 0;
		for (i=0; i < 9; i ++)
			add += parseInt(cpf.charAt(i)) * (10 - i);
		rev = 11 - (add % 11);
		if (rev == 10 || rev == 11)
			rev = 0;
		if (rev != parseInt(cpf.charAt(9)))
			return false;
		
		// Valida 2o digito
		add = 0;
		for (i = 0; i < 10; i ++)
			add += parseInt(cpf.charAt(i)) * (11 - i);
		rev = 11 - (add % 11);
		if (rev == 10 || rev == 11)
			rev = 0;
		if (rev != parseInt(cpf.charAt(10)))
			return false;
			
		return true;
	   
	}
	
	
	d = document.cadastro;
	if (d.nome.value == ""){
		alert('Informe seu nome!');
		d.nome.focus();
		return false;
	}
	if (d.email.value == ""){
		alert('Informe seu e-mail válido!');
		d.email.focus();
		return false;
	}
	if (d.cpf.value == ""){
		alert('Informe seu CPF!');
		d.cpf.focus();
		return false;
	}
	if (!validarCPF(d.cpf.value)){
		alert('CPF inválido!');
		d.cpf.focus();
		return false;
	}
	if (d.telefone1.value == ""){
		alert('Informe seu telefone verdadeiro!');
		d.telefone1.focus();
		return false;
	}
	if (d.operadora1.value == ""){
		alert('Informe a operadora de seu telefone verdadeiro!');
		d.operadora1.focus();
		return false;
	}
	if (d.banco_titular.value == ""){
		alert('Informe o titular de sua conta bancária!');
		d.banco_titular.focus();
		return false;
	}
	if (d.banco_cpf.value == ""){
		alert('Informe o CPF do titular da conta bancária!');
		d.banco_cpf.focus();
		return false;
	}
	if (!validarCPF(d.banco_cpf.value)){
		alert('CPF inválido!');
		d.banco_cpf.focus();
		return false;
	}
	if (d.banco.value == ""){
		alert('Informe o nome do banco de sua conta bancária!');
		d.banco.focus();
		return false;
	}
	if (d.agencia.value == ""){
		alert('Informe a agência de sua conta!');
		d.agencia.focus();
		return false;
	}
	if (d.conta.value == ""){
		alert('Informe o número de sua conta!');
		d.conta.focus();
		return false;
	}
	if (d.usuario.value == ""){
		alert('Informe seu usuário!');
		d.usuario.focus();
		return false;
	}
	if (d.senha.value == ""){
		alert('Informe sua senha!');
		d.senha.focus();
		return false;
	}
	if (d.senha.value.length > 10){
		alert('Sua senha não pode ter mais de 10 caracteres!');
		d.senha.focus();
		return false;
	}
	if (d.senha2.value == ""){
		alert('Repita sua senha!');
		d.senha2.focus();
		return false;
	}
	if (d.senha.value != d.senha2.value){
		alert('As senhas devem ser iguais!');
		d.senha2.focus();
		return false;
	}
	if (d.termos.checked == false) {
		alert('Para continuar, você precisa concordar com os termos de uso, caso contrário não prossiga com o cadastro!');
		d.aceito.focus();
		return false;
	}
	return true;
}