<?php require 'header.php' ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Saques
        <small>Gerencie as solicitações de saques dos clientes</small>
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">

          <div class="row">
            <div class="col-md-12">
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Solicitações de saque</h3>
                  <div class="box-tools">
                  </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                  <table class="table table-hover">
                    <tbody><tr>
                      <th>Número</th>
                      <th>Cliente</th>
                      <th>Saldo atual</th>
                      <th>Valor</th>
                      <th>Data</th>
                      <th>Atualizada</th>
                      <th>Ações</th>
                    </tr>
                    <tr>
                      <td>123</td>
                      <td><a href="#">Julieta Marques</a></td>
                      <td>R$110.80</td>
                      <td>R$50.60</td>
                      <td>11/07/2016</td>
                      <td>23/07/2016</td>
                      <td>
                        <button type="button" class="btn btn-default btn-sm"><i class="fa fa-check"></i> Aprovar</button>
                        <button type="button" class="btn btn-default btn-sm"><i class="fa fa-trash"></i> Rejeitar</button>
                      </td>
                    </tr>
                    <tr>
                      <td>123</td>
                      <td><a href="#">Julieta Marques</a></td>
                      <td>R$110.80</td>
                      <td>R$50.60</td>
                      <td>11/07/2016</td>
                      <td>23/07/2016</td>
                      <td>
                        <button type="button" class="btn btn-default btn-sm"><i class="fa fa-check"></i> Aprovar</button>
                        <button type="button" class="btn btn-default btn-sm"><i class="fa fa-trash"></i> Rejeitar</button>
                      </td>
                    </tr>
                    <tr>
                      <td>123</td>
                      <td><a href="#">Julieta Marques</a></td>
                      <td>R$110.80</td>
                      <td>R$50.60</td>
                      <td>11/07/2016</td>
                      <td>23/07/2016</td>
                      <td>
                        <button type="button" class="btn btn-default btn-sm"><i class="fa fa-check"></i> Aprovar</button>
                        <button type="button" class="btn btn-default btn-sm"><i class="fa fa-trash"></i> Rejeitar</button>
                      </td>
                    </tr>
                  </tbody></table>
                </div>
                <!-- /.box-body -->
              </div>
            </div>
          </div>

          <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title" id="myModalLabel">Nova solicitação de saque</h4>
                </div>
                <div class="modal-body">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Valor do saque</label>
                    <div class="input-group">
                      <span class="input-group-addon">R$</span>
                      <input type="text" class="form-control">
                    </div>
                  </div>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                  <button type="button" class="btn btn-primary" data-dismiss="modal">Fazer solicitação</button>
                </div>
              </div>
            </div>
          </div>

          <div class="modal fade" id="modalConfirmar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title" id="myModalLabel">Confirmar solicitação de depósito</h4>
                </div>
                <div class="modal-body">
                    <!-- form start -->
                    <form role="form">
                      <div class="box-body">

                        <div class="form-group">
                          <label for="exampleInputFile">Envie o comprovante</label>
                          <input type="file" id="exampleInputFile">
                        </div>

                        <p>OU</p>
                        <div class="form-group">
                          <label>Informe os dados do comprovante</label>
                        </div>

                        <div class="form-group">
                          <label for="exampleInputEmail1">Valor do depósito</label>
                          <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email" value="34.00" disabled>
                        </div>
                        <div class="form-group">
                          <label for="exampleInputPassword1">Data e hora</label>
                          <input type="text" class="form-control" id="exampleInputPassword1" placeholder="Data e hora">
                        </div>
                        <div class="form-group">
                          <label for="exampleInputPassword1">Forma do depósito</label>
                          <select class="form-control">
                            <option>Boca do caixa</option>
                            <option>Caixa eletrônico</option>
                            <option>TED/DOC</option>
                            <option>Transferência</option>
                          </select>
                        </div>
                      <!-- /.box-body -->
                    </form>
                  </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                  <button type="button" class="btn btn-primary" data-dismiss="modal">Confirmar solicitação</button>
                </div>
              </div>
            </div>
          </div>

        </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <?php require 'footer.php' ?>