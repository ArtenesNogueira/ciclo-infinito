<?php require 'header.php' ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Clientes
        <small>Gerência os dados e saldos dos clientes</small>
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">

      <div class="row">
              <!-- /.col -->
              <div class="col-md-12">
                <div class="box box-primary">
                  <!-- /.box-header -->
                  <div class="box-body no-padding">
                    <div class="mailbox-controls">
                      <!-- Check all button -->
                      <button type="button" class="btn btn-default btn-sm checkbox-toggle"><i class="fa fa-square-o"></i></button>
                      <div class="btn-group">
                        <form class="form-inline">
                          <input class="form-control" placeholder="valor" type="text">
                          <button type="button" class="btn btn-default btn-sm"><i class="fa fa-plus"></i> Somar valor</button>                          
                        </form>  
                      </div>
                      <!-- /.btn-group -->
                      <div class="pull-right">
                        1-50/200
                        <div class="btn-group">
                          <button type="button" class="btn btn-default btn-sm"><i class="fa fa-chevron-left"></i></button>
                          <button type="button" class="btn btn-default btn-sm"><i class="fa fa-chevron-right"></i></button>
                        </div>
                        <!-- /.btn-group -->
                      </div>
                      <!-- /.pull-right -->
                    </div>
                    <div class="table-responsive mailbox-messages">
                      <table class="table table-hover table-striped">
                        <tbody>
                          <tr>
                            <th></th>
                            <th>Id</th>
                            <th>Cliente</th>
                            <th>Saldo</th>
                            <th>Planos</th>
                          </tr>
                        <tr>
                          <td><input type="checkbox"></td>
                          <td class="mailbox-star">34</td>
                          <td class="mailbox-name"><a href="cliente.html">Fernanda Naquim</a></td>
                          <td class="mailbox-subject">R$345.78</td>
                          <td class="mailbox-attachment">2</td>
                        </tr>
                        <tr>
                          <td><input type="checkbox"></td>
                          <td class="mailbox-star">34</td>
                          <td class="mailbox-name"><a href="cliente.html">Fernanda Naquim</a></td>
                          <td class="mailbox-subject">R$345.78</td>
                          <td class="mailbox-attachment">2</td>
                        </tr>
                        <tr>
                          <td><input type="checkbox"></td>
                          <td class="mailbox-star">34</td>
                          <td class="mailbox-name"><a href="cliente.html">Fernanda Naquim</a></td>
                          <td class="mailbox-subject">R$345.78</td>
                          <td class="mailbox-attachment">2</td>
                        </tr>
                        </tbody>
                      </table>
                      <!-- /.table -->
                    </div>
                    <!-- /.mail-box-messages -->
                  </div>
                  <!-- /.box-body -->
                  <div class="box-footer no-padding">
                    <div class="mailbox-controls">
                      <!-- Check all button -->
                      <button type="button" class="btn btn-default btn-sm checkbox-toggle"><i class="fa fa-square-o"></i></button>
                      <div class="btn-group">
                        <form class="form-inline">
                          <input class="form-control" placeholder="valor" type="text">
                          <button type="button" class="btn btn-default btn-sm"><i class="fa fa-plus"></i> Somar valor</button>                          
                        </form>  
                      </div>
                      <!-- /.btn-group -->
                      <div class="pull-right">
                        1-50/200
                        <div class="btn-group">
                          <button type="button" class="btn btn-default btn-sm"><i class="fa fa-chevron-left"></i></button>
                          <button type="button" class="btn btn-default btn-sm"><i class="fa fa-chevron-right"></i></button>
                        </div>
                        <!-- /.btn-group -->
                      </div>
                      <!-- /.pull-right -->
                    </div>
                  </div>
                </div>
                <!-- /. box -->
              </div>
              <!-- /.col -->
            </div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <?php require 'footer.php' ?>