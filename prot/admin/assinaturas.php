<?php require 'header.php' ?>

<div class="content-wrapper">
	
	<section class="content-header">
	  <h1>
	    Assinaturas
	    <small>Confira as assinaturas dos clientes</small>
	  </h1>
	</section>

	<section class="content">
		
		<div class="row">
        	<div class="col-xs-12">

        			<div class="nav-tabs-custom">
        	            <ul class="nav nav-tabs">
        	              <li class="active"><a href="#tabAtivas" data-toggle="tab" aria-expanded="true">Ativas</a></li>
        	              <li class=""><a href="#tabEncerradas" data-toggle="tab" aria-expanded="false">Encerradas</a></li>
        	            </ul>
        	            <div class="tab-content">
        	              
        	              <div class="tab-pane active" id="tabAtivas">
        	                
	        	              <div class="box">
	        	              	
	        	              		<div class="box-header">
										
	        	              			<form class="form-inline" action="" method="GET">

	        	              			  <div class="form-group">
	        	              			    <input type="text" class="form-control" placeholder="Cliente" name="cliente">
	        	              			  </div>

	        	              			  <div class="form-group">
	        	              			  	<select class="form-control" name="plano">
	        	              			  		<option>Plano</option>
	        	              			  		<option value="0">Plano A</option>
	        	              			  		<option value="0">Plano B</option>
	        	              			  		<option value="0">Plano C</option>
	        	              			  		<option value="0">Plano D</option>
	        	              			  	</select>
	        	              			  </div>

	        	              			  <div class="form-group">
	        	              			    <input type="text" class="form-control" placeholder="Valor" name="valor">
	        	              			  </div>

	        	              			  <button type="submit" class="btn btn-success">Filtrar</button>

	        	              			</form>

	        	              		</div>

		        	              <div class="box-body">
		        	              	
		        	              	<table class="table table-striped">
		        	              	      <tbody><tr>
		        	              	        <th>Num. assinatura</th>
		        	              	        <th>Cliente</th>
		        	              	        <th>Plano</th>
		        	              	        <th>Data de adesão</th>
		        	              	        <th>Valor de retorno</th>
		        	              	        <th>Total ganho</th>
		        	              	        <th>Limite disponível</th>
		        	              	        <th></th>
		        	              	      </tr>
		        	              	      <?php for ($i = 1; $i <= 10; $i++): ?>
		        	              	      	<tr>
		        	              	      		<td>24</td>
		        	              	      		<td><a>Nome do cliente</a></td>
		        	              	      		<td>Nome do plano</td>
		        	              	      		<td>09/08/2016</td>
		        	              	      		<td class="text-red">R$5.00</td>
		        	              	      		<td class="text-red">R$1155.00</td>
		        	              	      		<td class="text-light-blue">231/360</td>
		        	              	      		<td><a class="btn btn-default btn-sm" href="detalhes-assinatura.php"><i class="fa fa-eye"></i>Detalhes</a></td>
		        	              	      	</tr>
		        	              	      <?php endfor; ?>
		        	              	    </tbody>
		        	              	</table>

		        	              </div>

		        	              <div class="box-footer clearfix">
		                            <ul class="pagination no-margin pull-right">
		                              <li><a href="#">«</a></li>
		                              <li><a href="#">1</a></li>
		                              <li><a href="#">2</a></li>
		                              <li><a href="#">3</a></li>
		                              <li><a href="#">»</a></li>
		                            </ul>
		                          </div>

	        	              	</div>

        	              </div>
        	              
        	              <div class="tab-pane" id="tabEncerradas">
        	                
        	              	        	              <div class="box">
        	              	        	              	
        	              	        	              		<div class="box-header">
        	              										
        	              	        	              			<form class="form-inline" action="" method="GET">

        	              	        	              			  <div class="form-group">
        	              	        	              			    <input type="text" class="form-control" placeholder="Cliente" name="cliente">
        	              	        	              			  </div>

        	              	        	              			  <div class="form-group">
        	              	        	              			  	<select class="form-control" name="plano">
        	              	        	              			  		<option>Plano</option>
        	              	        	              			  		<option value="0">Plano A</option>
        	              	        	              			  		<option value="0">Plano B</option>
        	              	        	              			  		<option value="0">Plano C</option>
        	              	        	              			  		<option value="0">Plano D</option>
        	              	        	              			  	</select>
        	              	        	              			  </div>

        	              	        	              			  <div class="form-group">
        	              	        	              			    <input type="text" class="form-control" placeholder="Valor" name="valor">
        	              	        	              			  </div>

        	              	        	              			  <button type="submit" class="btn btn-success">Filtrar</button>

        	              	        	              			</form>

        	              	        	              		</div>

        	              		        	              <div class="box-body">
        	              		        	              	
        	              		        	              	<table class="table table-striped">
        	              		        	              	      <tbody><tr>
        	              		        	              	        <th>Num. assinatura</th>
        	              		        	              	        <th>Cliente</th>
        	              		        	              	        <th>Plano</th>
        	              		        	              	        <th>Data de adesão</th>
        	              		        	              	        <th>Valor de retorno</th>
        	              		        	              	        <th>Total ganho</th>
        	              		        	              	        <th>Limite disponível</th>
        	              		        	              	        <th></th>
        	              		        	              	      </tr>
        	              		        	              	      <?php for ($i = 1; $i <= 5; $i++): ?>
        	              		        	              	      	<tr>
        	              		        	              	      		<td>24</td>
        	              		        	              	      		<td><a>Nome do cliente</a></td>
        	              		        	              	      		<td>Nome do plano</td>
        	              		        	              	      		<td>09/08/2016</td>
        	              		        	              	      		<td class="text-red">R$5.00</td>
        	              		        	              	      		<td class="text-red">R$1155.00</td>
        	              		        	              	      		<td class="text-light-blue">231/360</td>
        	              		        	              	      		<td><a class="btn btn-default btn-sm" href="detalhes-assinatura.php"><i class="fa fa-eye"></i>Detalhes</a></td>
        	              		        	              	      	</tr>
        	              		        	              	      <?php endfor; ?>
        	              		        	              	    </tbody>
        	              		        	              	</table>

        	              		        	              </div>

        	              		        	              <div class="box-footer clearfix">
        	              		                            <ul class="pagination no-margin pull-right">
        	              		                              <li><a href="#">«</a></li>
        	              		                              <li><a href="#">1</a></li>
        	              		                              <li><a href="#">2</a></li>
        	              		                              <li><a href="#">3</a></li>
        	              		                              <li><a href="#">»</a></li>
        	              		                            </ul>
        	              		                          </div>

        	              	        	              	</div>
        	              	
        	              </div>
        	            </div>
        	          </div>

        	</div>
        </div>

	</section>

</div>

<?php require 'footer.php' ?>