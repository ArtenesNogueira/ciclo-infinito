<?php include 'header.php' ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Administradores
        <small>Veja quem está administrando o sistema</small>
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">

          <div class="row">
            <div class="col-md-12">
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Solicitações de depósito</h3>

                  <div class="box-tools">
                      <a href="" class="btn btn-success" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus"></i> Novo administrador</a>
                  </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                  <table class="table table-hover">
                    <tbody><tr>
                      <th>Id</th>
                      <th>Nome</th>
                      <th>CPF</th>
                      <th>Nascimento</th>
                      <th>E-mail</th>
                      <th>Cadastro</th>
                    </tr>
                    <tr>
                      <td>123</td>
                      <td>Joana Maria</td>
                      <td>123.123.132.12-12</td>
                      <td>21/07/1992</td>
                      <td>maria@gmail.com</td>
                      <td>21/07/2016</td>
                    </tr>
                    <tr>
                      <td>123</td>
                      <td>Joana Maria</td>
                      <td>123.123.132.12-12</td>
                      <td>21/07/1992</td>
                      <td>maria@gmail.com</td>
                      <td>21/07/2016</td>
                    </tr>
                    <tr>
                      <td>123</td>
                      <td>Joana Maria</td>
                      <td>123.123.132.12-12</td>
                      <td>21/07/1992</td>
                      <td>maria@gmail.com</td>
                      <td>21/07/2016</td>
                    </tr>
                    <tr>
                      <td>123</td>
                      <td>Joana Maria</td>
                      <td>123.123.132.12-12</td>
                      <td>21/07/1992</td>
                      <td>maria@gmail.com</td>
                      <td>21/07/2016</td>
                    </tr>
                  </tbody></table>
                </div>
                <!-- /.box-body -->
              </div>
            </div>
          </div>

          <div class="modal fade" id="myModal" tabindex="0" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title" id="myModalLabel">Novo cadastro de administrador</h4>
                </div>
                <div class="modal-body">
                  <form action="./escritorio-virtual.html" method="post">
                        <div class="form-group has-feedback">
                          <input type="text" class="form-control" placeholder="Nome completo">
                          <span class="glyphicon glyphicon-user form-control-feedback"></span>
                        </div>
                        <div class="form-group has-feedback">
                          <input type="text" class="form-control" placeholder="CPF">
                          <span class="glyphicon glyphicon-file form-control-feedback"></span>
                        </div>
                        <div class="form-group has-feedback">
                          <input type="text" class="form-control" placeholder="Data de nascimento">
                          <span class="glyphicon glyphicon-calendar form-control-feedback"></span>
                        </div>
                        <div class="form-group has-feedback">
                          <input type="email" class="form-control" placeholder="E-mail">
                          <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                        </div>
                        <small>A senha será gerada automaticamente e enviada para o e-mail informado.</small>
                      </form>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                  <button type="button" class="btn btn-primary" data-dismiss="modal">Cadastrar</button>
                </div>
              </div>
            </div>
          </div>

        </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <?php require 'footer.php' ?>