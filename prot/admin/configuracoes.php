<?php require 'header.php' ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Configurações
        <small>gerais do sistema</small>
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">

      <div class="row">

                  <div class="col-md-6">
                    <div class="box box-primary">
                      <div class="box-header with-border">
                        <h3 class="box-title">Conta para depósito</h3>
                        <small>Conta para qual os clientes devem depositar o dinheiro</small>
                      </div>
                      <!-- /.box-header -->
                      <!-- form start -->
                      <form role="form">
                        <div class="box-body">
                          <div class="form-group">
                            <label for="exampleInputEmail1">Banco</label>
                            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Nome do banco" value="Itaú">
                          </div>
                          <div class="form-group">
                            <label for="exampleInputPassword1">Titular</label>
                            <input type="text" class="form-control" id="exampleInputPassword1" placeholder="titular" value="Lorem Ipsum">
                          </div>
                          <div class="form-group">
                            <label for="exampleInputPassword1">CPF</label>
                            <input type="text" class="form-control" id="exampleInputPassword1" placeholder="cpf" value="017.1827.812-12">
                          </div>
                          <div class="form-group">
                            <label for="exampleInputPassword1">Agência</label>
                            <input type="text" class="form-control" id="exampleInputPassword1" placeholder="agência" value="34569-90">
                          </div>
                          <div class="form-group">
                            <label for="exampleInputPassword1">Conta</label>
                            <input type="text" class="form-control" id="exampleInputPassword1" placeholder="conta" value="34569-90">
                          </div>

                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                          <button type="submit" class="btn btn-primary">Salvar</button>
                        </div>
                      </form>
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="box">
                                <div class="box-header">
                                  <h3 class="box-title">Slides</h3>

                                  <div class="box-tools">
                                      <a href="" class="btn btn-success" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus"></i> Novo slide</a>
                                  </div>
                                </div>
                                <!-- /.box-header -->
                                <div class="box-body table-responsive no-padding">
                                  <table class="table table-hover">
                                    <tbody><tr>
                                      <th>Slide</th>
                                      <th>Ações</th>
                                    </tr>
                                    <tr>
                                      <td>Slide de pormoção</td>
                                      <td>
                                        <button type="button" class="btn btn-default btn-sm"><i class="fa fa-check"></i> Editar</button>
                                        <button type="button" class="btn btn-default btn-sm"><i class="fa fa-trash"></i> Deletar</button>
                                      </td>
                                    </tr>
                                    <tr>
                                      <td>Slide de pormoção</td>
                                      <td>
                                        <button type="button" class="btn btn-default btn-sm"><i class="fa fa-check"></i> Editar</button>
                                        <button type="button" class="btn btn-default btn-sm"><i class="fa fa-trash"></i> Deletar</button>
                                      </td>
                                    </tr>
                                    <tr>
                                      <td>Slide de pormoção</td>
                                      <td>
                                        <button type="button" class="btn btn-default btn-sm"><i class="fa fa-check"></i> Editar</button>
                                        <button type="button" class="btn btn-default btn-sm"><i class="fa fa-trash"></i> Deletar</button>
                                      </td>
                                    </tr>
                                    <tr>
                                      <td>Slide de pormoção</td>
                                      <td>
                                        <button type="button" class="btn btn-default btn-sm"><i class="fa fa-check"></i> Editar</button>
                                        <button type="button" class="btn btn-default btn-sm"><i class="fa fa-trash"></i> Deletar</button>
                                      </td>
                                    </tr>
                                  </tbody></table>
                                </div>
                                <!-- /.box-body -->
                              </div>

                </div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <?php require 'footer.php' ?>