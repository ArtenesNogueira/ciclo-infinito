<?php require 'header.php' ?>

<div class="content-wrapper">
	
	<section class="content-header">
	  <h1>
	    Configurações
	    <small>Defina algumas opções para o módulo</small>
	  </h1>
	</section>

	<section class="content">
		
		<div class="row">
			
			<div class="col-md-4">
				
				<div class="box box-primary">
					
					<form action="" method="POST">

						<div class="box-header">
							<h3 class="box-title">Número máximo de planos</h3>
						</div>

						<div class="box-body">
							
							<small>Quantos planos no máximo os clientes poderão assinar?</small>

							<input type="text" name="numero" class="form-control" required>

						</div>

						<div class="box-footer">
							
							<button type="submit" class="btn btn-primary btn-sm">Salvar</button>

						</div>

					</form>

				</div>

			</div>

			<div class="col-md-4">
				
				<div class="box box-primary">
					
					<form action="" method="POST">

						<div class="box-header">
							<h3 class="box-title">Hora do recebimento</h3>
						</div>

						<div class="box-body">
							
							<small>À que horas os clientes devem receber o valor do plano?</small>

							<input type="text" name="hora" class="form-control" required>

						</div>

						<div class="box-footer">
							
							<button type="submit" class="btn btn-primary btn-sm">Salvar</button>

						</div>

					</form>

				</div>

			</div>

			<div class="col-md-4">
				
				<div class="box box-primary">
					
					<form action="" method="POST">

						<div class="box-header">
							<h3 class="box-title">Cliente padrão</h3>
						</div>

						<div class="box-body">
							
							<small>Qual será o cliente patrocinador de novos clientes?</small>

							<select name="cliente" class="form-control" required>
								<option>Escolha um cliente</option>
								<option>Cliente A</option>
								<option>Cliente A</option>
								<option>Cliente A</option>
								<option>Cliente A</option>
							</select>

						</div>

						<div class="box-footer">
							
							<button type="submit" class="btn btn-primary btn-sm">Salvar</button>

						</div>

					</form>

				</div>

			</div>

		</div>

		<div class="row">
			
			<div class="col-md-12">
				
				<div class="box box-primary">
					
					<form action="" method="POST">

						<div class="box-header">
							<h3 class="box-title">Conteúdo da página sobre</h3>
							<small>Construa o conteúdo que será exibido na página sobre</small>
						</div>

						<div class="box-body">
							
							<textarea name="paginaSobre" id="paginaSobre" rows="10" cols="80">
								Edite seu conteúdo aqui.
							</textarea>

						</div>

						<div class="box-footer">
							
							<button type="submit" class="btn btn-primary btn-sm">Salvar</button>
							<a class="btn btn-success btn-sm">Ver página</a>

						</div>

					</form>

				</div>

			</div>

		</div>

	</section>

</div>

<script type="text/javascript">
   CKEDITOR.replace( 'paginaSobre' );
</script>

<?php require 'footer.php' ?>