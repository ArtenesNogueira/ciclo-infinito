<?php require 'header.php' ?>

<div class="content-wrapper">
	
	<section class="content-header">
		<h1>
			Planos
			<small>Gerencie os planos cadastrados</small>
		</h1>
	</section>

	<section class="content">
		
		<div class="row">
			
			<div class="col-md-12">
				
				<div class="box">

					<div class="box-header">
						<h3 class="box-title">Planos cadastrados</h3>

						<div class="box-tools">
							<button class="btn btn-danger" data-toggle="modal" data-target="#modalEditar">Criar novo plano</button>
						</div>
					</div>
					
					<div class="box-body table-responsive no-padding">

						<table class="table table-striped">
							<tbody>
								<tr>
									<th>Nome</th>
									<th>Valor</th>
									<th>Retorno</th>
									<th>Limite</th>
									<th>Pontos</th>
									<th></th>
								</tr>

								<?php for ($i = 1; $i <= 8; $i++): ?>

									<tr>
										<td>Nome do plano</td>
										<td class="text-green">R$50.00</td>
										<td class="text-red">R$5.00</td>
										<td class="text-light-blue">360</td>
										<td class="text-light-blue">2</td>
										<td>
											<button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#modalEditar">Editar</button>
											<button class="btn btn-danger btn-sm">Deletar</button>
										</td>
									</tr>

								<?php endfor; ?>

							</tbody>
						</table>
					</div>
					<!-- /.box-body -->
				</div>

			</div>

		</div>

		<?php require 'partial-novo-plano.php' ?>

	</section>

</div>

<?php require 'footer.php' ?>