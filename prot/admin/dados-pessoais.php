<?php require 'header.php' ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dados pessoais
        <small>Veja e atualize seus dados pessoais</small>
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">

          <div class="row">

            <div class="col-md-6">
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Meus dados</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form role="form">
                  <div class="box-body">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Nome completo</label>
                      <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Nome completo" value="Alexander Pierce">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputPassword1">Data de nascimento</label>
                      <input type="text" class="form-control" id="exampleInputPassword1" placeholder="dd/mm/aaaa" value="23/12/1991">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputPassword1">E-mail</label>
                      <input type="text" class="form-control" id="exampleInputPassword1" placeholder="email" value="alexander@gmail.com">
                    </div>
                  </div>
                  <!-- /.box-body -->
                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Salvar</button>
                  </div>
                </form>
              </div>
            </div>

            <div class="col-md-6">
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Alterar senha</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form role="form">
                  <div class="box-body">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Senha atual</label>
                      <input type="password" class="form-control" id="exampleInputEmail1" placeholder="Senha atual">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputPassword1">Nova senha</label>
                      <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Nova senha">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputPassword1">Repita a nova senha</label>
                      <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Repita a nova senha">
                    </div>
                  </div>
                  <!-- /.box-body -->
                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Salvar</button>
                  </div>
                </form>
              </div>
            </div>

          </div>

        </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <?php require 'footer.php' ?>