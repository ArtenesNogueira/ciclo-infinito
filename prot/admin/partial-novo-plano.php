<div class="modal fade" id="modalEditar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">

  <div class="modal-dialog" role="document">

    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Editar dados do plano</h4>
      </div>

      <form action="" method="POST">

        <div class="modal-body">

          <div class="box-body">

            <div class="form-group">
              <label>Nome</label>
              <input type="text" name="nome" class="form-control" required>
            </div>

            <div class="form-group">
              <label>Valor</label>
              <input type="text" class="form-control" name="valor" required>
            </div>

            <div class="form-group">
              <label>Retorno</label>
              <input type="text" class="form-control" name="retorno" required>
            </div>

            <div class="form-group">
              <label>Limite</label>
              <input type="text" class="form-control" name="limite" required>
            </div>

            <div class="form-group">
              <label>Pontos</label>
              <input type="text" class="form-control" name="pontos" required>
            </div>

          </div>

        </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
          <button type="submit" class="btn btn-danger">Salvar</button>
        </div>

      </form>

    </div>
  </div>
</div>