<?php require 'header.php' ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Painel Administrativo
        <small>Informações gerais sobre o sistema</small>
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">

      <div class="row">

        <div class="col-md-6">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Novas solicitações de saque</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <table class="table table-striped">
                <tbody><tr>
                  <th>Número da solicitação</th>
                  <th>Valor</th>
                  <th>Status</th>
                </tr>
                <tr>
                  <td>#234</td>
                  <td>R$67.90</td>
                  <td><span class="badge bg-yellow">Pendente</span></td>
                </tr>
                <tr>
                  <td>#432</td>
                  <td>R$67.90</td>
                  <td><span class="badge bg-yellow">Pendente</span></td>
                </tr>
                <tr>
                  <td>#894</td>
                  <td>R$67.90</td>
                  <td><span class="badge bg-yellow">Pendente</span></td>
                </tr>
                <tr>
                  <td>#199</td>
                  <td>R$67.90</td>
                  <td><span class="badge bg-yellow">Pendente</span></td>
                </tr>
              </tbody></table>
            </div>
          </div>
        </div>

        <div class="col-md-6">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Novas solicitações de depósito</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <table class="table table-striped">
                <tbody><tr>
                  <th>Número da solicitação</th>
                  <th>Valor</th>
                  <th>Status</th>
                </tr>
                <tr>
                  <td>#234</td>
                  <td>R$67.90</td>
                  <td><span class="badge bg-yellow">Pendente</span></td>
                </tr>
                <tr>
                  <td>#432</td>
                  <td>R$67.90</td>
                  <td><span class="badge bg-aqua">Confirmada</span></td>
                </tr>
                <tr>
                  <td>#894</td>
                  <td>R$67.90</td>
                  <td><span class="badge bg-aqua">Confirmada</span></td>
                </tr>
                <tr>
                  <td>#199</td>
                  <td>R$67.90</td>
                  <td><span class="badge bg-yellow">Pendente</span></td>
                </tr>
              </tbody></table>
            </div>
          </div>
        </div>

      </div>

      <div class="row">

        <div class="col-md-6">

          <div class="box box-info">
            <div class="box-header ui-sortable-handle" style="cursor: move;">
              <i class="fa fa-envelope"></i>

              <h3 class="box-title">Mensagem rápida</h3>
              <!-- tools box -->
              <div class="pull-right box-tools">
                <!-- /. tools -->
              </div>
              <div class="box-body">
                <form action="#" method="post">
                  <div class="form-group">
                    <input type="email" class="form-control" name="emailto" placeholder="Para:">
                  </div>
                  <div class="form-group">
                    <input type="text" class="form-control" name="subject" placeholder="Título">
                  </div>
                  <div>
                    <ul class="wysihtml5-toolbar"><li class="dropdown">
                      <a class="btn btn-default dropdown-toggle " data-toggle="dropdown">

                        <span class="glyphicon glyphicon-font"></span>

                        <span class="current-font">Texto normal</span>
                        <b class="caret"></b>
                      </a>
                      <ul class="dropdown-menu">
                        <li><a data-wysihtml5-command="formatBlock" data-wysihtml5-command-value="p" tabindex="-1" href="javascript:;" unselectable="on">Texto Normal</a></li>
                        <li><a data-wysihtml5-command="formatBlock" data-wysihtml5-command-value="h1" tabindex="-1" href="javascript:;" unselectable="on">Título 1</a></li>
                        <li><a data-wysihtml5-command="formatBlock" data-wysihtml5-command-value="h2" tabindex="-1" href="javascript:;" unselectable="on">Título 2</a></li>
                        <li><a data-wysihtml5-command="formatBlock" data-wysihtml5-command-value="h3" tabindex="-1" href="javascript:;" unselectable="on">Título 3</a></li>
                        <li><a data-wysihtml5-command="formatBlock" data-wysihtml5-command-value="h4" tabindex="-1" href="javascript:;" unselectable="on">Título 4</a></li>
                        <li><a data-wysihtml5-command="formatBlock" data-wysihtml5-command-value="h5" tabindex="-1" href="javascript:;" unselectable="on">Título 5</a></li>
                        <li><a data-wysihtml5-command="formatBlock" data-wysihtml5-command-value="h6" tabindex="-1" href="javascript:;" unselectable="on">Título 6</a></li>
                      </ul>
                    </li>
                    <li>
                      <div class="btn-group">
                        <a class="btn  btn-default" data-wysihtml5-command="bold" title="CTRL+B" tabindex="-1" href="javascript:;" unselectable="on">Negrito</a>
                        <a class="btn  btn-default" data-wysihtml5-command="italic" title="CTRL+I" tabindex="-1" href="javascript:;" unselectable="on">Itálico</a>
                        <a class="btn  btn-default" data-wysihtml5-command="underline" title="CTRL+U" tabindex="-1" href="javascript:;" unselectable="on">Sublinhado</a>

                        <a class="btn  btn-default" data-wysihtml5-command="small" title="CTRL+S" tabindex="-1" href="javascript:;" unselectable="on">Pequeno</a>

                      </div>
                    </li>
                    <li>
                      <a class="btn  btn-default" data-wysihtml5-command="formatBlock" data-wysihtml5-command-value="blockquote" data-wysihtml5-display-format-name="false" tabindex="-1" href="javascript:;" unselectable="on">

                        <span class="glyphicon glyphicon-quote"></span>

                      </a>
                    </li>
                    <li>
                      <div class="btn-group">
                        <a class="btn  btn-default" data-wysihtml5-command="insertUnorderedList" title="Unordered list" tabindex="-1" href="javascript:;" unselectable="on">

                          <span class="glyphicon glyphicon-list"></span>

                        </a>
                        <a class="btn  btn-default" data-wysihtml5-command="insertOrderedList" title="Ordered list" tabindex="-1" href="javascript:;" unselectable="on">

                          <span class="glyphicon glyphicon-th-list"></span>

                        </a>
                        <a class="btn  btn-default" data-wysihtml5-command="Outdent" title="Outdent" tabindex="-1" href="javascript:;" unselectable="on">

                          <span class="glyphicon glyphicon-indent-right"></span>

                        </a>
                        <a class="btn  btn-default" data-wysihtml5-command="Indent" title="Indent" tabindex="-1" href="javascript:;" unselectable="on">

                          <span class="glyphicon glyphicon-indent-left"></span>

                        </a>
                      </div>
                    </li>
                    <li>
                      <div class="bootstrap-wysihtml5-insert-link-modal modal fade" data-wysihtml5-dialog="createLink">
                        <div class="modal-dialog ">
                          <div class="modal-content">
                            <div class="modal-header">
                              <a class="close" data-dismiss="modal">×</a>
                              <h3>Insert link</h3>
                            </div>
                            <div class="modal-body">
                              <div class="form-group">
                                <input value="http://" class="bootstrap-wysihtml5-insert-link-url form-control" data-wysihtml5-dialog-field="href">
                              </div> 
                              <div class="checkbox">
                                <label> 
                                  <input type="checkbox" class="bootstrap-wysihtml5-insert-link-target" checked="">Open link in new window
                                </label>
                              </div>
                            </div>
                            <div class="modal-footer">
                              <a class="btn btn-default" data-dismiss="modal" data-wysihtml5-dialog-action="cancel" href="#">Cancel</a>
                              <a href="#" class="btn btn-primary" data-dismiss="modal" data-wysihtml5-dialog-action="save">Insert link</a>
                            </div>
                          </div>
                        </div>
                      </div>
                      <a class="btn  btn-default" data-wysihtml5-command="createLink" title="Insert link" tabindex="-1" href="javascript:;" unselectable="on">

                        <span class="glyphicon glyphicon-share"></span>

                      </a>
                    </li>
                    <li>
                      <div class="bootstrap-wysihtml5-insert-image-modal modal fade" data-wysihtml5-dialog="insertImage">
                        <div class="modal-dialog ">
                          <div class="modal-content">
                            <div class="modal-header">
                              <a class="close" data-dismiss="modal">×</a>
                              <h3>Insert image</h3>
                            </div>
                            <div class="modal-body">
                              <div class="form-group">
                                <input value="http://" class="bootstrap-wysihtml5-insert-image-url form-control" data-wysihtml5-dialog-field="src">
                              </div> 
                            </div>
                            <div class="modal-footer">
                              <a class="btn btn-default" data-dismiss="modal" data-wysihtml5-dialog-action="cancel" href="#">Cancel</a>
                              <a class="btn btn-primary" data-dismiss="modal" data-wysihtml5-dialog-action="save" href="#">Insert image</a>
                            </div>
                          </div>
                        </div>
                      </div>
                      <a class="btn  btn-default" data-wysihtml5-command="insertImage" title="Insert image" tabindex="-1" href="javascript:;" unselectable="on">

                        <span class="glyphicon glyphicon-picture"></span>

                      </a>
                    </li>
                  </ul><textarea class="textarea" style="width: 100%; height: 125px; font-size: 14px; line-height: 18px; border: 1px solid rgb(221, 221, 221); padding: 10px; display: none;" placeholder="Message"></textarea><input type="hidden" name="_wysihtml5_mode" value="1"><iframe class="wysihtml5-sandbox" security="restricted" allowtransparency="true" frameborder="0" width="0" height="0" marginwidth="0" marginheight="0" style="display: inline-block; border-collapse: separate; border: 1px solid rgb(221, 221, 221); clear: none; float: none; margin: 0px; outline: rgb(51, 51, 51) none 0px; outline-offset: 0px; padding: 10px; position: static; top: auto; left: auto; right: auto; bottom: auto; z-index: auto; vertical-align: baseline; text-align: start; box-sizing: border-box; box-shadow: none; border-radius: 0px; width: 100%; height: 125px; background-color: rgb(255, 255, 255);"></iframe>
                </div>
              </form>
            </div>
            <div class="box-footer clearfix">
              <button type="button" class="pull-right btn btn-default" id="sendEmail">Enviar
                <i class="fa fa-arrow-circle-right"></i></button>
              </div>
            </div>
           </div> 

        </div>

        <div class="col-md-6">

          <div class="box box-danger">
                          <div class="box-header with-border">
                            <h3 class="box-title">Novos clientes</h3>

                            <div class="box-tools pull-right">
                              
                            </div>
                          </div>
                          <!-- /.box-header -->
                          <div class="box-body no-padding">
                            <ul class="users-list clearfix">
                              <li>
                                <a class="users-list-name" href="#">Alexander Pierce</a>
                                <span class="users-list-date">Today</span>
                              </li>
                              <li>
                                <a class="users-list-name" href="#">Norman</a>
                                <span class="users-list-date">Yesterday</span>
                              </li>
                              <li>
                                <a class="users-list-name" href="#">Jane</a>
                                <span class="users-list-date">12 Jan</span>
                              </li>
                              <li>
                                <a class="users-list-name" href="#">John</a>
                                <span class="users-list-date">12 Jan</span>
                              </li>
                              <li>
                                <a class="users-list-name" href="#">Alexander</a>
                                <span class="users-list-date">13 Jan</span>
                              </li>
                              <li>
                                <a class="users-list-name" href="#">Sarah</a>
                                <span class="users-list-date">14 Jan</span>
                              </li>
                              <li>
                                <a class="users-list-name" href="#">Nora</a>
                                <span class="users-list-date">15 Jan</span>
                              </li>
                              <li>
                                <a class="users-list-name" href="#">Nadia</a>
                                <span class="users-list-date">15 Jan</span>
                              </li>
                              <li>
                                <a class="users-list-name" href="#">Alexander Pierce</a>
                                <span class="users-list-date">Today</span>
                              </li>
                              <li>
                                <a class="users-list-name" href="#">Norman</a>
                                <span class="users-list-date">Yesterday</span>
                              </li>
                              <li>
                                <a class="users-list-name" href="#">Jane</a>
                                <span class="users-list-date">12 Jan</span>
                              </li>
                              <li>
                                <a class="users-list-name" href="#">John</a>
                                <span class="users-list-date">12 Jan</span>
                              </li>
                              <li>
                                <a class="users-list-name" href="#">Alexander</a>
                                <span class="users-list-date">13 Jan</span>
                              </li>
                              <li>
                                <a class="users-list-name" href="#">Sarah</a>
                                <span class="users-list-date">14 Jan</span>
                              </li>
                              <li>
                                <a class="users-list-name" href="#">Nora</a>
                                <span class="users-list-date">15 Jan</span>
                              </li>
                              <li>
                                <a class="users-list-name" href="#">Nadia</a>
                                <span class="users-list-date">15 Jan</span>
                              </li>
                              <li>
                                <a class="users-list-name" href="#">Alexander Pierce</a>
                                <span class="users-list-date">Today</span>
                              </li>
                              <li>
                                <a class="users-list-name" href="#">Norman</a>
                                <span class="users-list-date">Yesterday</span>
                              </li>
                              <li>
                                <a class="users-list-name" href="#">Jane</a>
                                <span class="users-list-date">12 Jan</span>
                              </li>
                              <li>
                                <a class="users-list-name" href="#">John</a>
                                <span class="users-list-date">12 Jan</span>
                              </li>
                              <li>
                                <a class="users-list-name" href="#">Alexander</a>
                                <span class="users-list-date">13 Jan</span>
                              </li>
                              <li>
                                <a class="users-list-name" href="#">Sarah</a>
                                <span class="users-list-date">14 Jan</span>
                              </li>
                              <li>
                                <a class="users-list-name" href="#">Nora</a>
                                <span class="users-list-date">15 Jan</span>
                              </li>
                              <li>
                                <a class="users-list-name" href="#">Nadia</a>
                                <span class="users-list-date">15 Jan</span>
                              </li>
                            </ul>
                            <!-- /.users-list -->
                          </div>
                          <!-- /.box-body -->
                          <div class="box-footer text-center">
                            <a href="javascript:void(0)" class="uppercase">Ver todos os clientes</a>
                          </div>
                          <!-- /.box-footer -->
                        </div>

        </div>

      </div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <?php require 'footer.php' ?>