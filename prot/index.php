<?php require 'header.php' ?>

<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Escritório virtual
        <small>Informações gerais sobre sua conta</small>
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">

      <div class="row">
        <div class="col-md-12">
          <div class="callout callout-success">
            <p>Saldo atual</p>
            <h2>R$400,00</h2>
          </div>    
        </div>
      </div>

      

      <div class="row">
        <div class="col-md-6">
          <div class="box box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Novidades</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                  <li data-target="#carousel-example-generic" data-slide-to="0" class=""></li>
                  <li data-target="#carousel-example-generic" data-slide-to="1" class="active"></li>
                  <li data-target="#carousel-example-generic" data-slide-to="2" class=""></li>
                </ol>
                <div class="carousel-inner">
                  <div class="item">
                    <img src="http://placehold.it/900x500/39CCCC/ffffff&amp;text=I+Love+Bootstrap" alt="First slide">

                    <div class="carousel-caption">
                      First Slide
                    </div>
                  </div>
                  <div class="item active">
                    <img src="http://placehold.it/900x500/3c8dbc/ffffff&amp;text=I+Love+Bootstrap" alt="Second slide">

                    <div class="carousel-caption">
                      Second Slide
                    </div>
                  </div>
                  <div class="item">
                    <img src="http://placehold.it/900x500/f39c12/ffffff&amp;text=I+Love+Bootstrap" alt="Third slide">

                    <div class="carousel-caption">
                      Third Slide
                    </div>
                  </div>
                </div>
                <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                  <span class="fa fa-angle-left"></span>
                </a>
                <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                  <span class="fa fa-angle-right"></span>
                </a>
              </div>
            </div>
            <!-- /.box-body -->
          </div>
        </div>

        <div class="col-md-6">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Últimas mensagens</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <div class="mailbox-controls">
                <!-- /.btn-group -->
                <!-- /.pull-right -->
              </div>
              <div class="table-responsive mailbox-messages">
                <table class="table table-hover table-striped">
                  <tbody>
                    <tr>
                      <td><div class="icheckbox_flat-blue" aria-checked="false" aria-disabled="false" style="position: relative;"><input type="checkbox" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div></td>
                      <td class="mailbox-star"><a href="#"></a></td>
                      <td class="mailbox-name"><a href="#">Alfa Renda</a></td>
                      <td class="mailbox-subject"><b>Nova promoção!</b> - Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec at justo et mauris semper malesuada. Integer ac sagittis nisi, non feugiat neque, Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec at justo et mauris semper malesuada. Integer ac sagittis nisLorem ipsum dolo...
                      </td>
                      <td class="mailbox-attachment"></td>
                      <td class="mailbox-date">2 horas atrás</td>
                    </tr>
                    <tr>
                      <td><div class="icheckbox_flat-blue" aria-checked="false" aria-disabled="false" style="position: relative;"><input type="checkbox" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div></td>
                      <td class="mailbox-star"><a href="#"></a></td>
                      <td class="mailbox-name"><a href="#">Alfa Renda</a></td>
                      <td class="mailbox-subject"><b>Nova promoção!</b> - Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec at justo et mauris semper malesuada. Integer ac sagittis nisi, non feugiat neque...
                      </td>
                      <td class="mailbox-attachment"></td>
                      <td class="mailbox-date">2 horas atrás</td>
                    </tr>
                    <tr>
                      <td><div class="icheckbox_flat-blue" aria-checked="false" aria-disabled="false" style="position: relative;"><input type="checkbox" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div></td>
                      <td class="mailbox-star"><a href="#"></a></td>
                      <td class="mailbox-name"><a href="#">Alfa Renda</a></td>
                      <td class="mailbox-subject"><b>Nova promoção!</b> - Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec at justo et mauris semper malesuada. Integer ac sagittis nisi, non feugiat neque...
                      </td>
                      <td class="mailbox-attachment"></td>
                      <td class="mailbox-date">2 horas atrás</td>
                    </tr>
                  </tbody>
                </table>
                <!-- /.table -->
              </div>
              <!-- /.mail-box-messages -->
            </div>
            <!-- /.box-body -->
            <div class="box-footer no-padding">
              <div class="mailbox-controls">
              </div>
            </div>
          </div>
        </div>

      </div>

      <div class="row">

        <div class="col-md-6">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Últimas solicitações de saque</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <table class="table table-striped">
                <tbody><tr>
                  <th>Número da solicitação</th>
                  <th>Valor</th>
                  <th>Status</th>
                </tr>
                <tr>
                  <td>#234</td>
                  <td>R$67.90</td>
                  <td><span class="badge bg-yellow">Pendente</span></td>
                </tr>
                <tr>
                  <td>#432</td>
                  <td>R$67.90</td>
                  <td><span class="badge bg-green">Aprovada</span></td>
                </tr>
                <tr>
                  <td>#894</td>
                  <td>R$67.90</td>
                  <td><span class="badge bg-red">Rejeitada</span></td>
                </tr>
                <tr>
                  <td>#199</td>
                  <td>R$67.90</td>
                  <td><span class="badge bg-yellow">Pendente</span></td>
                </tr>
              </tbody></table>
            </div>
          </div>
        </div>

        <div class="col-md-6">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Últimas solicitações de depósito</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <table class="table table-striped">
                <tbody><tr>
                  <th>Número da solicitação</th>
                  <th>Valor</th>
                  <th>Status</th>
                </tr>
                <tr>
                  <td>#234</td>
                  <td>R$67.90</td>
                  <td><span class="badge bg-yellow">Pendente</span></td>
                </tr>
                <tr>
                  <td>#432</td>
                  <td>R$67.90</td>
                  <td><span class="badge bg-green">Aprovada</span></td>
                </tr>
                <tr>
                  <td>#894</td>
                  <td>R$67.90</td>
                  <td><span class="badge bg-aqua">Falta confirmação</span></td>
                </tr>
                <tr>
                  <td>#199</td>
                  <td>R$67.90</td>
                  <td><span class="badge bg-yellow">Pendente</span></td>
                </tr>
              </tbody></table>
            </div>
          </div>
        </div>

      </div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <?php require 'footer.php' ?>