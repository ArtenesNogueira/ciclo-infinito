<?php include 'header.php' ?>

<div class="content-wrapper">

	<section class="content-header">
	  <h1>
	    Assinaturas
	    <small>Confira suas assinaturas aqui</small>
	  </h1>
	</section>

	<section class="content">
		
		<div class="row">
        	<div class="col-xs-12">

        			<div class="nav-tabs-custom">
        	            <ul class="nav nav-tabs">
        	              <li class="active"><a href="#tabAtivas" data-toggle="tab" aria-expanded="true">Ativas</a></li>
        	              <li class=""><a href="#tabEncerradas" data-toggle="tab" aria-expanded="false">Encerradas</a></li>
        	            </ul>
        	            <div class="tab-content">
        	              <div class="tab-pane active" id="tabAtivas">
        	                
        	              	<table class="table table-striped">
        	              	      <tbody><tr>
        	              	        <th>Num. assinatura</th>
        	              	        <th>Plano</th>
        	              	        <th>Data de adesão</th>
        	              	        <th>Valor de retorno</th>
        	              	        <th>Total ganho</th>
        	              	        <th>Limite disponível</th>
        	              	        <th></th>
        	              	      </tr>
        	              	      <?php for ($i = 1; $i <= 4; $i++): ?>
        	              	      	<tr>
        	              	      		<td>24</td>
        	              	      		<td>Nome do plano</td>
        	              	      		<td>09/08/2016</td>
        	              	      		<td>R$5.00</td>
        	              	      		<td>R$1155.00</td>
        	              	      		<td>231/360</td>
        	              	      		<td><a class="btn btn-default btn-sm" href="detalhes-assinatura.php"><i class="fa fa-eye"></i>Detalhes</a></td>
        	              	      	</tr>
        	              	      <?php endfor; ?>
        	              	    </tbody></table>

        	              </div>
        	              
        	              <div class="tab-pane" id="tabEncerradas">
        	                
        	                <table class="table table-striped">
        	              	      <tbody><tr>
        	              	        <th>Num. assinatura</th>
        	              	        <th>Plano</th>
        	              	        <th>Data de adesão</th>
        	              	        <th>Valor de retorno</th>
        	              	        <th>Total ganho</th>
        	              	        <th>Limite disponível</th>
        	              	        <th></th>
        	              	      </tr>
        	              	      <?php for ($i = 1; $i <= 8; $i++): ?>
        	              	      	<tr>
        	              	      		<td>24</td>
        	              	      		<td>Nome do plano</td>
        	              	      		<td>09/08/2016</td>
        	              	      		<td>R$5.00</td>
        	              	      		<td>R$1155.00</td>
        	              	      		<td>231/360</td>
        	              	      		<td><a class="btn btn-default btn-sm" href="detalhes-assinatura.php"><i class="fa fa-eye"></i>Detalhes</a></td>
        	              	      	</tr>
        	              	      <?php endfor; ?>
        	              	    </tbody></table>
        	              	
        	              </div>
        	            </div>
        	          </div>

        	</div>
        </div>

		

		<div class="row">
        <div class="col-xs-12">
          
        </div>
      </div>

	</section>

</div>

<?php include 'footer.php' ?>