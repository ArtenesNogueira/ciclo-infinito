<?php include 'header.php' ?>

<div class="content-wrapper">

	<section class="content-header">
		<h1>
			Assinatura
			<small># 24</small>
		</h1>
	</section>

	<section class="content">
		
		<div class="row">

			<div class="col-md-6 col-sm-6 col-xs-12">

				<div class="small-box bg-green">
					<div class="inner">
						<p>Total ganho</p>

						<h3>R$1155.00</h3>
					</div>
					<div class="icon">
						<i class="fa fa-money"></i>
					</div>
				</div>

			</div>

			<div class="col-md-6 col-sm-6 col-xs-12">

				<div class="small-box bg-aqua">
					<div class="inner">
						<p>Limite disponível</p>

						<h3>210/360</h3>
					</div>
					<div class="icon">
						<i class="fa fa-pie-chart"></i>
					</div>
				</div>

			</div>
			
		</div>


		<div class="row">

			<div class="col-md-3 col-sm-6 col-xs-12">

				<div class="small-box bg-red">
					<div class="inner">
						<p>Pago</p>

						<h3>R$50.00</h3>
					</div>
					<div class="icon">
						<i class="fa fa-money"></i>
					</div>
				</div>

			</div>

			<div class="col-md-3 col-sm-6 col-xs-12">

				<div class="small-box bg-green">
					<div class="inner">
						<p>Retorno de</p>

						<h3>R$5.00 / dia</h3>
					</div>
					<div class="icon">
						<i class="fa fa-thumbs-o-up"></i>
					</div>
				</div>

			</div>

			<div class="col-md-3 col-sm-6 col-xs-12">

				<div class="small-box bg-aqua">
					<div class="inner">
						<p>Limite de transações de</p>

						<h3>360 vezes</h3>
					</div>
					<div class="icon">
						<i class="fa fa-line-chart"></i>
					</div>
				</div>

			</div>

			<div class="col-md-3 col-sm-6 col-xs-12">

				<div class="small-box bg-yellow">
					<div class="inner">
						<p>Custo em pontos de</p>

						<h3>2 pontos</h3>
					</div>
					<div class="icon">
						<i class="fa fa-soccer-ball-o"></i>
					</div>
				</div>

			</div>

		</div>

		<div class="row">

			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<h3 class="box-title">Movimentações nesta assinatura</h3>
					</div>
					<div class="box-body table-responsive no-padding">
						<table class="table table-striped">
							<tbody><tr>
								<th>Valor</th>
								<th>Data</th>
								<th>Descrição</th>
							</tr>
							<?php for ($i = 1; $i <= 10; $i++): ?>
        	              	      	<tr>
        	              	      		<td>5.00</td>
        	              	      		<td>09/08/2016 00:12:34</td>
        	              	      		<td>Recebimento de valor</td>
        	              	      	</tr>
        	              	      <?php endfor; ?>
						</tbody></table>
					</div>
					
					<div class="box-footer clearfix">
						<ul class="pagination pagination-sm no-margin pull-right">
							<li><a href="#">«</a></li>
							<li><a href="#">1</a></li>
							<li><a href="#">2</a></li>
							<li><a href="#">3</a></li>
							<li><a href="#">»</a></li>
						</ul>
					</div>

				</div>
				<!-- /.box -->
			</div>

		</div>

	</section>

</div>

<?php include 'footer.php' ?>