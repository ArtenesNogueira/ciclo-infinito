<?php include 'header.php' ?>

<div class="content-wrapper">

	<section class="content-header">
	  <h1>
	    Planos
	    <small>Veja os planos disponíveis para assinatura</small>
	  </h1>
	</section>

	<section class="content">
		
		<?php for ($i = 1; $i <= 3; $i++): ?>

			<div class="box box-solid box-primary">

			    <div class="box-header with-border">
			      <h3 class="box-title">Título do plano</h3>
			    </div>

			    <div class="box-body">
			      
			      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
			      tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
			      quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
			      consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
			      cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
			      proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

			      <div class="row">
			          
			      	<div class="col-md-3 col-sm-6 col-xs-12">

			      		<div class="small-box bg-red">
			      			<div class="inner">
			      				<p>Valor de</p>

			      				<h3>R$50.00</h3>
			      			</div>
			      			<div class="icon">
			      				<i class="fa fa-money"></i>
			      			</div>
			      		</div>

			      	</div>

			          <div class="col-md-3 col-sm-6 col-xs-12">

			          	<div class="small-box bg-green">
			          		<div class="inner">
			          			<p>Retorno de</p>

			          			<h3>R$5.00 / dia</h3>
			          		</div>
			          		<div class="icon">
			          			<i class="fa fa-thumbs-o-up"></i>
			          		</div>
			          	</div>

			          </div>

			          <div class="col-md-3 col-sm-6 col-xs-12">

			          	<div class="small-box bg-aqua">
			          		<div class="inner">
			          			<p>Limite de créditos de</p>

			          			<h3>360 transações</h3>
			          		</div>
			          		<div class="icon">
			          			<i class="fa fa-line-chart"></i>
			          		</div>
			          	</div>

			          </div>

			          <div class="col-md-3 col-sm-6 col-xs-12">

			          	<div class="small-box bg-yellow">
			          		<div class="inner">
			          			<p>Custo em pontos de</p>

			          			<h3>2 pontos</h3>
			          		</div>
			          		<div class="icon">
			          			<i class="fa fa-soccer-ball-o"></i>
			          		</div>
			          	</div>

			          </div>

			        </div>

			    </div>

			    <div class="box-footer">
			      <div class="box-tools pull-right">
			        <button class="btn btn-primary">Contratar</button>
			      </div>
			    </div>

			  </div>

		<?php endfor; ?>

	</section>

</div>

<?php include 'footer.php' ?>