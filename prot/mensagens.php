<?php require 'header.php' ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Mensagens
        <small>Leia as mensagens do administrador</small>
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">

      <div class="row">
              <div class="col-md-3">

                <div class="box box-solid">
                  <div class="box-header with-border">
                    <h3 class="box-title">Pastas</h3>

                    <div class="box-tools">
                      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                      </button>
                    </div>
                  </div>
                  <div class="box-body no-padding">
                    <ul class="nav nav-pills nav-stacked">
                      <li class="active"><a href="#"><i class="fa fa-inbox"></i> Caixa de entrada
                        <span class="label label-primary pull-right">12</span></a></li>
                      <li><a href="#"><i class="fa fa-trash-o"></i> Lixeira <span class="label label-warning pull-right">65</span></a>
                      </li>
                    </ul>
                  </div>
                  <!-- /.box-body -->
                </div>
                <!-- /. box -->
                <!-- /.box -->
              </div>
              <!-- /.col -->
              <div class="col-md-9">
                <div class="box box-primary">
                  <div class="box-header with-border">
                    <h3 class="box-title">Caixa de entrada</h3>

                    <!-- /.box-tools -->
                  </div>
                  <!-- /.box-header -->
                  <div class="box-body no-padding">
                    <div class="mailbox-controls">
                      <!-- Check all button -->
                      <button type="button" class="btn btn-default btn-sm checkbox-toggle"><i class="fa fa-square-o"></i>
                      </button>
                      <div class="btn-group">
                        <button type="button" class="btn btn-default btn-sm"><i class="fa fa-trash-o"></i></button>
                      </div>
                      <!-- /.btn-group -->
                      <div class="pull-right">
                        1-50/200
                        <div class="btn-group">
                          <button type="button" class="btn btn-default btn-sm"><i class="fa fa-chevron-left"></i></button>
                          <button type="button" class="btn btn-default btn-sm"><i class="fa fa-chevron-right"></i></button>
                        </div>
                        <!-- /.btn-group -->
                      </div>
                      <!-- /.pull-right -->
                    </div>
                    <div class="table-responsive mailbox-messages">
                      <table class="table table-hover table-striped">
                        <tbody>
                        <tr>
                           <td><input type="checkbox"></td>
                          <td class="mailbox-star"></td>
                          <td class="mailbox-name"><a href="ver-mensagem.php">Alexander Pierce</a></td>
                          <td class="mailbox-subject"><b>AdminLTE 2.0 Issue</b> - Trying to find a solution to this problem...
                          </td>
                          <td class="mailbox-attachment"></td>
                          <td class="mailbox-date">5 mins ago</td>
                        </tr>
                        <tr>
                           <td><input type="checkbox"></td>
                          <td class="mailbox-star"></td>
                          <td class="mailbox-name"><a href="ver-mensagem.php">Alexander Pierce</a></td>
                          <td class="mailbox-subject"><b>AdminLTE 2.0 Issue</b> - Trying to find a solution to this problem...
                          </td>
                          <td class="mailbox-attachment"></td>
                          <td class="mailbox-date">28 mins ago</td>
                        </tr>
                        <tr>
                           <td><input type="checkbox"></td>
                          <td class="mailbox-star"></td>
                          <td class="mailbox-name"><a href="ver-mensagem.php">Alexander Pierce</a></td>
                          <td class="mailbox-subject"><b>AdminLTE 2.0 Issue</b> - Trying to find a solution to this problem...
                          </td>
                          <td class="mailbox-attachment"></td>
                          <td class="mailbox-date">11 hours ago</td>
                        </tr>
                        </tbody>
                      </table>
                      <!-- /.table -->
                    </div>
                    <!-- /.mail-box-messages -->
                  </div>
                  <!-- /.box-body -->
                  <div class="box-footer no-padding">
                    <div class="mailbox-controls">
                      <!-- Check all button -->
                      <button type="button" class="btn btn-default btn-sm checkbox-toggle"><i class="fa fa-square-o"></i>
                      </button>
                      <div class="btn-group">
                        <button type="button" class="btn btn-default btn-sm"><i class="fa fa-trash-o"></i></button>
                      </div>
                      <!-- /.btn-group -->
                      <div class="pull-right">
                        1-50/200
                        <div class="btn-group">
                          <button type="button" class="btn btn-default btn-sm"><i class="fa fa-chevron-left"></i></button>
                          <button type="button" class="btn btn-default btn-sm"><i class="fa fa-chevron-right"></i></button>
                        </div>
                        <!-- /.btn-group -->
                      </div>
                      <!-- /.pull-right -->
                    </div>
                  </div>
                </div>
                <!-- /. box -->
              </div>
              <!-- /.col -->
            </div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <?php require 'footer.php' ?>